# Intersect
**Intersect** is a framework providing basic backend and frontend functionality to be shared across multiple projects.

## Changelog
See `CHANGELOG.md` for all current and released features/changes

## Source
https://bitbucket.org/timkippdev/intersect

## Installation via Composer
Include the following code snippet inside your project `composer.json` file (update if necessary)
```
"repositories": [
  {
    "type": "vcs",
    "url": "https://bitbucket.org/timkippdev/intersect"
  }
],
"require" : {
  "timkipp/intersect" : "^3.1.1"
}
```