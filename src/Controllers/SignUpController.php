<?php

namespace TimKipp\Intersect\Controllers;

use TimKipp\Intersect\Domain\Account;

class SignUpController extends AbstractBaseController {

    public function processSignUp()
    {
        if (isset($_POST['email']) && trim($_POST['email']) != '')
        {
            $account = new Account();
            $account->setEmail($_POST['email']);

            if (isset($_POST['password']) && trim($_POST['password']) != '')
            {
                $account->setPassword($_POST['password']);
            }

            if (isset($_POST['username']) && trim($_POST['username']) != '')
            {
                $account->setUsername($_POST['username']);
            }

            if (isset($_POST['firstName']) && trim($_POST['firstName']) != '')
            {
                $account->setFirstName($_POST['firstName']);
            }

            if (isset($_POST['lastName']) && trim($_POST['lastName']) != '')
            {
                $account->setLastName($_POST['lastName']);
            }

            if (isset($_POST['source']) && trim($_POST['source']) != '')
            {
                $account->setRegistrationSource($_POST['source']);
            }

            $passwordLessSignUpAllowed = (isset($_POST['isPasswordLess']) && filter_var($_POST['isPasswordLess'], FILTER_VALIDATE_BOOLEAN));

            if ($passwordLessSignUpAllowed)
            {
                $this->getAccountService()->createWithoutPassword($account);
            }
            else
            {
                $this->getAccountService()->create($account);
            }
        }

        return true;
    }

}