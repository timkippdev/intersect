<?php

namespace TimKipp\Intersect\Controllers;

use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Domain\SocialProvider;

class LoginController extends AbstractBaseController {

    public function processLogin()
    {
        if ((isset($_POST['password']) && trim($_POST['password']) != '') && ((isset($_POST['email']) && trim($_POST['email']) != '') || (isset($_POST['username']) && trim($_POST['username']) != '')))
        {
            $account = new Account();

            if (isset($_POST['email']) && trim($_POST['email']) != '')
            {
                $account->setEmail($_POST['email']);
            }

            if (isset($_POST['username']) && trim($_POST['username']) != '')
            {
                $account->setUsername($_POST['username']);
            }

            $account->setPassword($_POST['password']);

            $this->getAccountService()->login($account, true);
        }
    }

    public function processLogout()
    {
        $this->getAccountService()->logout();
    }

    /**
     * @throws \Exception
     */
    public function processSocialLoginCallback()
    {
        if ($this->getAccountService()->isLoginWithFacebookDetected())
        {
            $this->getAccountService()->loginWithFacebook();
        }
        else if ($this->getAccountService()->isLoginWithTwitterDetected())
        {
            $this->getAccountService()->loginWithTwitter();
        }
        else if ($this->getAccountService()->isLoginWithGoogleDetected())
        {
            $this->getAccountService()->loginWithGoogle();
        }
    }

    /**
     * @throws \Exception
     */
    public function redirectToSocialProvider($providerName, $returnProviderUrlOnly = false)
    {
        $providerUrl = null;

        switch ($providerName)
        {
            case SocialProvider::PROVIDER_NAME_FACEBOOK:
                $providerUrl = $this->getAccountService()->getFacebookLoginUrl();
                break;
            case SocialProvider::PROVIDER_NAME_TWITTER:
                $providerUrl = $this->getAccountService()->getTwitterLoginUrl();
                break;
            case SocialProvider::PROVIDER_NAME_GOOGLE:
                $providerUrl = $this->getAccountService()->getGoogleLoginUrl();
                break;
            default:
                throw new \Exception('Unsupported social provider');
        }

        if ($returnProviderUrlOnly)
        {
            return $providerUrl;
        }

        header('Location: ' . $providerUrl);
        exit();
    }

}