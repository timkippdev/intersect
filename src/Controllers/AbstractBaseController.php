<?php

namespace TimKipp\Intersect\Controllers;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;
use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Email\Services\EmailService;
use TimKipp\Intersect\Helper\CookieManager;
use TimKipp\Intersect\Helper\SessionManager;
use TimKipp\Intersect\Order\Services\OrderService;
use TimKipp\Intersect\Services\AccountService;
use TimKipp\Intersect\Services\SocialAccountService;
use TimKipp\Intersect\Services\SocialProviderService;

/**
 * Class AbstractBaseController
 * @package TimKipp\Intersect\Controllers
 */
abstract class AbstractBaseController {

    private $accountService;
    private $activeAccount;
    private $cookieManager;
    private $databaseAdapter;
    private $emailService;
    private $orderService;
    private $sessionManager;
    private $socialAccountService;
    private $socialProviderService;

    /**
     * AbstractBaseController constructor.
     * @param array $controllerParameters
     * @throws \Exception
     */
    public function __construct($controllerParameters = array())
    {
        if (!array_key_exists('databaseAdapter', $controllerParameters) || !($controllerParameters['databaseAdapter'] instanceof AdapterInterface))
        {
            throw new \Exception('"databaseAdapter" not present in AbstractBaseController parameters');
        }

        if (!array_key_exists('accountService', $controllerParameters) || !($controllerParameters['accountService'] instanceof AccountService))
        {
            throw new \Exception('"accountService" not present in AbstractBaseController parameters');
        }

        $this->databaseAdapter = $controllerParameters['databaseAdapter'];
        $this->accountService = $controllerParameters['accountService'];
        $this->activeAccount = $this->accountService->getCurrentSession();
    }

    /**
     * @return null|Account
     */
    protected function getActiveAccount()
    {
        return $this->activeAccount;
    }

    /**
     * @return AdapterInterface
     */
    protected function getDatabaseAdapter()
    {
        return $this->databaseAdapter;
    }

    /**
     * @return AccountService
     */
    protected function getAccountService()
    {
        return $this->accountService;
    }

    /**
     * @return CookieManager
     */
    protected function getCookieManager()
    {
        return (!is_null($this->cookieManager) ? $this->cookieManager : new CookieManager());
    }

    /**
     * @return SessionManager
     */
    protected function getSessionManager()
    {
        return (!is_null($this->sessionManager) ? $this->sessionManager : new SessionManager());
    }

    /**
     * @return EmailService
     */
    protected function getEmailService()
    {
        return (!is_null($this->emailService) ? $this->emailService : new EmailService($this->getDatabaseAdapter()));
    }

    /**
     * @return OrderService
     */
    protected function getOrderService()
    {
        return (!is_null($this->orderService) ? $this->orderService : new OrderService($this->getDatabaseAdapter()));
    }

    /**
     * @return SocialAccountService
     */
    protected function getSocialAccountService()
    {
        return (!is_null($this->socialAccountService) ? $this->socialAccountService : new SocialAccountService($this->getDatabaseAdapter()));
    }

    /**
     * @return SocialProviderService
     */
    protected function getSocialProviderService()
    {
        return (!is_null($this->socialProviderService) ? $this->socialProviderService : new SocialProviderService($this->getDatabaseAdapter()));
    }

}