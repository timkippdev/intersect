<?php

namespace TimKipp\Intersect\Domain;

interface TemporalDomain {

    public function getDateCreatedColumn();

    public function getDateUpdatedColumn();

}