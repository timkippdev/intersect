<?php

namespace TimKipp\Intersect\Domain;

/**
 * Class AbstractDomain
 * @package TimKipp\Intersect\Domain
 */
abstract class AbstractDomain implements MappableDomain {

    /**
     * @param array $dbRow
     * @return static
     */
    public static function initFromDbRow(array $dbRow)
    {
        $domainObject = new static();
        $columnMappings = $domainObject::getColumnMappings();

        foreach ($columnMappings as $column => $property)
        {
            if (array_key_exists($column, $dbRow)) $domainObject->{$property} = $dbRow[$column];
        }

        return $domainObject;
    }

    /**
     * @return array
     */
    public static function getNullableColumns()
    {
        return array();
    }

    /**
     * @return array
     */
    public static function getNonUpdatableColumns()
    {
        return array();
    }

    /**
     * @return mixed
     */
    abstract public function getPrimaryKeyColumn();

}