<?php

namespace TimKipp\Intersect\Domain;

/**
 * Class Account
 * @package TimKipp\Intersect\Domain
 */
class Account extends AbstractTemporalDomain {

    public $accountId;
    public $email;
    public $username;
    public $password;
    public $passwordResetCode;
    public $passwordResetExpiry;
    public $autoLoginToken;
    public $status;
    public $role;
    public $verificationCode;
    public $firstName;
    public $lastName;
    public $dateCreated;
    public $dateUpdated;
    public $dateLastLogin;
    public $registrationSource;
    public $registrationIP;
    public $lastLoginIP;
    public $salt;

    /**
     * @return array
     */
    public static function getColumnMappings()
    {
        return array(
            'account_id' => 'accountId',
            'email' => 'email',
            'username' => 'username',
            'password' => 'password',
            'password_reset_code' => 'passwordResetCode',
            'password_reset_expiry' => 'passwordResetExpiry',
            'auto_login_token' => 'autoLoginToken',
            'status' => 'status',
            'role' => 'role',
            'verification_code' => 'verificationCode',
            'first_name' => 'firstName',
            'last_name' => 'lastName',
            'date_created' => 'dateCreated',
            'date_updated' => 'dateUpdated',
            'date_last_login' => 'dateLastLogin',
            'registration_source' => 'registrationSource',
            'ip_registration' => 'registrationIP',
            'ip_last_login' => 'lastLoginIP',
            'salt' => 'salt'
        );
    }

    /**
     * @return array
     */
    public static function getNullableColumns()
    {
        return array(
            'username',
            'password_reset_code',
            'password_reset_expiry',
            'role',
            'verification_code',
            'first_name',
            'last_name',
            'registration_source'
        );
    }

    /**
     * @return array
     */
    public static function getNonUpdatableColumns()
    {
        return array(
            'auto_login_token',
            'ip_registration'
        );
    }

    /**
     * @return string
     */
    public function getPrimaryKeyColumn()
    {
        return 'account_id';
    }

    /**
     * @return mixed
     */
    public function getVerificationCode()
    {
        return $this->verificationCode;
    }

    /**
     * @param mixed $verificationCode
     */
    public function setVerificationCode($verificationCode)
    {
        $this->verificationCode = $verificationCode;
    }

    /**
     * @return mixed
     */
    public function getPasswordResetCode()
    {
        return $this->passwordResetCode;
    }

    /**
     * @param mixed $passwordResetCode
     */
    public function setPasswordResetCode($passwordResetCode)
    {
        $this->passwordResetCode = $passwordResetCode;
    }

    /**
     * @return mixed
     */
    public function getPasswordResetExpiry()
    {
        return $this->passwordResetExpiry;
    }

    /**
     * @param $passwordResetExpiry
     */
    public function setPasswordResetExpiry($passwordResetExpiry)
    {
        $this->passwordResetExpiry = $passwordResetExpiry;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param mixed $accountId
     */
    public function setAccountId($accountId)
    {
        $this->accountId = (int) $accountId;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getDateLastLogin()
    {
        return $this->dateLastLogin;
    }

    /**
     * @param $dateLastLogin
     */
    public function setDateLastLogin($dateLastLogin)
    {
        $this->dateLastLogin = $dateLastLogin;
    }

    /**
     * @return mixed
     */
    public function getRegistrationSource()
    {
        return $this->registrationSource;
    }

    /**
     * @param mixed $registrationSource
     */
    public function setRegistrationSource($registrationSource)
    {
        $this->registrationSource = $registrationSource;
    }

    /**
     * @return mixed
     */
    public function getRegistrationIP()
    {
        return $this->registrationIP;
    }

    /**
     * @param $registrationIP
     */
    public function setRegistrationIP($registrationIP)
    {
        $this->registrationIP = $registrationIP;
    }

    /**
     * @return mixed
     */
    public function getLastLoginIP()
    {
        return $this->lastLoginIP;
    }

    /**
     * @param $lastLoginIP
     */
    public function setLastLoginIP($lastLoginIP)
    {
        $this->lastLoginIP = $lastLoginIP;
    }

    /**
     * @return mixed
     */
    public function getAutoLoginToken()
    {
        return $this->autoLoginToken;
    }

    /**
     * @param $autoLoginToken
     */
    public function setAutoLoginToken($autoLoginToken)
    {
        $this->autoLoginToken = $autoLoginToken;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

}