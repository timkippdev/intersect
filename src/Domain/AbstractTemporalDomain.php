<?php

namespace TimKipp\Intersect\Domain;

abstract class AbstractTemporalDomain extends AbstractDomain implements TemporalDomain {

    public function getDateCreatedColumn()
    {
        return 'date_created';
    }

    public function getDateUpdatedColumn()
    {
        return 'date_updated';
    }

}