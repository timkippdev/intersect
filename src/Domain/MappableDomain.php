<?php

namespace TimKipp\Intersect\Domain;

/**
 * Interface MappableDomain
 * @package TimKipp\Intersect\Domain
 */
interface MappableDomain {

    /**
     * @return mixed
     */
    public static function getColumnMappings();

    /**
     * @return mixed
     */
    public static function getNullableColumns();

}