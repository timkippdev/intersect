<?php

namespace TimKipp\Intersect\Database\Dao;

use TimKipp\Intersect\Domain\AbstractDomain;
use TimKipp\Intersect\Validation\NullValidator;

/**
 * Class NullDao
 * @package TimKipp\Intersect\Database\Dao
 */
class NullDao extends AbstractDao {

    /**
     * @return null
     */
    protected function getTable()
    {
        return null;
    }

    /**
     * @return null
     */
    public function getAll()
    {
        return null;
    }

    /**
     * @param $id
     * @return null
     */
    public function getById($id)
    {
        return null;
    }

    /**
     * @param AbstractDomain $obj
     * @param bool $skipValidation
     * @return null
     */
    public function createRecord(AbstractDomain $obj, $skipValidation = false)
    {
        return null;
    }

    /**
     * @param AbstractDomain $obj
     * @return null
     */
    public function deleteRecord(AbstractDomain $obj)
    {
        return null;
    }

    /**
     * @param AbstractDomain $obj
     * @param $objId
     * @return null
     */
    public function updateRecord(AbstractDomain $obj, $objId)
    {
        return null;
    }

    /**
     * @return null
     */
    public function getDomainClass()
    {
        return null;
    }

    /**
     * @return NullValidator
     */
    public function getValidator()
    {
        return new NullValidator();
    }
}