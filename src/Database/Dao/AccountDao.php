<?php

namespace TimKipp\Intersect\Database\Dao;

use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Types\AccountStatusType;
use TimKipp\Intersect\Validation\AccountValidator;

/**
 * Class AccountDao
 * @package TimKipp\Intersect\Database\Dao
 */
class AccountDao extends AbstractDao {

    /**
     * @return string
     */
    public function getDomainClass()
    {
        return Account::class;
    }

    /**
     * @return AccountValidator
     */
    public function getValidator()
    {
        return new AccountValidator();
    }

    /**
     * @return string
     */
    protected function getTable()
    {
        return 'account';
    }

    public function getByAutoLoginToken($autoLoginToken)
    {
        $result = $this->getAdapter()->query("
            SELECT * FROM " . $this->getTable() . " 
            WHERE 
                auto_login_token=:autoLoginToken
            LIMIT 1
        ", array(
            'autoLoginToken' => $autoLoginToken
        ));

        return $this->convertResultToDomainClass($result)->getFirstRecord();
    }

    /**
     * @param $email
     * @return mixed|null
     */
    public function getByEmail($email)
    {
        $result = $this->getAdapter()->query("
            SELECT * FROM " . $this->getTable() . " 
            WHERE 
                email=:email
            LIMIT 1
        ", array(
            'email' => $email
        ));

        return $this->convertResultToDomainClass($result)->getFirstRecord();
    }

    /**
     * @param $verificationCode
     * @return mixed|null
     */
    public function getByVerificationCode($verificationCode)
    {
        $result = $this->getAdapter()->query("
            SELECT * FROM " . $this->getTable() . " 
            WHERE 
                verification_code=:verification_code
            LIMIT 1
        ", array(
            'verification_code' => $verificationCode
        ));

        return $this->convertResultToDomainClass($result)->getFirstRecord();
    }

    /**
     * @param Account $account
     * @return bool
     */
    public function checkIfEmailOrUsernameValidAndActive(Account $account)
    {
        $result = $this->getAdapter()->query("
            SELECT account_id FROM " . $this->getTable() . " 
            WHERE 
                (email=:email OR (username IS NOT NULL AND username=:username)) 
                AND status='" . AccountStatusType::ACTIVE . "' 
            LIMIT 1
        ", array(
            'email' => $account->getEmail(),
            'username' => $account->getUsername()
        ));

        return ($result->getCount() == 1 && !is_null($result->getFirstRecord()['account_id']));
    }

    /**
     * @param $email
     * @param $username
     * @param $password
     * @return mixed|null
     */
    public function getByEmailUsernameAndPassword($email, $username, $password)
    {
        $result = $this->getAdapter()->query("
            SELECT * FROM " . $this->getTable() . " 
            WHERE 
                (email=:email OR (username IS NOT NULL AND username=:username)) 
                AND (password IS NULL OR password=:password)
            LIMIT 1
        ", array(
            'email' => $email,
            'username' => $username,
            'password' => $password
        ));

        return $this->convertResultToDomainClass($result)->getFirstRecord();
    }

}