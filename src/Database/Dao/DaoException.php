<?php

namespace TimKipp\Intersect\Database\Dao;

/**
 * Class DaoException
 * @package TimKipp\Intersect\Database\Dao
 */
class DaoException extends \Exception {}