<?php

namespace TimKipp\Intersect\Database\Dao;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;

/**
 * Class AbstractAssociationDao
 * @package TimKipp\Intersect\Database\Dao
 */
abstract class AbstractAssociationDao implements DaoAssociationInterface {

    /** @var AdapterInterface */
    private $databaseAdapter;

    abstract protected function getAssociationColumnOne();
    abstract protected function getAssociationColumnTwo();
    abstract protected function getTable();

    /**
     * AbstractAssociationDao constructor.
     * @param AdapterInterface $databaseAdapter
     */
    public function __construct(AdapterInterface $databaseAdapter)
    {
        $this->databaseAdapter = $databaseAdapter;
    }

    /**
     * @param $columnOneValue
     * @param $columnTwoValue
     * @return \TimKipp\Intersect\Database\DatabaseResult
     */
    public function getAssociation($columnOneValue, $columnTwoValue)
    {
        return $this->getAdapter()->query("
            SELECT 
                * 
            FROM " . $this->getTable() . " 
            WHERE " . $this->getAssociationColumnOne() . "=:columnOne 
                AND " . $this->getAssociationColumnTwo() . "=:columnTwo 
            LIMIT 1
        ", array(
            "columnOne" => (int) $columnOneValue,
            "columnTwo" => (int) $columnTwoValue
        ));
    }

    /**
     * @param $columnOneValue
     * @param $columnTwoValue
     * @return \TimKipp\Intersect\Database\DatabaseResult
     */
    public function createAssociation($columnOneValue, $columnTwoValue)
    {
        $this->getAdapter()->query("
            INSERT INTO " . $this->getTable() . "
                (" . $this->getAssociationColumnOne() . ", " . $this->getAssociationColumnTwo() . ")
            VALUES
                (:columnOne, :columnTwo)
        ", array(
            "columnOne" => (int) $columnOneValue,
            "columnTwo" => (int) $columnTwoValue,
        ));

        return $this->getAssociation($columnOneValue, $columnTwoValue);
    }

    /**
     * @param $columnOneValue
     * @param $columnTwoValue
     * @return bool
     */
    public function deleteAssociation($columnOneValue, $columnTwoValue)
    {
        $this->getAdapter()->query("
            DELETE FROM " . $this->getTable() . " 
            WHERE " . $this->getAssociationColumnOne() . "=:columnOne 
                AND " . $this->getAssociationColumnTwo() . "=:columnTwo  
            LIMIT 1
        ", array(
            "columnOne" => (int) $columnOneValue,
            "columnTwo" => (int) $columnTwoValue,
        ));

        return true;
    }

    /**
     * @return AdapterInterface
     */
    protected function getAdapter()
    {
        return $this->databaseAdapter;
    }

    /**
     * @param $columnOneValue
     * @return array
     */
    public function getAssociationsForColumnOne($columnOneValue)
    {
        $associationResults = $this->getAdapter()->query("
            SELECT 
                * 
            FROM " . $this->getTable() . " 
            WHERE " . $this->getAssociationColumnOne() . "=:columnOne
        ", array(
            "columnOne" => (int) $columnOneValue
        ));

        $associations = array();

        foreach ($associationResults->getRecords() as $associationResult)
        {
            $associations[] = $associationResult[$this->getAssociationColumnTwo()];
        }

        return $associations;
    }

    /**
     * @param $columnTwoValue
     * @return array
     */
    public function getAssociationsForColumnTwo($columnTwoValue)
    {
        $associationResults = $this->getAdapter()->query("
            SELECT 
                * 
            FROM " . $this->getTable() . " 
            WHERE " . $this->getAssociationColumnTwo() . "=:columnTwo
        ", array(
            "columnTwo" => (int) $columnTwoValue
        ));

        $associations = array();

        foreach ($associationResults->getRecords() as $associationResult)
        {
            $associations[] = $associationResult[$this->getAssociationColumnOne()];
        }

        return $associations;
    }
}