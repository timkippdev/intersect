<?php

namespace TimKipp\Intersect\Database\Dao;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;
use TimKipp\Intersect\Database\DatabaseResult;
use TimKipp\Intersect\Domain\AbstractDomain;
use TimKipp\Intersect\Domain\TemporalDomain;
use TimKipp\Intersect\Validation\NullValidator;
use TimKipp\Intersect\Validation\Validator;

/**
 * Class AbstractDao
 * @package TimKipp\Intersect\Database\Dao
 */
abstract class AbstractDao implements DaoInterface {

    /** @var AdapterInterface */
    private $databaseAdapter;

    /** @var AbstractDomain $domainClass */
    private $domainClass;

    /** @var Validator $validator */
    private $validator;

    /**
     * AbstractDao constructor.
     * @param AdapterInterface $databaseAdapter
     */
    public function __construct(AdapterInterface $databaseAdapter)
    {
        $this->databaseAdapter = $databaseAdapter;

        $domainClass = $this->getDomainClass();

        if (is_string($domainClass))
        {
            $domainClass = new $domainClass();
        }

        $this->domainClass = $domainClass;
        $this->validator = $this->getValidator();
    }

    /**
     * @return mixed
     */
    abstract public function getDomainClass();

    /**
     * @return mixed
     */
    public function getValidator()
    {
        return new NullValidator();
    }

    /**
     * @return false|string
     */
    public function getMySQLNow()
    {
        return date('Y-m-d H:i:s');
    }

    /**
     * @return mixed
     */
    abstract protected function getTable();

    /**
     * @param AbstractDomain $domain
     * @param bool $skipValidation
     * @return mixed|null
     */
    public function createRecord(AbstractDomain $domain, $skipValidation = false)
    {
        if (!$skipValidation)
        {
            $this->validator->validateCreate($domain);
        }

        return $this->create($domain);
    }

    /**
     * @param AbstractDomain $domain
     * @param $domainId
     * @return mixed|null
     */
    public function updateRecord(AbstractDomain $domain, $domainId)
    {
        $this->validator->validateUpdate($domain);

        $updateFields = $this->getUpdateFields($domain);

        if (count($updateFields['fields']) == 0)
        {
            return $this->getById($domainId);
        }

        $primaryIdColumnPlaceholder = ':' . $this->getPrimaryKeyColumn();

        $updateFields['parameters'][$primaryIdColumnPlaceholder] = (int) $domainId;

        $this->getAdapter()->query("
            UPDATE `" . $this->getTable() . "`
            SET 
                " . implode(', ', $updateFields['fields']) . "
            WHERE
                " . $this->getPrimaryKeyColumn() . "=" . $primaryIdColumnPlaceholder . "
            LIMIT 1 
        ", $updateFields['parameters']);

        return $this->getById($domainId);
    }

    /**
     * @param AbstractDomain $domain
     * @return bool
     * @throws \Exception
     */
    public function deleteRecord(AbstractDomain $domain)
    {
        $this->validator->validateDelete($domain);

        $columnMappings = $domain::getColumnMappings();

        if (array_key_exists($this->getPrimaryKeyColumn(), $columnMappings))
        {
            $primaryKeyDomainProperty = $columnMappings[$this->getPrimaryKeyColumn()];
            $primaryKeyPropertyValue = $domain->{$primaryKeyDomainProperty};

            if (is_null($primaryKeyPropertyValue) || intval($primaryKeyPropertyValue) == 0)
            {
                throw new \Exception('domain must have a valid primary key value');
            }

            $this->getAdapter()->query("DELETE FROM `" . $this->getTable() . "` WHERE " . $this->getPrimaryKeyColumn() . "=? LIMIT 1", array((int) $primaryKeyPropertyValue));

            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $result = $this->getAdapter()->query("SELECT * FROM `" . $this->getTable() . "`");

        return $this->convertResultToDomainClass($result)->getRecords();
    }

    /**
     * @param $column
     * @param $value
     * @param int|null $limit
     * @return mixed|null
     */
    public function getAllBy($column, $value, int $limit = null)
    {
        $sql = "SELECT * FROM `" . $this->getTable() . "` WHERE " . $column . (is_null($value) ? ' IS NULL' : '=:' . $column);

        if (!is_null($limit))
        {
            $sql .= " LIMIT " . $limit;
        }

        $queryParameters = (is_null($value) ? array() : array($column => $value));

        $result = $this->getAdapter()->query($sql, $queryParameters);
        $convertedResult = $this->convertResultToDomainClass($result);

        if ($limit == 1)
        {
            return $convertedResult->getFirstRecord();
        }

        return $convertedResult->getRecords();
    }

    /**
     * @param $id
     * @return mixed|null
     */
    public function getById($id)
    {
        return $this->getAllBy($this->getPrimaryKeyColumn(), (int) $id, 1);
    }

    /**
     * @return mixed
     */
    public function getByIds(array $ids)
    {
        if (count($ids) == 0)
        {
            return array();
        }

        $result = $this->getAdapter()->query("SELECT * FROM `" . $this->getTable(). "` WHERE " . $this->getPrimaryKeyColumn() . " IN (" . implode(',', $ids) . ")");

        return $this->convertResultToDomainClass($result)->getRecords();
    }

    /**
     * @return AdapterInterface
     */
    protected function getAdapter()
    {
        return $this->databaseAdapter;
    }

    /**
     * @return mixed
     */
    protected function getPrimaryKeyColumn()
    {
        return $this->domainClass->getPrimaryKeyColumn();
    }

    /**
     * @param DatabaseResult|null $databaseResult
     * @return DatabaseResult
     */
    protected function convertResultToDomainClass(DatabaseResult &$databaseResult = null)
    {
        $initMethod = "initFromDbRow";

        if (!is_null($databaseResult) && !is_null($this->getDomainClass()) && method_exists($this->getDomainClass(), $initMethod))
        {
            $objectRecords = array();

            foreach ($databaseResult->getRecords() as $dbRow)
            {
                $objectRecords[] = $this->getDomainClass()::initFromDbRow($dbRow);
            }

            $databaseResult->setRecords($objectRecords);
        }

        return $databaseResult;
    }

    /**
     * @param AbstractDomain $domain
     * @return mixed|null
     */
    private function create(AbstractDomain $domain)
    {
        $insertData = $this->getInsertData($domain);

        $result = $this->getAdapter()->query("
            INSERT INTO `" . $this->getTable() . "`
                (" . $insertData['columns'] . ")
            VALUES
                (" . $insertData['placeholders'] . ")
        ", $insertData['parameters']);

        return $this->getById($result->getInsertId());
    }

    /**
     * @param AbstractDomain $domainObject
     * @return array
     */
    private function getInsertData(AbstractDomain $domainObject)
    {
        $insertColumns = array();
        $insertValuesPlaceholders = array();

        $columnMappings = $domainObject::getColumnMappings();

        unset($columnMappings[$this->getPrimaryKeyColumn()]);

        $insertValueParameters = array();

        $dateCreatedColumn = null;
        $isTemporalDomain = ($domainObject instanceof TemporalDomain);
        if ($isTemporalDomain)
        {
            $dateCreatedColumn = $domainObject->getDateCreatedColumn();
        }

        foreach ($columnMappings as $columnName => $domainProperty)
        {
            $insertColumns[] = $columnName;
            $insertValuesPlaceholder = ':' . $columnName;
            $insertValuesPlaceholders[] = $insertValuesPlaceholder;

            if ($isTemporalDomain && (!is_null($dateCreatedColumn) && trim($dateCreatedColumn) != '') && (($columnName == $dateCreatedColumn) && is_null($domainObject->{$domainProperty})))
            {
                $domainObject->{$domainProperty} = $this->getMySQLNow();
            }

            $insertValue = ((!is_null($domainObject->{$domainProperty})) ? $domainObject->{$domainProperty} : NULL);

            $insertValueParameters[$insertValuesPlaceholder] = $insertValue;
        }

        return array(
            'columns' => implode(', ', $insertColumns),
            'placeholders' => implode(', ', $insertValuesPlaceholders),
            'parameters' => $insertValueParameters
        );
    }

    /**
     * @param AbstractDomain $domainObject
     * @return array
     */
    private function getUpdateFields(AbstractDomain $domainObject)
    {
        $updateFields = array();
        $columnMappings = $domainObject::getColumnMappings();

        unset($columnMappings[$this->getPrimaryKeyColumn()]);

        $nullableColumns = $domainObject::getNullableColumns();
        $nonUpdatableColumns = $domainObject::getNonUpdatableColumns();

        $updateValueParameters = array();

        $dateUpdatedColumn = null;
        $isTemporalDomain = ($domainObject instanceof TemporalDomain);
        if ($isTemporalDomain)
        {
            $dateUpdatedColumn = $domainObject->getDateUpdatedColumn();
        }

        foreach ($columnMappings as $columnName => $domainProperty)
        {
            if (in_array($columnName, $nonUpdatableColumns))
            {
                continue;
            }

            $domainPropertyValue = $domainObject->{$domainProperty};

            if ($isTemporalDomain && (!is_null($dateUpdatedColumn) && trim($dateUpdatedColumn) != '') && (($columnName == $dateUpdatedColumn) && is_null($domainObject->{$domainProperty})))
            {
                $domainPropertyValue = $this->getMySQLNow();
            }

            if (!in_array($columnName, $nullableColumns) && is_null($domainPropertyValue))
            {
                continue;
            }

            $updatePlaceholder = ':' . $columnName;
            $updateValue = ((!is_null($domainPropertyValue)) ? $domainPropertyValue : NULL);

            $updateFields[] = $columnName . '=' . $updatePlaceholder;
            $updateValueParameters[$updatePlaceholder] = $updateValue;
        }

        return array(
            'fields' => $updateFields,
            'parameters' => $updateValueParameters
        );
    }

}