<?php

namespace TimKipp\Intersect\Database\Dao;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;

class GenericDao extends AbstractDao {

    private $domainClass;
    private $table;

    public function __construct(AdapterInterface $databaseAdapter, $domainClass, $table)
    {
        $this->domainClass = $domainClass;
        $this->table = $table;

        parent::__construct($databaseAdapter);
    }

    public function getDomainClass()
    {
        return $this->domainClass;
    }

    protected function getTable()
    {
        return $this->table;
    }

}