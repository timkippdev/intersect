<?php

namespace TimKipp\Intersect\Database\Dao;

use TimKipp\Intersect\Domain\SocialProvider;

/**
 * Class SocialProviderDao
 * @package TimKipp\Intersect\Database\Dao
 */
class SocialProviderDao extends AbstractDao {

    /**
     * @return string
     */
    protected function getTable()
    {
        return 'social_provider';
    }

    /**
     * @return string
     */
    public function getDomainClass()
    {
        return SocialProvider::class;
    }

    /**
     * @param $providerName
     * @return SocialProvider|null
     */
    public function getByName($providerName)
    {
        $result = $this->getAdapter()->query("SELECT * FROM " . $this->getTable() . " WHERE provider_name=? LIMIT 1", array($providerName));

        return $this->convertResultToDomainClass($result)->getFirstRecord();
    }

}