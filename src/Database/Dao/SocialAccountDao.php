<?php

namespace TimKipp\Intersect\Database\Dao;

use TimKipp\Intersect\Domain\SocialAccount;

/**
 * Class SocialAccountDao
 * @package TimKipp\Intersect\Database\Dao
 */
class SocialAccountDao extends AbstractDao {

    /**
     * @return string
     */
    protected function getTable()
    {
        return 'social_account';
    }

    /**
     * @return string
     */
    public function getDomainClass()
    {
        return SocialAccount::class;
    }

    /**
     * @param $providerId
     * @return mixed
     */
    public function getAllForProviderId($providerId)
    {
        $result = $this->getAdapter()->query("SELECT * FROM " . $this->getTable() . " WHERE provider_id=?", array((int) $providerId));

        return $this->convertResultToDomainClass($result)->getRecords();
    }

    /**
     * @param $email
     * @param $providerId
     * @return mixed|null
     */
    public function getByEmailAndProviderId($email, $providerId)
    {
        if (is_null($providerId))
        {
            return null;
        }

        $result = $this->getAdapter()->query("SELECT * FROM " . $this->getTable() . " WHERE email=? AND provider_id=? LIMIT 1", array($email, $providerId));

        return $this->convertResultToDomainClass($result)->getFirstRecord();
    }

}