<?php

namespace TimKipp\Intersect\Database\Dao;

use TimKipp\Intersect\Domain\AbstractDomain;

/**
 * Interface DaoInterface
 * @package TimKipp\Intersect\Database\Dao
 */
interface DaoInterface {

    public function createRecord(AbstractDomain $obj, $skipValidation = false);

    public function deleteRecord(AbstractDomain $obj);

    public function updateRecord(AbstractDomain $obj, $objId);

    public function getAll();

    public function getAllBy($column, $value, int $limit = null);

    public function getById($id);

    public function getByIds(array $ids);

}