<?php

namespace TimKipp\Intersect\Database\Dao;

/**
 * Interface DaoAssociationInterface
 * @package TimKipp\Intersect\Database\Dao
 */
interface DaoAssociationInterface {

    public function getAssociation($columnOneValue, $columnTwoValue);

    public function getAssociationsForColumnOne($columnOneValue);

    public function getAssociationsForColumnTwo($columnTwoValue);

    public function createAssociation($columnOneValue, $columnTwoValue);

    public function deleteAssociation($columnOneValue, $columnTwoValue);

}