<?php

namespace TimKipp\Intersect\Database\Adapters;

use TimKipp\Intersect\Database\DatabaseException;
use TimKipp\Intersect\Database\DatabaseResult;
use TimKipp\Intersect\Utility\DatabaseUtility;

/**
 * Class MySQLAdapter
 * @package TimKipp\Intersect\Database\Adapters
 */
class MySQLAdapter extends AbstractAdapter {

    protected function connect()
    {
        $dsn = 'mysql:host=' . $this->getHost();

        if (!is_null($this->getDatabase()))
        {
            $dsn .= ';dbname=' . $this->getDatabase();
        }

        $connection = new \PDO($dsn . ';charset=utf8', $this->getUsername(), $this->getPassword(), array(
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
        ));

        $this->setConnection($connection);
    }

    /**
     * @param $name
     * @return DatabaseResult
     */
    public function createDatabase($name)
    {
        return $this->query("CREATE DATABASE IF NOT EXISTS `" . $name . "`");
    }

    /**
     * @param $name
     * @return DatabaseResult
     */
    public function dropDatabase($name)
    {
        return $this->query("DROP DATABASE IF EXISTS `" . $name . "`");
    }

    /**
     * @param $name
     */
    public function selectDatabase($name)
    {
        $this->setDatabase($name);
        $this->getConnection()->exec('USE ' . $name);
    }

    /**
     * @param $table
     * @return DatabaseResult
     */
    public function dropTable($table)
    {
        return $this->query("DROP TABLE IF EXISTS `" . $table . "`");
    }

    /**
     * @param $sql
     * @param array $parameters
     * @return DatabaseResult
     */
    public function query($sql, $parameters = array())
    {
        $result = new DatabaseResult();
        $result->setQuery(DatabaseUtility::removeQueryLineBreaks($sql));
        $result->setQueryParameters($parameters);

        $statement = $this->getConnection()->prepare($sql);

        try {
            $statement->execute($parameters);
        } catch (\Exception $e) {
            $result->setError($e->getMessage());
            $statement = null;
        }

        if (is_null($statement))
        {
            return $result;
        }

        $needsRecords = false;
        if (substr(strtolower(trim($sql)), 0, 6) == 'select')
        {
            $needsRecords = true;
        }
        else if (substr(strtolower(trim($sql)), 0, 4) == 'show')
        {
            $needsRecords = true;
        }

        if ($statement && is_null($result->getError()))
        {
            if ($needsRecords)
            {
                $records = $statement->fetchAll();
                $result->setRecords($records);
            }

            $result->setInsertId($this->getConnection()->lastInsertId());
            $result->setAffectedRows($statement->rowCount());
        }

        return $result;
    }

    /**
     * @param $table
     * @return bool
     */
    public function tableExists($table)
    {
        $tableExistsSql = "
          SELECT * 
          FROM information_schema.tables
          WHERE 
            table_schema = '" . $this->getDatabase() . "' 
            AND table_name = '" . $table . "'
          LIMIT 1;
        ";

        return ($this->query($tableExistsSql)->getCount() > 0);
    }

}