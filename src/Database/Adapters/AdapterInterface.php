<?php

namespace TimKipp\Intersect\Database\Adapters;

use TimKipp\Intersect\Database\DatabaseResult;

/**
 * Interface AdapterInterface
 * @package TimKipp\Intersect\Database\Adapters
 */
interface AdapterInterface {

    /**
     * @return mixed
     */
    public function getConnection();

    /**
     * @param $connection
     * @return mixed
     */
    public function setConnection($connection);

    /**
     * @param $name
     * @return mixed
     */
    public function createDatabase($name);

    /**
     * @param $name
     * @return mixed
     */
    public function dropDatabase($name);

    /**
     * @param $name
     * @return mixed
     */
    public function selectDatabase($name);

    /**
     * @param $table
     * @return mixed
     */
    public function dropTable($table);

    /**
     * @param $sql
     * @param array $parameters
     * @return DatabaseResult
     */
    public function query($sql, $parameters = array());

    /**
     * @param $table
     * @return mixed
     */
    public function tableExists($table);

}