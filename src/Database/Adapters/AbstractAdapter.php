<?php

namespace TimKipp\Intersect\Database\Adapters;

/**
 * Class AbstractAdapter
 * @package TimKipp\Intersect\Database\Adapters
 */
abstract class AbstractAdapter implements AdapterInterface {

    private $host;
    private $username;
    private $password;
    private $database;
    private $connection;

    /**
     * AbstractAdapter constructor.
     * @param $host
     * @param $username
     * @param $password
     * @param null $database
     */
    public function __construct($host, $username, $password, $database = null)
    {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;

        $this->connect();
    }

    abstract protected function connect();

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return null
     */
    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * @param null $database
     */
    public function setDatabase($database)
    {
        $this->database = $database;
    }

    /**
     * @return mixed
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param mixed $connection
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;
    }

}