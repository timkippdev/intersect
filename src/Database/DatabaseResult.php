<?php

namespace TimKipp\Intersect\Database;

/**
 * Class DatabaseResult
 * @package TimKipp\Intersect\Database
 */
class DatabaseResult {

    private $affectedRows = 0;
    private $count = 0;
    private $records = array();
    private $error = null;
    private $insertId = null;
    private $query;
    private $queryParameters = array();

    /**
     * @return mixed|null
     */
    public function getFirstRecord()
    {
        if (count($this->records) > 0)
        {
            return $this->records[0];
        }

        return null;
    }

    /**
     * @return int
     */
    public function getAffectedRows()
    {
        return $this->affectedRows;
    }

    /**
     * @param int $affectedRows
     */
    public function setAffectedRows(int $affectedRows)
    {
        $this->affectedRows = $affectedRows;
    }

    /**
     * @return null
     */
    public function getInsertId()
    {
        return $this->insertId;
    }

    /**
     * @param null $insertId
     */
    public function setInsertId($insertId)
    {
        $this->insertId = $insertId;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getRecords()
    {
        return $this->records;
    }

    /**
     * @param mixed $records
     */
    public function setRecords($records)
    {
        $this->records = $records;
        $this->count = count($records);
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return array
     */
    public function getQueryParameters()
    {
        return $this->queryParameters;
    }

    /**
     * @param array $queryParameters
     */
    public function setQueryParameters(array $queryParameters)
    {
        $this->queryParameters = $queryParameters;
    }

}