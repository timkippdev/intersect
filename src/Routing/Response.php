<?php

namespace TimKipp\Intersect\Routing;

/**
 * Class Response
 * @package TimKipp\Intersect\Routing
 */
class Response {

    private $body;
    private $status = 200;

    /**
     * Response constructor.
     * @param $body
     * @param int $status
     */
    public function __construct($body, $status = 200)
    {
        $this->body = $body;
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus(int $status)
    {
        $this->status = (int) $status;
        return $this;
    }

}