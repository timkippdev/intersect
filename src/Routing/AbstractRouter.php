<?php

namespace TimKipp\Intersect\Routing;

/**
 * Class AbstractRouter
 * @package TimKipp\Intersect\Routing
 *
 * Sample .htaccess file for pretty urls
 *
    RewriteEngine On

    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d

    RewriteRule ^apis/([^\.]+) apis/index.php?url=$1 [QSA,NC,L]
 */
abstract class AbstractRouter {

    private $routerBaseUrl = '/';
    private $currentRequestMethod;
    private $parameters = array();
    private $route;
    private $urlParameter;

    /**
     * AbstractRouter constructor.
     * @param string $urlParameter
     */
    public function __construct($urlParameter = 'url')
    {
        $this->urlParameter = $urlParameter;
        $this->currentRequestMethod = (isset($_SERVER['REQUEST_METHOD']) ? strtoupper($_SERVER['REQUEST_METHOD']) : 'GET');
    }

    /**
     * @return array
     */
    abstract protected function getCustomRoutes();

    /**
     * @return array
     */
    protected function getProvidedRoutes()
    {
        return array(
            'POST /signup' => '\TimKipp\Intersect\Controllers\SignUpController::processSignUp()',
            'POST /login' => '\TimKipp\Intersect\Controllers\LoginController::processLogin()',
            'GET /logout' => '\TimKipp\Intersect\Controllers\LoginController::processLogout()',
            'GET /social/process' => '\TimKipp\Intersect\Controllers\LoginController::processSocialLoginCallback()',
            'POST /social/:providerName' => '\TimKipp\Intersect\Controllers\LoginController::redirectToSocialProvider(providerName)',
        );
    }

    /**
     * @param null $controllerParameters
     * @return mixed|Response
     */
    public function getResponse($controllerParameters = array())
    {
        $urlParameterValue = (isset($_GET[$this->urlParameter]) && trim($_GET[$this->urlParameter] != '') ? trim($_GET[$this->urlParameter]) : '/');

        $this->route = $this->getRouteFromUrl($urlParameterValue);

        if (!is_null($this->route))
        {
            $controller = $this->route->getController();
            $controllerObject = new $controller($controllerParameters);
            $response = call_user_func_array(array($controllerObject, $this->route->getMethod()), $this->route->getParameters());

            if (!($response instanceof Response))
            {
                $response = new Response($response);
            }
        }
        else
        {
            $response = new Response('Route not found', 404);
        }

        return $response;
    }

    /**
     * @param $requestMethod
     */
    protected function setCurrentRequestMethod($requestMethod)
    {
        $this->currentRequestMethod = $requestMethod;
    }

    /**
     * @param $url
     * @return null|Route
     */
    protected function getRouteFromUrl($url)
    {
        $url = trim($url, '/');
        $url = ltrim(ltrim($url, $this->routerBaseUrl), '/');

        foreach ($this->getMergedRoutes() as $route => $action)
        {
            $bits = explode(' ', $route);
            $routeMethod = strtoupper(array_shift($bits));
            $routePath = trim(join(' ', $bits), '/');

            if ($this->currentRequestMethod != $routeMethod)
            {
                continue;
            }

            $this->setParametersFromRoutePath($routePath);

            foreach ($this->parameters as $parameter)
            {
                $routePath = str_replace(':' . $parameter, '(?P<' . $parameter . '>[^/$]+)', $routePath);
            }

            if (!preg_match('#^' . $routePath . '$#', $url, $routeMatches) || !preg_match('#^(?P<controller>.+)\:\:(?P<method>.+)\((?P<parameters>.*)\)$#', $action, $actionMatches))
            {
                continue;
            }

            return $this->getRouteFromMatches($routeMatches, $actionMatches);
        }

        return null;
    }

    /**
     * @param $routePath
     */
    private function setParametersFromRoutePath($routePath)
    {
        if (preg_match_all('#:([a-z0-9]+)/?#i', $routePath, $matches))
        {
            foreach ($matches[1] as $match)
            {
                $this->parameters[] = $match;
            }
        }
    }

    /**
     * @param $routeMatches
     * @param $actionMatches
     * @return Route
     */
    private function getRouteFromMatches($routeMatches, $actionMatches)
    {
        $controller = $actionMatches['controller'];
        $method = $actionMatches['method'];

        $parameters = array();
        $tempParameters = explode(",", $actionMatches['parameters']);

        foreach ($tempParameters as $tempParameter)
        {
            if(isset($routeMatches[$tempParameter]))
            {
                $parameters[] = $routeMatches[$tempParameter];
            }
        }

        $route = new Route();
        $route->setController($controller);
        $route->setMethod($method);
        $route->setParameters($parameters);
        $route->setRequestMethod($this->currentRequestMethod);

        return $route;
    }

    /**
     * @return array
     */
    private function getMergedRoutes()
    {
        return array_merge($this->getProvidedRoutes(), $this->getCustomRoutes());
    }

}