<?php

namespace TimKipp\Intersect\Email\Services;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;
use TimKipp\Intersect\Email\Domain\BaseEmail;
use TimKipp\Intersect\Email\Domain\EmailHeaders;
use TimKipp\Intersect\Email\Domain\EmailQueue;
use TimKipp\Intersect\Email\Sender\DefaultEmailSender;
use TimKipp\Intersect\Email\Sender\EmailSender;
use TimKipp\Intersect\Event\Dispatcher;
use TimKipp\Intersect\Event\EmailSentEvent;
use TimKipp\Intersect\Event\EmailSentFailedEvent;
use TimKipp\Intersect\Event\Event;

/**
 * Class EmailService
 * @package TimKipp\Intersect\Email\Services
 */
class EmailService {

    private $emailQueueService;
    private $emailSender;
    private $eventDispatcher;

    public function __construct(AdapterInterface $adapter, EmailSender $emailSender = null)
    {
        $this->emailQueueService = new EmailQueueService($adapter);

        if (is_null($emailSender))
        {
            $emailSender = new DefaultEmailSender();
        }

        $this->emailSender = $emailSender;

        $this->eventDispatcher = Dispatcher::get();
    }

    /**
     * @return Dispatcher
     */
    public function getEventDispatcher()
    {
        return $this->eventDispatcher;
    }

    public function queue(BaseEmail $email)
    {
        $this->validateFromEmail($email);

        $emailQueue = new EmailQueue();
        $emailQueue->setSubject($email->getSubject());
        $emailQueue->setMessage($email->getMessage());
        $emailQueue->setDateCreated($this->getMySQLNow());
        $emailQueue->setType($this->getEmailType($email));
        $emailQueue->setHeaders($email->getHeaders());
        $emailQueue->setRecipient($email->getRecipient());

        $this->emailQueueService->create($emailQueue);
    }

    public function send(BaseEmail $email)
    {
        $this->validateFromEmail($email);
        $this->performSend($email->getRecipient(), $email->getSubject(), $email->getMessage(), $email->getHeaders());
    }

    public function processQueue($limit = null)
    {
        if (!is_null($limit))
        {
            $limit = (int) $limit;
        }

        $results = $this->emailQueueService->getAllUnsent($limit);

        /** @var EmailQueue $emailQueue */
        foreach ($results as $emailQueue)
        {
            $this->performSend($emailQueue->getRecipient(), $emailQueue->getSubject(), $emailQueue->getMessage(), $emailQueue->getHeaders());

            $emailQueue->setDateSent($this->getMySQLNow());
            $this->emailQueueService->update($emailQueue, $emailQueue->getEmailId());
        }
    }

    public function getEmailQueueService()
    {
        return $this->emailQueueService;
    }

    public function getEmailSender()
    {
        return $this->emailSender;
    }

    private function getMySQLNow()
    {
        return date('Y-m-d H:i:s');
    }

    private function performSend($recipient, $subject, $message, EmailHeaders $headers)
    {
        $emailSent = false;
        $response = 'Something went wrong';

        try {
            $this->emailSender->send($recipient, $subject, $message, $headers);

            $emailSent = true;
        } catch (\Exception $e) {
            $response = $e->getMessage();
        }

        if ($emailSent)
        {
            $this->getEventDispatcher()->dispatch(new EmailSentEvent($recipient, $subject, $message, $headers), 'Email has been sent');
        }
        else
        {
            $this->getEventDispatcher()->dispatch(new EmailSentFailedEvent($recipient, $subject, $message, $headers), $response);
        }
    }

    private function validateFromEmail(BaseEmail $email)
    {
        if (is_null($email->getFromEmail()) || trim($email->getFromEmail()) == '')
        {
            new \Exception('Email cannot be sent or queued because the From email is not set');
            die();
        }
    }

    private function getEmailType(BaseEmail $email)
    {
        return (new \ReflectionClass($email))->getShortName();
    }

}