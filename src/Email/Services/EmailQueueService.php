<?php

namespace TimKipp\Intersect\Email\Services;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;
use TimKipp\Intersect\Email\Dao\EmailQueueDao;
use TimKipp\Intersect\Email\Domain\EmailQueue;
use TimKipp\Intersect\Services\AbstractService;

/**
 * Class EmailQueueService
 * @package TimKipp\Intersect\Email\Services
 *
 * @method EmailQueueDao getDao
 */
class EmailQueueService extends AbstractService {

    public function __construct(AdapterInterface $databaseAdapter)
    {
        parent::__construct($databaseAdapter);

        $dao = new EmailQueueDao($databaseAdapter);
        $this->setDao($dao);
    }

    /**
     * @return \TimKipp\Intersect\Database\DatabaseResult
     */
    public function deleteAll()
    {
        return $this->getDao()->deleteAll();
    }

    /**
     * @return \TimKipp\Intersect\Database\DatabaseResult
     */
    public function deleteAllSent()
    {
        return $this->getDao()->deleteAllSent();
    }

    public function getAllUnsent($limit = null)
    {
        if (!is_null($limit))
        {
            $limit = (int) $limit;
        }

        return $this->getDao()->getAllBy('date_sent', null, $limit);
    }

}