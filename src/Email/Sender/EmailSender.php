<?php

namespace TimKipp\Intersect\Email\Sender;

use TimKipp\Intersect\Email\Domain\EmailHeaders;

/**
 * Interface EmailSenderInterface
 * @package TimKipp\Intersect\Email\Sender
 */
interface EmailSender {

    /**
     * @param $recipient
     * @param $subject
     * @param $message
     * @param $headers
     * @return bool
     */
    public function send($recipient, $subject, $message, EmailHeaders $headers);

}