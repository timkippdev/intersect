<?php

namespace TimKipp\Intersect\Email\Sender;
use TimKipp\Intersect\Email\Domain\EmailHeaders;

/**
 * Class DefaultEmailSender
 * @package TimKipp\Intersect\Email\Sender
 */
class DefaultEmailSender implements EmailSender {

    /**
     * @param $recipient
     * @param $subject
     * @param $message
     * @param EmailHeaders $headers
     * @return bool
     */
    public function send($recipient, $subject, $message, EmailHeaders $headers)
    {
        return mail($recipient, $subject, $message, $headers->getAsString());
    }


}