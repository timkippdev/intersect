<?php

namespace TimKipp\Intersect\Email\Domain;

/**
 * Class BaseEmail
 * @package TimKipp\Intersect\Email\Domain
 */
abstract class BaseEmail {

    private $recipient;
    private $ccRecipients = array();
    private $bccRecipients = array();

    /**
     * @return mixed
     */
    abstract public function getSubject();

    /**
     * @return mixed
     */
    abstract public function getMessage();

    /**
     * @return mixed
     */
    abstract public function getFromEmail();

    /**
     * @return mixed
     */
    abstract public function getFromName();

    /**
     * @return mixed
     */
    abstract public function getReplyToEmail();

    /**
     * @param $recipient
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    }

    /**
     * @return mixed
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param $ccRecipient
     */
    public function addCCRecipient($ccRecipient)
    {
        $this->ccRecipients[] = $ccRecipient;
    }

    /**
     * @return array
     */
    public function getCCRecipients()
    {
        return $this->ccRecipients;
    }

    /**
     * @param $bccRecipient
     */
    public function addBCCRecipient($bccRecipient)
    {
        $this->bccRecipients[] = $bccRecipient;
    }

    /**
     * @return array
     */
    public function getBCCRecipients()
    {
        return $this->bccRecipients;
    }

    /**
     * @return EmailHeaders
     */
    public function getHeaders()
    {
        $fromName = (!is_null($this->getFromName()) && trim($this->getFromName()) != '') ? $this->getFromName() : $this->getFromEmail();
        $replyToEmail = (!is_null($this->getReplyToEmail()) && trim($this->getReplyToEmail()) != '') ? $this->getReplyToEmail() : $this->getFromEmail();

        $headers = new EmailHeaders();

        $headers->addHeader(new EmailHeader('MIME-Version', '1.0'));
        $headers->addHeader(new EmailHeader('Content-type', 'text/html; charset=iso-8859-1'));
        $headers->addHeader(new EmailHeader('From', $fromName . ' <' . $this->getFromEmail() . '>'));
        $headers->addHeader(new EmailHeader('Reply-To', $fromName . ' <' . $replyToEmail . '>'));

        if (count($this->getCCRecipients()) > 0)
        {
            $headers->addHeader(new EmailHeader('Cc', implode(',', $this->getCCRecipients())));
        }
        if (count($this->getBCCRecipients()) > 0)
        {
            $headers->addHeader(new EmailHeader('Bcc', implode(',', $this->getBCCRecipients())));
        }

        return $headers;
    }

}