<?php

namespace TimKipp\Intersect\Email\Domain;

class EmailHeaders {

    private $headers = array();

    public function __construct() {}

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    public function addHeader(EmailHeader $emailHeader)
    {
        $this->headers[$emailHeader->getName()] = $emailHeader;
    }

    public function getAsString($lineSeparator = "\r\n")
    {
        if (is_null($this->headers) || count($this->headers) == 0)
        {
            return '';
        }

        $headers = array();

        /** @var EmailHeader $header */
        foreach ($this->headers as $header)
        {
            $headers[] = $header->getName() . ': ' . trim($header->getValue());
        }

        return implode($lineSeparator, $headers);
    }

}