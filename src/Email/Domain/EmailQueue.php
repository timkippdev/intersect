<?php

namespace TimKipp\Intersect\Email\Domain;

use TimKipp\Intersect\Domain\AbstractDomain;
use TimKipp\Intersect\Domain\AbstractTemporalDomain;

/**
 * Class EmailQueue
 * @package TimKipp\Intersect\Email\Domain
 */
class EmailQueue extends AbstractTemporalDomain {

    public $emailId;
    public $recipient;
    public $subject;
    public $message;
    /** @var EmailHeaders $headers */
    public $headers;
    public $headersDb;
    public $type;
    public $dateCreated;
    public $dateSent;

    /**
     * @return string
     */
    public function getPrimaryKeyColumn()
    {
        return 'email_id';
    }

    /**
     * @return null
     */
    public function getDateUpdatedColumn()
    {
        return null;
    }

    /**
     * @return array
     */
    public static function getColumnMappings()
    {
        return array(
            'email_id' => 'emailId',
            'recipient' => 'recipient',
            'subject' => 'subject',
            'message' => 'message',
            'headers' => 'headersDb',
            'type' => 'type',
            'date_created' => 'dateCreated',
            'date_sent' => 'dateSent'
        );
    }

    /**
     * @return array
     */
    public static function getNullableColumns()
    {
        return array(
            'subject',
            'date_sent'
        );
    }

    /**
     * @return mixed
     */
    public function getEmailId()
    {
        return $this->emailId;
    }

    /**
     * @param mixed $emailId
     */
    public function setEmailId($emailId)
    {
        $this->emailId = $emailId;
    }

    /**
     * @return mixed
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param mixed $recipient
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return EmailHeaders
     */
    public function getHeaders()
    {
        if (is_null($this->headers) && !is_null($this->headersDb))
        {
            $this->populateHeaders();
        }

        return $this->headers;
    }

    public function getHeadersDb()
    {
        if (is_null($this->headersDb) || !is_null($this->headers))
        {
            $this->headersDb = $this->headers->getAsString();
        }

        return $this->headersDb;
    }

    /**
     * @param EmailHeaders $headers
     */
    public function setHeaders(EmailHeaders $headers)
    {
        $this->headers = $headers;
        $this->headersDb = $headers->getAsString();
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateSent()
    {
        return $this->dateSent;
    }

    /**
     * @param mixed $dateSent
     */
    public function setDateSent($dateSent)
    {
        $this->dateSent = $dateSent;
    }

    private function populateHeaders()
    {
        if (is_null($this->headersDb) || trim($this->headersDb) == '')
        {
            return;
        }

        $headerLines = explode("\r\n", $this->headersDb);

        $headers = new EmailHeaders();

        foreach ($headerLines as $headerLine)
        {
            $headerParts = explode(':', $headerLine);

            $headers->addHeader(new EmailHeader(trim($headerParts[0]), trim($headerParts[1])));
        }

        $this->headers = $headers;
    }

}