<?php

namespace TimKipp\Intersect\Email\Dao;

use TimKipp\Intersect\Database\Dao\AbstractDao;
use TimKipp\Intersect\Email\Domain\EmailQueue;

/**
 * Class EmailQueueDao
 * @package TimKipp\Intersect\Email\Dao
 */
class EmailQueueDao extends AbstractDao {

    /**
     * @return string
     */
    protected function getTable()
    {
        return 'email_queue';
    }

    /**
     * @return string
     */
    public function getDomainClass()
    {
        return EmailQueue::class;
    }

    /**
     * @return \TimKipp\Intersect\Database\DatabaseResult
     */
    public function deleteAll()
    {
        return $this->getAdapter()->query("DELETE FROM " . $this->getTable());
    }

    /**
     * @return \TimKipp\Intersect\Database\DatabaseResult
     */
    public function deleteAllSent()
    {
        return $this->getAdapter()->query("DELETE FROM " . $this->getTable() . " WHERE date_sent IS NOT NULL");
    }

}