<?php

namespace TimKipp\Intersect\Services;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;
use TimKipp\Intersect\Database\Dao\GenericDao;

class GenericService extends AbstractService {

    public function __construct(AdapterInterface $databaseAdapter, $domainClass, $table)
    {
        parent::__construct($databaseAdapter);

        $this->setDao(new GenericDao($databaseAdapter, $domainClass, $table));
    }

}