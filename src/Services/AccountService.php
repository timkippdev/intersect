<?php

namespace TimKipp\Intersect\Services;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;
use TimKipp\Intersect\Database\Dao\AccountDao;
use TimKipp\Intersect\Domain\AbstractDomain;
use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Domain\SocialAccount;
use TimKipp\Intersect\Domain\SocialProvider;
use TimKipp\Intersect\Event\AccountCreatedEvent;
use TimKipp\Intersect\Event\AccountCreatedFailedEvent;
use TimKipp\Intersect\Event\AccountLoginFailedEvent;
use TimKipp\Intersect\Event\AccountLoginEvent;
use TimKipp\Intersect\Event\AccountLogoutEvent;
use TimKipp\Intersect\Event\AccountResetPasswordEvent;
use TimKipp\Intersect\Event\AccountResetPasswordFailedEvent;
use TimKipp\Intersect\Event\AccountSocialLoginEvent;
use TimKipp\Intersect\Event\SocialAccountLoginFailedEvent;
use TimKipp\Intersect\Social\Facebook\FacebookClient;
use TimKipp\Intersect\Social\Google\GoogleClient;
use TimKipp\Intersect\Social\SocialClient;
use TimKipp\Intersect\Social\SocialUserDetails;
use TimKipp\Intersect\Social\Twitter\TwitterClient;
use TimKipp\Intersect\Types\AccountStatusType;
use TimKipp\Intersect\Utility\IPAddressUtility;
use TimKipp\Intersect\Validation\AccountValidator;
use TimKipp\Intersect\Validation\ValidationException;

/**
 * Class AccountService
 * @package TimKipp\Intersect\Services
 *
 * @method AccountDao getDao
 * @method AccountValidator getValidator
 */
class AccountService extends AbstractService {

    protected static $SESSION_EXPIRY = 30;
    protected static $SESSION_NAME = 'act-rem-me';
    protected static $SESSION_SECRET = 'mdzwaTjhBI15StsnCXr4';

    /** @var Encryptor $encryptor */
    protected $encryptor;

    /** @var FacebookClient $facebookClient */
    protected $facebookClient;

    /** @var GoogleClient $googleClient */
    protected $googleClient;

    protected $passwordResetExpiryThresholdInMinutes = 15;

    /** @var SocialAccountService $socialAccountService */
    protected $socialAccountService;

    /** @var TwitterClient $twitterClient */
    protected $twitterClient;

    /**
     * AccountService constructor.
     * @param AdapterInterface $databaseAdapter
     */
    public function __construct(AdapterInterface $databaseAdapter)
    {
        parent::__construct($databaseAdapter);

        $this->setDao(new AccountDao($databaseAdapter));

        $this->socialAccountService = new SocialAccountService($databaseAdapter);
        $this->encryptor = new EncryptionService();
    }

    /**
     * @return Encryptor
     */
    public function getEncryptor()
    {
        return $this->encryptor;
    }

    /**
     * @param Encryptor $encryptor
     */
    public function setEncryptor(Encryptor $encryptor)
    {
        $this->encryptor = $encryptor;
    }

    /**
     * @param $passwordResetExpiryThresholdInMinutes
     */
    public function setPasswordResetExpiryThresholdInMinutes($passwordResetExpiryThresholdInMinutes)
    {
        $this->passwordResetExpiryThresholdInMinutes = (int) $passwordResetExpiryThresholdInMinutes;
    }

    /**
     * @param FacebookClient $facebookClient
     */
    public function setFacebookClient(FacebookClient $facebookClient)
    {
        $this->facebookClient = $facebookClient;
    }

    /**
     * @param TwitterClient $twitterClient
     */
    public function setTwitterClient(TwitterClient $twitterClient)
    {
        $this->twitterClient = $twitterClient;
    }

    /**
     * @param GoogleClient $googleClient
     */
    public function setGoogleClient(GoogleClient $googleClient)
    {
        $this->googleClient = $googleClient;
    }

    /**
     * @param SocialAccountService $socialAccountService
     */
    public function setSocialAccountService(SocialAccountService $socialAccountService)
    {
        $this->socialAccountService = $socialAccountService;
    }

    /**
     * @param Account $account
     * @return mixed|null
     * @throws ValidationException
     */
    public function create(AbstractDomain $account)
    {
        return $this->createAccount($account);
    }

    /**
     * @param Account|AbstractDomain $account
     * @return Account
     */
    public function delete(AbstractDomain $account)
    {
        return $this->deactivate($account);
    }

    /**
     * @param Account $account
     * @return mixed|null
     * @throws ValidationException
     */
    public function createWithoutPassword(Account $account)
    {
        return $this->createAccount($account, true);
    }

    /**
     * @return null|Account
     */
    public function getCurrentSession()
    {
        $sessionValue = $this->getSessionManager()->read(self::$SESSION_NAME);
        $sessionIsFromCookie = false;

        if (is_null($sessionValue))
        {
            $sessionValue = $this->getCookieManager()->read(self::$SESSION_NAME);
            $sessionIsFromCookie = true;
        }

        if (is_null($sessionValue) || trim($sessionValue) == '')
        {
            return null;
        }

        $decodedSessionValueParts = explode(':', base64_decode($sessionValue));

        if (count($decodedSessionValueParts) != 3)
        {
            $this->clearSession();
            return null;
        }

        list($accountId, $cookieTime, $sessionSignature) = $decodedSessionValueParts;

        if ($sessionSignature !== $this->generateSessionSignature($accountId, $cookieTime))
        {
            $this->clearSession();
            return null;
        }

        $accountFromSession = $this->getById($accountId);

        if (is_null($accountFromSession))
        {
            $this->clearSession();
            return null;
        }

        if ($sessionIsFromCookie)
        {
            $accountFromSession->setLastLoginIP(IPAddressUtility::get());
            $accountFromSession->setDateLastLogin($this->getMySQLNow());

            $this->update($accountFromSession, $accountFromSession->getAccountId());

            $this->getSessionManager()->write(self::$SESSION_NAME, $sessionValue);
        }

        return $accountFromSession;
    }

    /**
     * @param Account $account
     * @param bool $storeCookie
     * @return bool
     */
    public function login(Account $account, $storeCookie = false)
    {
        return $this->handleLogin($account, $storeCookie, false);
    }

    /**
     * @return bool
     */
    public function logout()
    {
        $accountToLogout = $this->getCurrentSession();

        if (!is_null($accountToLogout))
        {
            $this->clearSession();

            $this->getEventDispatcher()->dispatch(new AccountLogoutEvent($accountToLogout), 'Account was logged out successfully.');
        }

        return true;
    }

    /**
     * @param $autoLoginToken
     * @return null|Account
     */
    public function getByAutoLoginToken($autoLoginToken)
    {
        return $this->getDao()->getByAutoLoginToken($autoLoginToken);
    }

    /**
     * @param $email
     * @return null|Account
     */
    public function getByEmail($email)
    {
        return $this->getDao()->getByEmail($email);
    }

    /**
     * @param $verificationCode
     * @return null|Account
     */
    public function getByVerificationCode($verificationCode)
    {
        return $this->getDao()->getByVerificationCode($verificationCode);
    }

    /**
     * @param Account $account
     * @return Account
     */
    public function deactivate(Account $account)
    {
        $account->setStatus(AccountStatusType::DEACTIVATED);
        return $this->update($account, $account->getAccountId());
    }

    /**
     * @param Account $account
     * @return Account
     */
    public function activate(Account $account)
    {
        $account->setStatus(AccountStatusType::ACTIVE);
        return $this->update($account, $account->getAccountId());
    }

    /**
     * @param Account $account
     * @param $newPassword
     * @return Account
     */
    public function resetPassword(Account $account, $newPassword)
    {
        // generate a new salt
        $account->setSalt($this->encryptor->generateSalt());
        $account->setPassword($this->generateNewPassword($newPassword, $account->getSalt()));
        $account->setPasswordResetCode(null);
        $account->setPasswordResetExpiry(null);

        $updatedAccount = $this->update($account, $account->getAccountId());

        if (!is_null($updatedAccount))
        {
            $this->getEventDispatcher()->dispatch(new AccountResetPasswordEvent($updatedAccount), 'Account password was reset successfully.');
        }

        return $updatedAccount;
    }

    /**
     * @param Account $account
     * @param $newPassword
     * @param null $passwordResetCode
     * @return null|Account|string
     * @throws ValidationException
     */
    public function resetPasswordWithVerification(Account $account, $newPassword, $passwordResetCode = null)
    {
        if (is_null($passwordResetCode))
        {
            $passwordResetCode = sha1($account->getAccountId() . $account->getEmail() . $account->getPassword());
            $account->setPasswordResetCode($passwordResetCode);

            $now = strtotime($this->getMySQLNow());
            $expiryTime = $this->getMySQLNow(($now + (60 * $this->passwordResetExpiryThresholdInMinutes)));
            $account->setPasswordResetExpiry($expiryTime);

            $this->update($account, $account->getAccountId());

            return $passwordResetCode;
        }

        if ($passwordResetCode != $account->getPasswordResetCode())
        {
            throw new ValidationException('Password reset code does not match stored value');
        }
        else
        {
            $now = strtotime($this->getMySQLNow());
            $expiryTimeFromDb = strtotime($account->getPasswordResetExpiry());

            if ($expiryTimeFromDb < $now)
            {
                $this->getEventDispatcher()->dispatch(new AccountResetPasswordFailedEvent(), 'Password reset code has expired');
                throw new ValidationException('Password reset code has expired');
            }
        }

        return $this->resetPassword($account, $newPassword);
    }

    /**
     * @param Account $account
     * @param $verificationCode
     * @return Account
     * @throws ValidationException
     */
    public function verify(Account $account, $verificationCode)
    {
        if (is_null($account->getVerificationCode()) || $account->getStatus() != AccountStatusType::PENDING)
        {
            return $account;
        }

        if ($verificationCode != $account->getVerificationCode())
        {
            throw new ValidationException('Verification code does not match stored value');
        }

        $account->setStatus(AccountStatusType::ACTIVE);
        $account->setVerificationCode(null);

        return $this->update($account, $account->getAccountId());
    }

    /**
     * @return bool
     */
    public function isLoginWithFacebookDetected()
    {
        return (isset($_GET['code'], $_GET['state']));
    }

    /**
     * @param array $scopeParameters
     * @return string
     * @throws \Exception
     */
    public function getFacebookLoginUrl(array $scopeParameters = array('email'))
    {
        if (is_null($this->facebookClient))
        {
            throw new \Exception('FacebookClient not set properly');
        }

        return $this->facebookClient->getLoginUrl($scopeParameters);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function loginWithFacebook($storeCookie = true, $shouldCreateAccount = true)
    {
        if (is_null($this->facebookClient))
        {
            throw new \Exception('FacebookClient not set properly');
        }

        return $this->handleSocialLogin($this->facebookClient, SocialProvider::PROVIDER_NAME_FACEBOOK, $storeCookie, $shouldCreateAccount);
    }

    /**
     * @return bool
     */
    public function isLoginWithTwitterDetected()
    {
        return (isset($_GET['oauth_token'], $_GET['oauth_verifier']));
    }

    /**
     * @return string
     * @throws \Exception
     * @throws \TimKipp\Intersect\Social\SocialClientException
     */
    public function getTwitterLoginUrl()
    {
        if (is_null($this->twitterClient))
        {
            throw new \Exception('TwitterClient not set properly');
        }

        return $this->twitterClient->getLoginUrl();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function loginWithTwitter($storeCookie = true, $shouldCreateAccount = true)
    {
        if (is_null($this->twitterClient))
        {
            throw new \Exception('TwitterClient not set properly');
        }

        return $this->handleSocialLogin($this->twitterClient, SocialProvider::PROVIDER_NAME_TWITTER, $storeCookie, $shouldCreateAccount);
    }

    /**
     * @return bool
     */
    public function isLoginWithGoogleDetected()
    {
        return (isset($_GET['code']));
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getGoogleLoginUrl()
    {
        if (is_null($this->googleClient))
        {
            throw new \Exception('GoogleClient not set properly');
        }

        return $this->googleClient->getLoginUrl();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function loginWithGoogle($storeCookie = true, $shouldCreateAccount = true)
    {
        if (is_null($this->googleClient))
        {
            throw new \Exception('GoogleClient not set properly');
        }

        return $this->handleSocialLogin($this->googleClient, SocialProvider::PROVIDER_NAME_GOOGLE, $storeCookie, $shouldCreateAccount);
    }

    /**
     * @param $autoLoginToken
     * @return bool
     */
    public function loginWithAutoLoginToken($autoLoginToken)
    {
        $account = $this->getByAutoLoginToken($autoLoginToken);

        if (is_null($account))
        {
            return $this->handleFailedLogin(null);
        }

        return $this->handleLogin($account, false, false, true);
    }

    protected function generateSessionValue($accountId, $timestamp)
    {
        return base64_encode($accountId . ':' . $timestamp . ':' . $this->generateSessionSignature($accountId, $timestamp));
    }

    protected function generateSessionSignature($accountId, $timestamp)
    {
        $stringToHash = $accountId . $timestamp . $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'];

        return hash_hmac('SHA1', $stringToHash, self::$SESSION_SECRET);
    }

    private function clearSession()
    {
        $this->getSessionManager()->clear(self::$SESSION_NAME);
        $this->getCookieManager()->clear(self::$SESSION_NAME);
    }

    /**
     * @param $password
     * @return string
     */
    private function generateNewPassword($password, $salt)
    {
        return $this->encryptor->encrypt($password, $salt);
    }

    /**
     * @param Account $account
     * @return string
     */
    private function generateNewAutoLoginTokenHash(Account $account, $salt)
    {
        return sha1($salt . $account->getEmail() . $account->getPassword() . time());
    }

    /**
     * @param $rawPassword
     * @param $hashedPassword
     * @param $salt
     * @return bool
     */
    private function verifyPassword($rawPassword, $hashedPassword, $salt)
    {
        return $this->encryptor->isValid($rawPassword, $hashedPassword, $salt);
    }

    /**
     * @param Account $account
     * @param $accountStatus
     * @param bool $isPasswordless
     * @return mixed|null
     * @throws ValidationException
     */
    private function createAccount(Account $account, $isPasswordless = false)
    {
        $account->setSalt($this->encryptor->generateSalt());

        if ($isPasswordless)
        {
            $this->getValidator()->validateCreateWithoutPassword($account);
            $account->setPassword(null);
        }
        else
        {
            $this->getValidator()->validateCreate($account);

            $account->setPassword($this->generateNewPassword($account->getPassword(), $account->getSalt()));
        }

        $account->setAccountId(null);

        // default account status in PENDING
        if (is_null($account->getStatus()))
        {
            $account->setStatus(AccountStatusType::PENDING);
        }

        // default account role is 1
        if (is_null($account->getRole()))
        {
            $account->setRole(1);
        }

        if ($account->getStatus() != AccountStatusType::ACTIVE)
        {
            $verificationCode = sha1($account->getEmail() . $account->getPassword() . $account->getSalt());
            $account->setVerificationCode($verificationCode);
        }

        if (!is_null($account->getRegistrationSource()))
        {
            $account->setRegistrationSource(strtolower($account->getRegistrationSource()));
        }

        $account->setAutoLoginToken($this->generateNewAutoLoginTokenHash($account, $account->getSalt()));

        $account->setRegistrationIP(IPAddressUtility::get());

        $createdAccount = $this->getDao()->createRecord($account, true);

        if (!is_null($createdAccount))
        {
            $this->getEventDispatcher()->dispatch(new AccountCreatedEvent($createdAccount), 'Account created successfully.');
        }
        else
        {
            $this->getEventDispatcher()->dispatch(new AccountCreatedFailedEvent($account), 'There was a problem creating your account.');
        }

        return $createdAccount;
    }

    /**
     * @param SocialUserDetails $socialUserDetails
     * @return Account
     * @throws \Exception
     */
    private function createAccountFromSocialUserDetails(SocialUserDetails $socialUserDetails, $providerName)
    {
        $newAccount = new Account();
        $newAccount->setEmail($socialUserDetails->getEmail());
        $newAccount->setFirstName($socialUserDetails->getFirstName());
        $newAccount->setLastName($socialUserDetails->getLastName());
        $newAccount->setRegistrationSource($providerName);

        $createdAccount = $this->createWithoutPassword($newAccount, AccountStatusType::ACTIVE);

        if (is_null($createdAccount))
        {
            throw new \Exception('There was a problem creating your account.');
        }

        return $createdAccount;
    }

    /**
     * @param Account|null $account
     * @param bool $isSocialLogin
     * @return bool
     */
    private function handleFailedLogin(Account $account = null, $isSocialLogin = false)
    {
        if ($isSocialLogin)
        {
            $this->getEventDispatcher()->dispatch(new SocialAccountLoginFailedEvent(), 'There was a problem logging in with your social account.');
        }
        else
        {
            $this->getEventDispatcher()->dispatch(new AccountLoginFailedEvent($account), 'Invalid account credentials.');
        }

        return false;
    }

    /**
     * @param Account $account
     * @param $storeCookie
     * @param bool $isSocialLogin
     * @return bool
     */
    private function handleLogin(Account $account, $storeCookie, $isSocialLogin = false, $isAutoLogin = false)
    {
        if (!$this->getDao()->checkIfEmailOrUsernameValidAndActive($account))
        {
            return $this->handleFailedLogin($account, $isSocialLogin);
        }

        if (!$isAutoLogin)
        {
            $accountColumnToQuery = 'email';
            $accountValueToQuery = $account->getEmail();

            if (is_null($account->getEmail()))
            {
                $accountColumnToQuery = 'username';
                $accountValueToQuery = $account->getUsername();
            }

            /** @var Account $realAccount */
            $realAccount = $this->getDao()->getAllBy($accountColumnToQuery, $accountValueToQuery, 1);

            if (!$this->verifyPassword($account->getPassword(), $realAccount->getPassword(), $realAccount->getSalt()))
            {
                return $this->handleFailedLogin($account, $isSocialLogin);
            }
        }
        else
        {
            $realAccount = $account;
        }

        $realAccount->setDateLastLogin($this->getMySQLNow());
        $realAccount->setLastLoginIP(IPAddressUtility::get());

        $updatedAccount = $this->update($realAccount, $realAccount->getAccountId());

        $now = time();
        $sessionValue = $this->generateSessionValue($updatedAccount->getAccountId(), $now);

        $this->getSessionManager()->write(self::$SESSION_NAME, $sessionValue);

        if ($storeCookie)
        {
            $this->getCookieManager()->write(self::$SESSION_NAME, $sessionValue, ($now + (86400 * (int) self::$SESSION_EXPIRY)), '/');
        }

        if ($isSocialLogin)
        {
            $this->getEventDispatcher()->dispatch(new AccountSocialLoginEvent($realAccount), 'Social account logged in successfully.');
        }
        else
        {
            $this->getEventDispatcher()->dispatch(new AccountLoginEvent($realAccount), 'Account logged in successfully.');
        }

        return true;
    }

    /**
     * SOCIAL LOGIN SCENARIOS
     * 1) social account exists but normal account does not - throw exception
     * 2) no social account or normal account exists - create account then create social account
     * 3) normal account exists but no social account - create social account
     * 4) both social and normal account exists - do nothing
     *
     * @param SocialClient $socialClient
     * @param $providerName
     * @return bool
     * @throws \Exception
     */
    private function handleSocialLogin(SocialClient $socialClient, $providerName, $storeCookie = true, $shouldCreateAccount = true)
    {
        $accessToken = $socialClient->getAccessToken();

        if (is_null($accessToken) || is_null($accessToken->getAccessToken()))
        {
            return false;
        }

        $userDetails = $socialClient->getUserDetails($accessToken);

        $exisingSocialAccount = $this->socialAccountService->getByEmailAndProviderName($userDetails->getEmail(), $providerName);
        $existingAccount = $this->getByEmail($userDetails->getEmail());

        if (is_null($existingAccount) && !is_null($exisingSocialAccount))
        {
            throw new \Exception('Social account (' . $exisingSocialAccount->getId() . ') does not have an existing account associated with it. This should not happen ever.');
        }

        if (is_null($existingAccount) && is_null($exisingSocialAccount))
        {
            if (!$shouldCreateAccount)
            {
                return false;
            }

            $createdAccount = $this->createAccountFromSocialUserDetails($userDetails, $providerName);

            $newSocialAccount = new SocialAccount();
            $newSocialAccount->setEmail($userDetails->getEmail());

            $createdSocialAccount = $this->socialAccountService->createFromAccount($createdAccount, $providerName, $userDetails->getId(), $accessToken, false);
            if (is_null($createdSocialAccount))
            {
                throw new \Exception('Something went wrong creating the new social account');
            }

            $existingAccount = $createdAccount;
        }
        else if (!is_null($existingAccount) && is_null($exisingSocialAccount))
        {
            $createdSocialAccount = $this->socialAccountService->createFromAccount($existingAccount, $providerName, $userDetails->getId(), $accessToken, true);

            if (is_null($createdSocialAccount))
            {
                throw new \Exception('Something went wrong creating the new social account');
            }
        }

        return $this->handleLogin($existingAccount, $storeCookie, true);
    }

}