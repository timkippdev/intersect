<?php

namespace TimKipp\Intersect\Services;

class EncryptionService implements Encryptor {

    /**
     * @param $valueToEncrypt
     * @return mixed
     * @throws \Exception
     */
    public function encrypt($valueToEncrypt, $salt = null)
    {
        return password_hash(($salt . $valueToEncrypt), PASSWORD_BCRYPT, array('cost' => 8));
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function generateSalt()
    {
        return bin2hex(random_bytes(24));
    }

    public function isValid($value, $encryptedValue, $salt = null)
    {
        return password_verify($salt . $value, $encryptedValue);
    }

}