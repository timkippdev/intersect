<?php

namespace TimKipp\Intersect\Services;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;
use TimKipp\Intersect\Database\Dao\SocialAccountDao;
use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Domain\SocialAccount;
use TimKipp\Intersect\Event\SocialAccountCreatedEvent;
use TimKipp\Intersect\Event\SocialAccountLinkedEvent;
use TimKipp\Intersect\Social\SocialAccessToken;

/**
 * Class SocialAccountService
 * @package TimKipp\Intersect\Services
 *
 * @method SocialAccountDao getDao
 */
class SocialAccountService extends AbstractService {

    /** @var SocialProviderService $socialProviderService */
    protected $socialProviderService;

    /**
     * SocialAccountService constructor.
     * @param AdapterInterface $databaseAdapter
     */
    public function __construct(AdapterInterface $databaseAdapter)
    {
        parent::__construct($databaseAdapter);

        $this->setDao(new SocialAccountDao($databaseAdapter));

        $this->socialProviderService = new SocialProviderService($databaseAdapter);
    }

    /**
     * @param $providerId
     * @return mixed
     */
    public function getAllForProviderId($providerId)
    {
        return $this->getDao()->getAllForProviderId($providerId);
    }

    /**
     * @param $email
     * @param $providerName
     * @return SocialAccount|null
     */
    public function getByEmailAndProviderName($email, $providerName)
    {
        $provider = $this->socialProviderService->getByName($providerName);

        return $this->getDao()->getByEmailAndProviderId($email, (!is_null($provider) ? $provider->getProviderId() : null));
    }

    /**
     * @param Account $account
     * @param $providerName
     * @param $providerAccountId
     * @param SocialAccessToken $accessToken
     * @param $linkingToExistingAccount
     * @return mixed|null
     */
    public function createFromAccount(Account $account, $providerName, $providerAccountId, SocialAccessToken $accessToken, $linkingToExistingAccount)
    {
        $socialAccount = new SocialAccount();
        $socialAccount->setAccountId($account->getAccountId());
        $socialAccount->setEmail($account->getEmail());
        $socialAccount->setProviderAccountId($providerAccountId);
        $socialAccount->setProviderAccessToken($accessToken->getAccessToken());
        $socialAccount->setProviderAccessTokenSecret($accessToken->getAccessTokenSecret());
        $socialAccount->setStatus(1);

        $provider = $this->socialProviderService->getByName($providerName);

        $socialAccount->setProviderId($provider->getProviderId());

        $createdSocialAccount = $this->create($socialAccount);

        if (!is_null($createdSocialAccount))
        {
            if ($linkingToExistingAccount)
            {
                $this->getEventDispatcher()->dispatch(new SocialAccountLinkedEvent($account, $createdSocialAccount), 'Social account was linked successfully.');
            }
            else
            {
                $this->getEventDispatcher()->dispatch(new SocialAccountCreatedEvent($createdSocialAccount), 'Social account created successfully.');
            }
        }

        return $createdSocialAccount;
    }

}