<?php

namespace TimKipp\Intersect\Services;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;
use TimKipp\Intersect\Database\Dao\SocialProviderDao;
use TimKipp\Intersect\Domain\SocialProvider;

/**
 * Class SocialProviderService
 * @package TimKipp\Intersect\Services
 *
 * @method SocialProviderDao getDao
 */
class SocialProviderService extends AbstractService {

    /**
     * SocialProviderService constructor.
     * @param AdapterInterface $databaseAdapter
     */
    public function __construct(AdapterInterface $databaseAdapter)
    {
        parent::__construct($databaseAdapter);

        $this->setDao(new SocialProviderDao($databaseAdapter));
    }

    /**
     * @param $providerName
     * @return null|SocialProvider
     */
    public function getByName($providerName)
    {
        return $this->getDao()->getByName($providerName);
    }

}