<?php

namespace TimKipp\Intersect\Services;

interface Encryptor {

    /**
     * @param $valueToEncrypt
     * @param null $salt
     * @return string
     */
    public function encrypt($valueToEncrypt, $salt = null);

    /**
     * @return string
     */
    public function generateSalt();

    /**
     * @param $value
     * @param $encryptedValue
     * @return bool
     */
    public function isValid($value, $encryptedValue, $salt = null);

}