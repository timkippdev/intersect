<?php

namespace TimKipp\Intersect\Services;

use TimKipp\Intersect\Domain\AbstractDomain;

/**
 * Interface ServiceInterface
 * @package TimKipp\Intersect\Services
 */
interface ServiceInterface {

    /**
     * @param AbstractDomain $obj
     * @return AbstractDomain|null
     */
    public function create(AbstractDomain $obj);

    /**
     * @param AbstractDomain $obj
     * @return null
     */
    public function delete(AbstractDomain $obj);

    /**
     * @param AbstractDomain $obj
     * @param $objId
     * @return AbstractDomain
     */
    public function update(AbstractDomain $obj, $objId);

    /**
     * @return array
     */
    public function getAll();

    /**
     * @param $column
     * @param $value
     * @param int|null $limit
     * @return mixed
     */
    public function getAllBy($column, $value, int $limit = null);

    /**
     * @param $id
     * @return AbstractDomain
     */
    public function getById($id);

    /**
     * @param $ids
     * @return array
     */
    public function getByIds(array $ids);

}