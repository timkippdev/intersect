<?php

namespace TimKipp\Intersect\Services;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;
use TimKipp\Intersect\Database\Dao\DaoInterface;
use TimKipp\Intersect\Database\Dao\NullDao;
use TimKipp\Intersect\Domain\AbstractDomain;
use TimKipp\Intersect\Event\Dispatcher;
use TimKipp\Intersect\Helper\CookieManager;
use TimKipp\Intersect\Helper\SessionManager;
use TimKipp\Intersect\Validation\NullValidator;
use TimKipp\Intersect\Validation\Validator;

/**
 * Class AbstractService
 * @package TimKipp\Intersect\Services
 */
abstract class AbstractService implements ServiceInterface {

    /** @var CookieManager $cookieManager */
    private $cookieManager;

    /** @var AdapterInterface $adapterInterface */
    private $databaseAdapter;

    /** @var DaoInterface $dao */
    private $dao;

    /** @var SessionManager $sessionManager */
    private $sessionManager;

    /** @var Dispatcher $eventDispatcher */
    private $eventDispatcher;

    /**
     * AbstractService constructor.
     * @param AdapterInterface $databaseAdapter
     */
    public function __construct(AdapterInterface $databaseAdapter)
    {
        $this->databaseAdapter = $databaseAdapter;
        $this->dao = new NullDao($databaseAdapter);

        $this->sessionManager = new SessionManager();
        $this->cookieManager = new CookieManager();

        $this->eventDispatcher = Dispatcher::get();
    }

    /**
     * @return DaoInterface|NullDao
     */
    public function getDao()
    {
        return $this->dao;
    }
    
    /**
     * @param DaoInterface $dao
     */
    public function setDao(DaoInterface $dao)
    {
        $this->dao = $dao;
    }

    /**
     * @return AdapterInterface
     */
    public function getDatabaseAdapter()
    {
        return $this->databaseAdapter;
    }

    /**
     * @return NullValidator|Validator
     */
    public function getValidator()
    {
        return $this->getDao()->getValidator();
    }

    /**
     * @return CookieManager
     */
    public function getCookieManager()
    {
        return $this->cookieManager;
    }

    /**
     * @param CookieManager $cookieManager
     */
    public function setCookieManager(CookieManager $cookieManager)
    {
        $this->cookieManager = $cookieManager;
    }

    /**
     * @return SessionManager
     */
    public function getSessionManager()
    {
        return $this->sessionManager;
    }

    /**
     * @param SessionManager $sessionManager
     */
    public function setSessionManager(SessionManager $sessionManager)
    {
        $this->sessionManager = $sessionManager;
    }

    /**
     * @return Dispatcher|static
     */
    public function getEventDispatcher()
    {
        return $this->eventDispatcher;
    }

    /**
     * @param Dispatcher $eventDispatcher
     */
    public function setEventDispatcher(Dispatcher $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param null $timestamp
     * @return false|string
     */
    public function getMySQLNow($timestamp = null)
    {
        return date('Y-m-d H:i:s', (is_null($timestamp) ? time() : $timestamp));
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->getDao()->getAll();
    }

    /**
     * @param $column
     * @param $value
     * @param int|null $limit
     * @return mixed|null
     */
    public function getAllBy($column, $value, int $limit = null)
    {
        return $this->getDao()->getAllBy($column, $value, $limit);
    }

    /**
     * @param $id
     * @return mixed|null
     */
    public function getById($id)
    {
        return $this->getDao()->getById($id);
    }

    /**
     * @param $ids
     * @return array
     */
    public function getByIds(array $ids)
    {
        return $this->getDao()->getByIds($ids);
    }

    /**
     * @param AbstractDomain $obj
     * @return mixed|null
     */
    public function create(AbstractDomain $obj)
    {
        return $this->getDao()->createRecord($obj);
    }

    /**
     * @param AbstractDomain $obj
     * @return bool|null
     */
    public function delete(AbstractDomain $obj)
    {
        return $this->getDao()->deleteRecord($obj);
    }

    /**
     * @param AbstractDomain $obj
     * @param $objId
     * @return mixed|null
     */
    public function update(AbstractDomain $obj, $objId)
    {
        return $this->getDao()->updateRecord($obj, $objId);
    }
}