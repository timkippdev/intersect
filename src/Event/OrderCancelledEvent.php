<?php

namespace TimKipp\Intersect\Event;

use TimKipp\Intersect\Order\Domain\Order;

/**
 * Class OrderCancelledEvent
 * @package TimKipp\Intersect\Event
 */
class OrderCancelledEvent extends Event {

    private $cancelledOrder;

    /**
     * OrderCancelledEvent constructor.
     * @param Order $cancelledOrder
     */
    public function __construct(Order $cancelledOrder)
    {
        $this->cancelledOrder = $cancelledOrder;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Event::ORDER_CANCELLED;
    }

    /**
     * @return Order
     */
    public function getCancelledOrder()
    {
        return $this->cancelledOrder;
    }

}