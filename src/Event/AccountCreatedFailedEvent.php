<?php

namespace TimKipp\Intersect\Event;

use TimKipp\Intersect\Domain\Account;

/**
 * Class AccountCreatedFailedEvent
 * @package TimKipp\Intersect\Event
 */
class AccountCreatedFailedEvent extends AbstractAccountEvent {

    /**
     * @return string
     */
    public function getName()
    {
        return Event::ACCOUNT_CREATED_FAILED;
    }

}