<?php

namespace TimKipp\Intersect\Event;

/**
 * Class AccountResetPasswordFailedEvent
 * @package TimKipp\Intersect\Event
 */
class AccountResetPasswordFailedEvent extends AbstractAccountEvent {

    /**
     * @return string
     */
    public function getName()
    {
        return Event::ACCOUNT_PASSWORD_RESET_FAILED;
    }

}