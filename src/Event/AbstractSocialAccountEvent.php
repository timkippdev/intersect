<?php

namespace TimKipp\Intersect\Event;

use TimKipp\Intersect\Domain\SocialAccount;

/**
 * Class AbstractSocialAccountEvent
 * @package TimKipp\Intersect\Event
 */
abstract class AbstractSocialAccountEvent extends Event {

    private $socialAccount;

    /**
     * AbstractSocialAccountEvent constructor.
     * @param SocialAccount $socialAccount
     */
    public function __construct(SocialAccount $socialAccount = null)
    {
        $this->socialAccount = $socialAccount;
    }

    /**
     * @return SocialAccount
     */
    public function getSocialAccount()
    {
        return $this->socialAccount;
    }

}