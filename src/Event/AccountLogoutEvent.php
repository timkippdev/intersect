<?php

namespace TimKipp\Intersect\Event;

/**
 * Class AccountLogoutEvent
 * @package TimKipp\Intersect\Event
 */
class AccountLogoutEvent extends AbstractAccountEvent {

    /**
     * @return string
     */
    public function getName()
    {
        return Event::ACCOUNT_LOGOUT;
    }

}