<?php

namespace TimKipp\Intersect\Event;

/**
 * Class AccountResetPasswordEvent
 * @package TimKipp\Intersect\Event
 */
class AccountResetPasswordEvent extends AbstractAccountEvent {

    /**
     * @return string
     */
    public function getName()
    {
        return Event::ACCOUNT_PASSWORD_RESET;
    }

}