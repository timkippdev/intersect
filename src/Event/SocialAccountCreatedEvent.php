<?php

namespace TimKipp\Intersect\Event;

/**
 * Class SocialAccountCreatedEvent
 * @package TimKipp\Intersect\Event
 */
class SocialAccountCreatedEvent extends AbstractSocialAccountEvent {

    /**
     * @return string
     */
    public function getName()
    {
        return Event::SOCIAL_ACCOUNT_CREATED;
    }

}