<?php

namespace TimKipp\Intersect\Event;

/**
 * Class EmailSentFailedEvent
 * @package TimKipp\Intersect\Event
 */
class EmailSentFailedEvent extends AbstractEmailEvent {

    /**
     * @return mixed|string
     */
    public function getName()
    {
        return Event::EMAIL_SENT_FAILED;
    }

}