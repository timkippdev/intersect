<?php

namespace TimKipp\Intersect\Event;

use TimKipp\Intersect\Domain\Account;

/**
 * Class AbstractAccountEvent
 * @package TimKipp\Intersect\Event
 */
abstract class AbstractAccountEvent extends Event {

    private $account;

    /**
     * AccountLoginEvent constructor.
     * @param Account $account
     */
    public function __construct(Account $account = null)
    {
        $this->account = $account;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

}