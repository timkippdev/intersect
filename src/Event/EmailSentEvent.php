<?php

namespace TimKipp\Intersect\Event;

/**
 * Class EmailSentEvent
 * @package TimKipp\Intersect\Event
 */
class EmailSentEvent extends AbstractEmailEvent {

    /**
     * @return mixed|string
     */
    public function getName()
    {
        return Event::EMAIL_SENT;
    }

}