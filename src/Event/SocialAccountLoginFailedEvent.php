<?php

namespace TimKipp\Intersect\Event;

/**
 * Class SocialAccountLoginFailedEvent
 * @package TimKipp\Intersect\Event
 */
class SocialAccountLoginFailedEvent extends AbstractSocialAccountEvent {

    /**
     * @return string
     */
    public function getName()
    {
        return Event::SOCIAL_ACCOUNT_LOGIN_FAILED;
    }

}