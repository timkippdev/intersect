<?php

namespace TimKipp\Intersect\Event;

use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Domain\SocialAccount;

/**
 * Class SocialAccountLinkedEvent
 * @package TimKipp\Intersect\Event
 */
class SocialAccountLinkedEvent extends AbstractSocialAccountEvent {

    private $accountLinkedTo;

    /**
     * SocialAccountLinkedEvent constructor.
     * @param Account $account
     * @param SocialAccount $createdSocialAccount
     */
    public function __construct(Account $account, SocialAccount $createdSocialAccount)
    {
        parent::__construct($createdSocialAccount);
        $this->accountLinkedTo = $account;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Event::SOCIAL_ACCOUNT_LINKED;
    }

    /**
     * @return Account
     */
    public function getAccountLinkedTo()
    {
        return $this->accountLinkedTo;
    }

}