<?php

namespace TimKipp\Intersect\Event;

use TimKipp\Intersect\Domain\Account;

/**
 * Class AccountLoginFailedEvent
 * @package TimKipp\Intersect\Event
 */
class AccountLoginFailedEvent extends AbstractAccountEvent {

    /**
     * @return string
     */
    public function getName()
    {
        return Event::ACCOUNT_LOGIN_FAILED;
    }

}