<?php

namespace TimKipp\Intersect\Event;

/**
 * Class Dispatcher
 * @package TimKipp\Intersect\Event
 */
class Dispatcher {

    private static $instance;

    private $listeners = array();

    /**
     * Dispatcher constructor.
     */
    private function __construct() {}

    /**
     * @return static
     */
    public static function get()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
     * @param Event $event
     */
    public function dispatch(Event $event, $customMessage = null)
    {
        foreach ($this->getListenersByEventName($event->getName()) as $listener)
        {
            $listener->handle($event, $customMessage);
        }
    }

    /**
     * @return array
     */
    public function getListeners()
    {
        return $this->listeners;
    }

    /**
     * @param $eventName
     * @return array|mixed
     */
    public function getListenersByEventName($eventName)
    {
        if (!$this->hasListenersForEventName($eventName))
        {
            return array();
        }

        return $this->listeners[$eventName];
    }

    /**
     * @param $eventName
     * @return bool
     */
    public function hasListenersForEventName($eventName)
    {
        return isset($this->listeners[$eventName]);
    }

    /**
     * @param $eventName
     * @param Listener $listener
     */
    public function addListener($eventName, Listener $listener)
    {
        $this->listeners[$eventName][] = $listener;
    }

}