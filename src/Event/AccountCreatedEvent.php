<?php

namespace TimKipp\Intersect\Event;

use TimKipp\Intersect\Domain\Account;

/**
 * Class AccountCreatedEvent
 * @package TimKipp\Intersect\Event
 */
class AccountCreatedEvent extends AbstractAccountEvent {

    /**
     * @return string
     */
    public function getName()
    {
        return Event::ACCOUNT_CREATED;
    }

}