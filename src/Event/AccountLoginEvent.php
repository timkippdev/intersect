<?php

namespace TimKipp\Intersect\Event;

use TimKipp\Intersect\Domain\Account;

/**
 * Class AccountLoginEvent
 * @package TimKipp\Intersect\Event
 */
class AccountLoginEvent extends AbstractAccountEvent {

    /**
     * @return string
     */
    public function getName()
    {
        return Event::ACCOUNT_LOGIN;
    }

}