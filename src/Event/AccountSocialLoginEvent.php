<?php

namespace TimKipp\Intersect\Event;

/**
 * Class AccountSocialLoginEvent
 * @package TimKipp\Intersect\Event
 */
class AccountSocialLoginEvent extends AbstractAccountEvent {

    /**
     * @return string
     */
    public function getName()
    {
        return Event::SOCIAL_ACCOUNT_LOGIN;
    }

}