<?php

namespace TimKipp\Intersect\Event;

use TimKipp\Intersect\Order\Domain\Order;
use TimKipp\Intersect\Order\Domain\Payment;

/**
 * Class PaymentCreatedEvent
 * @package TimKipp\Intersect\Event
 */
class PaymentCreatedEvent extends Event {

    private $createdPayment;
    private $order;

    /**
     * PaymentCreatedEvent constructor.
     * @param Payment $createdPayment
     * @param Order $order
     */
    public function __construct(Payment $createdPayment, Order $order = null)
    {
        $this->createdPayment = $createdPayment;
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Event::PAYMENT_CREATED;
    }

    /**
     * @return Payment
     */
    public function getCreatedPayment()
    {
        return $this->createdPayment;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

}