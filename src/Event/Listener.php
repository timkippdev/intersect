<?php

namespace TimKipp\Intersect\Event;

/**
 * Class Listener
 * @package TimKipp\Intersect\Event
 */
abstract class Listener {

    /**
     * @param Event $event
     * @param null $customMessage
     * @return mixed
     */
    abstract public function handle(Event $event, $customMessage = null);

}