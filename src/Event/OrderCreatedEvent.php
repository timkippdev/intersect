<?php

namespace TimKipp\Intersect\Event;

use TimKipp\Intersect\Order\Domain\Order;

/**
 * Class OrderCreatedEvent
 * @package TimKipp\Intersect\Event
 */
class OrderCreatedEvent extends Event {

    private $createdOrder;

    /**
     * OrderCreatedEvent constructor.
     * @param Order $createdOrder
     */
    public function __construct(Order $createdOrder)
    {
        $this->createdOrder = $createdOrder;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Event::ORDER_CREATED;
    }

    /**
     * @return Order
     */
    public function getCreatedOrder()
    {
        return $this->createdOrder;
    }

}