<?php

namespace TimKipp\Intersect\Event;

use TimKipp\Intersect\Email\Domain\EmailHeaders;

/**
 * Class AbstractEmailEvent
 * @package TimKipp\Intersect\Event
 */
abstract class AbstractEmailEvent extends Event {

    private $recipient;
    private $subject;
    private $message;
    private $headers;

    /**
     * EmailSentEvent constructor.
     * @param $recipient
     * @param $subject
     * @param $message
     * @param EmailHeaders $headers
     */
    public function __construct($recipient, $subject, $message, EmailHeaders $headers)
    {
        $this->recipient = $recipient;
        $this->subject = $subject;
        $this->message = $message;
        $this->headers = $headers;
    }

    /**
     * @return mixed
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return EmailHeaders
     */
    public function getHeaders()
    {
        return $this->headers;
    }

}