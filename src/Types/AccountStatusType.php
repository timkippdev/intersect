<?php

namespace TimKipp\Intersect\Types;

/**
 * Class AccountStatusType
 * @package TimKipp\Intersect\Types
 */
class AccountStatusType {

    const PENDING = 1;
    const ACTIVE = 2;
    const DEACTIVATED = 3;

}