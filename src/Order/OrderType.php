<?php

namespace TimKipp\Intersect\Order;

class OrderType {

    const ONLINE = 1;
    const INVOICE = 2;

}