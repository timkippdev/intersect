<?php

namespace TimKipp\Intersect\Order\Dao;

use TimKipp\Intersect\Database\Dao\AbstractDao;
use TimKipp\Intersect\Order\Domain\Order;
use TimKipp\Intersect\Order\Validation\OrderValidator;

/**
 * Class OrderDao
 * @package TimKipp\Intersect\Order\Dao
 */
class OrderDao extends AbstractDao {

    /**
     * @return string
     */
    protected function getTable()
    {
        return 'order';
    }

    /**
     * @return string
     */
    public function getDomainClass()
    {
        return Order::class;
    }

    /**
     * @return OrderValidator
     */
    public function getValidator()
    {
        return new OrderValidator();
    }

    /**
     * @param $accountId
     * @return array
     */
    public function getOrdersForAccountId($accountId)
    {
        $results = $this->getAdapter()->query("SELECT * FROM `" . $this->getTable() . "` WHERE account_id=:account_id ORDER BY date_created ASC", array(
            'account_id' => (int) $accountId)
        );

        return $this->convertResultToDomainClass($results)->getRecords();
    }

}