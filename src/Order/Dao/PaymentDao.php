<?php

namespace TimKipp\Intersect\Order\Dao;

use TimKipp\Intersect\Database\Dao\AbstractDao;
use TimKipp\Intersect\Order\Domain\Payment;
use TimKipp\Intersect\Order\Validation\PaymentValidator;

/**
 * Class PaymentDao
 * @package TimKipp\Intersect\Order\Dao
 */
class PaymentDao extends AbstractDao {

    /**
     * @return string
     */
    protected function getTable()
    {
        return 'order_payment';
    }

    /**
     * @return string
     */
    public function getDomainClass()
    {
        return Payment::class;
    }

    /**
     * @return PaymentValidator
     */
    public function getValidator()
    {
        return new PaymentValidator();
    }

}