<?php

namespace TimKipp\Intersect\Order\Dao;

use TimKipp\Intersect\Database\Dao\AbstractDao;
use TimKipp\Intersect\Order\Domain\PaymentType;

/**
 * Class PaymentTypeDao
 * @package TimKipp\Intersect\Order\Dao
 */
class PaymentTypeDao extends AbstractDao {

    /**
     * @return string
     */
    protected function getTable()
    {
        return 'order_payment_type';
    }

    /**
     * @return string
     */
    public function getDomainClass()
    {
        return PaymentType::class;
    }

}