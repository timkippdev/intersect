<?php

namespace TimKipp\Intersect\Utility;

/**
 * Class DatabaseUtility
 * @package TimKipp\Intersect\Utility
 */
class DatabaseUtility {

    /**
     * @param $sql
     * @return string
     */
    public static function removeQueryLineBreaks($sql)
    {
        return trim(preg_replace('/\s+/', ' ', $sql));
    }

}