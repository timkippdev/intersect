<?php

namespace TimKipp\Intersect\Utility;

class IPAddressUtility {

    public static function get()
    {
        $ipAddress = null;

        if (isset($_SERVER['HTTP_CLIENT_IP']))
        {
            $ipAddress = $_SERVER['HTTP_CLIENT_IP'];
        }
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ipAddresses = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ipAddresses = array_map('trim', $ipAddresses);

            if (count($ipAddresses) > 0)
            {
                $ipAddress = $ipAddresses[0];
            }
        }
        else if (isset($_SERVER['REMOTE_ADDR']))
        {
            $ipAddress = $_SERVER['REMOTE_ADDR'];
        }

        $ipAddress = filter_var($ipAddress, FILTER_VALIDATE_IP);

        return (is_null($ipAddress) || $ipAddress === false) ? '0.0.0.0' : $ipAddress;
    }

}