<?php

namespace TimKipp\Intersect\Social;

/**
 * Class SocialClientException
 * @package TimKipp\Intersect\Social
 */
class SocialClientException extends \Exception {
}