<?php

namespace TimKipp\Intersect\Social\Facebook;

use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\Helpers\FacebookRedirectLoginHelper;
use TimKipp\Intersect\Social\SocialAccessToken;
use TimKipp\Intersect\Social\SocialClient;
use TimKipp\Intersect\Social\SocialClientException;
use TimKipp\Intersect\Social\SocialUserDetails;

/**
 * Class FacebookClient
 * @package TimKipp\Intersect\Social\Facebook
 */
class FacebookClient implements SocialClient {

    private $loginRedirectUrl;

    /** @var Facebook $facebook */
    private $facebook;

    /** @var \Facebook\Helpers\FacebookRedirectLoginHelper $facebookRedirectLoginHelper */
    private $facebookRedirectLoginHelper;

    /**
     * FacebookClient constructor.
     * @param FacebookConfiguration $facebookConfiguration
     * @throws SocialClientException
     */
    public function __construct(FacebookConfiguration $facebookConfiguration)
    {
        if (is_null($facebookConfiguration->getApplicationId()) || trim($facebookConfiguration->getApplicationId()) == '')
        {
            throw new SocialClientException("FacebookConfiguration applicationId is not set");
        }

        if (is_null($facebookConfiguration->getApplicationSecret()) || trim($facebookConfiguration->getApplicationSecret()) == '')
        {
            throw new SocialClientException("FacebookConfiguration applicationSecret is not set");
        }

        $this->facebook = new Facebook($facebookConfiguration->toFacebookArray());
        $this->facebookRedirectLoginHelper = $this->facebook->getRedirectLoginHelper();
        $this->loginRedirectUrl = $facebookConfiguration->getRedirectUrl();
    }

    /**
     * @return null|SocialAccessToken
     * @throws SocialClientException
     */
    public function getAccessToken()
    {
        try {
            $accessToken = $this->getFacebookRedirectLoginHelper()->getAccessToken();
        } catch (FacebookSDKException $e) {
            throw new SocialClientException($e->getMessage());
        }

        return new SocialAccessToken($accessToken);
    }

    /**
     * @param SocialAccessToken|null $accessToken
     * @return mixed|SocialUserDetails
     * @throws FacebookSDKException
     * @throws SocialClientException
     */
    public function getUserDetails(SocialAccessToken $accessToken = null)
    {
        $response = $this->getFacebook()->get('/me?fields=id,email,first_name,last_name', $accessToken->getAccessToken());

        if (is_null($response))
        {
            throw new SocialClientException('Something went wrong retrieving user details from Facebook');
        }

        $facebookUser = $response->getGraphUser();

        if (is_null($facebookUser))
        {
            throw new SocialClientException('Something went wrong retrieving user details from Facebook');
        }

        if (is_null($facebookUser->getEmail()) || trim($facebookUser->getEmail()) == '')
        {
            throw new SocialClientException('A valid Facebook email could not be found');
        }

        $socialUserDetails = new SocialUserDetails();
        $socialUserDetails->setId($facebookUser->getId());
        $socialUserDetails->setEmail($facebookUser->getEmail());
        $socialUserDetails->setFirstName($facebookUser->getFirstName());
        $socialUserDetails->setLastName($facebookUser->getLastName());

        return $socialUserDetails;
    }

    /**
     * @param array $parameters
     * @return string
     */
    public function getLoginUrl(array $parameters = array())
    {
        return $this->facebookRedirectLoginHelper->getLoginUrl($this->loginRedirectUrl, $parameters);
    }

    /**
     * @return \Facebook\Helpers\FacebookRedirectLoginHelper
     */
    protected function getFacebookRedirectLoginHelper()
    {
        return $this->facebookRedirectLoginHelper;
    }

    /**
     * @return Facebook
     */
    protected function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param Facebook $facebook
     */
    protected function setFacebook(Facebook $facebook)
    {
        $this->facebook = $facebook;
    }

    /**
     * @param FacebookRedirectLoginHelper $facebookRedirectLoginHelper
     */
    protected function setFacebookRedirectLoginHelper(FacebookRedirectLoginHelper $facebookRedirectLoginHelper)
    {
        $this->facebookRedirectLoginHelper = $facebookRedirectLoginHelper;
    }

}