<?php

namespace TimKipp\Intersect\Social\Facebook;

use Facebook\HttpClients\FacebookHttpClientInterface;
use Facebook\PersistentData\PersistentDataInterface;
use Facebook\PseudoRandomString\PseudoRandomStringGeneratorInterface;
use Facebook\Url\UrlDetectionInterface;

/**
 * Class FacebookConfiguration
 * @package TimKipp\Intersect\Social\Facebook
 */
class FacebookConfiguration {

    private $applicationId;
    private $applicationSecret;
    private $defaultGraphVersion;
    private $enableBetaMode = false;
    private $httpClientHandler;
    private $persistentDataHandler;
    private $psuedoRandomStringGenerator;
    private $urlDetectionHandler;
    private $redirectUrl;

    /**
     * FacebookConfiguration constructor.
     * @param $applicationId
     * @param $applicationSecret
     * @param string $defaultGraphVersion
     */
    public function __construct($applicationId, $applicationSecret, $redirectUrl, $defaultGraphVersion = 'v2.10')
    {
        $this->applicationId = $applicationId;
        $this->applicationSecret = $applicationSecret;
        $this->defaultGraphVersion = $defaultGraphVersion;
        $this->redirectUrl = $redirectUrl;
    }

    /**
     * This is the acceptable configuration format for the Facebook SDK
     *
     * @return array
     */
    public function toFacebookArray()
    {
        return array(
            'app_id' => $this->getApplicationId(),
            'app_secret' => $this->getApplicationSecret(),
            'default_graph_version' => $this->getDefaultGraphVersion(),
            'enable_beta_mode' => $this->getEnableBetaMode(),
            'http_client_handler' => $this->getHttpClientHandler(),
            'persistent_data_handler' => $this->getPersistentDataHandler(),
            'pseudo_random_string_generator' => $this->getPsuedoRandomStringGenerator(),
            'url_detection_handler' => $this->getUrlDetectionHandler()
        );
    }

    /**
     * @return mixed
     */
    public function getApplicationId()
    {
        return $this->applicationId;
    }

    /**
     * @return mixed
     */
    public function getApplicationSecret()
    {
        return $this->applicationSecret;
    }

    /**
     * @return mixed
     */
    public function getDefaultGraphVersion()
    {
        return $this->defaultGraphVersion;
    }

    /**
     * @return bool
     */
    public function getEnableBetaMode()
    {
        return $this->enableBetaMode;
    }

    /**
     * @param bool $enableBetaMode
     */
    public function setEnableBetaMode(bool $enableBetaMode)
    {
        $this->enableBetaMode = $enableBetaMode;
    }

    /**
     * @return mixed
     */
    public function getHttpClientHandler()
    {
        return $this->httpClientHandler;
    }

    /**
     * @param FacebookHttpClientInterface $httpClientHandler
     */
    public function setHttpClientHandler(FacebookHttpClientInterface $httpClientHandler)
    {
        $this->httpClientHandler = $httpClientHandler;
    }

    /**
     * @return mixed
     */
    public function getPersistentDataHandler()
    {
        return $this->persistentDataHandler;
    }

    /**
     * @param PersistentDataInterface $persistentDataHandler
     */
    public function setPersistentDataHandler(PersistentDataInterface $persistentDataHandler)
    {
        $this->persistentDataHandler = $persistentDataHandler;
    }

    /**
     * @return mixed
     */
    public function getPsuedoRandomStringGenerator()
    {
        return $this->psuedoRandomStringGenerator;
    }

    /**
     * @param PseudoRandomStringGeneratorInterface $psuedoRandomStringGenerator
     */
    public function setPsuedoRandomStringGenerator(PseudoRandomStringGeneratorInterface $psuedoRandomStringGenerator)
    {
        $this->psuedoRandomStringGenerator = $psuedoRandomStringGenerator;
    }

    /**
     * @return mixed
     */
    public function getUrlDetectionHandler()
    {
        return $this->urlDetectionHandler;
    }

    /**
     * @param UrlDetectionInterface $urlDetectionHandler
     */
    public function setUrlDetectionHandler(UrlDetectionInterface $urlDetectionHandler)
    {
        $this->urlDetectionHandler = $urlDetectionHandler;
    }

    /**
     * @return mixed
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

}