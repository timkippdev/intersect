<?php

namespace TimKipp\Intersect\Social;

class SocialAccessToken {

    private $accessToken;
    private $accessTokenSecret;

    public function __construct($accessToken, $accessTokenSecret = null)
    {
        $this->accessToken = $accessToken;
        $this->accessTokenSecret = $accessTokenSecret;
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }

    public function getAccessTokenSecret()
    {
        return $this->accessTokenSecret;
    }

}