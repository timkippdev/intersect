<?php

namespace TimKipp\Intersect\Validation;

/**
 * Class NullValidator
 * @package TimKipp\Intersect\Validation
 */
class NullValidator implements Validator {

    /**
     * @param $obj
     * @return bool
     */
    public function validateCreate($obj)
    {
        return true;
    }

    /**
     * @param $obj
     * @return bool
     */
    public function validateDelete($obj)
    {
        return true;
    }

    /**
     * @param $obj
     * @return bool
     */
    public function validateUpdate($obj)
    {
        return true;
    }
}