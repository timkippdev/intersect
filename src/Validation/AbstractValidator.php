<?php

namespace TimKipp\Intersect\Validation;

/**
 * Class AbstractValidator
 * @package TimKipp\Intersect\Validation
 */
abstract class AbstractValidator implements Validator {

    /**
     * @param $s
     * @return bool
     */
    public function isBlank($s)
    {
        return (is_null($s) || trim($s) == '');
    }

    /**
     * @param $array
     * @return bool
     */
    public function isEmpty($array)
    {
        return (is_null($array) || !is_array($array) || count($array) == 0);
    }

    /**
     * @param $i
     * @return bool
     */
    public function isGreaterThanZero($i)
    {
        return (!is_null($i) && intval($i) > 0);
    }

}