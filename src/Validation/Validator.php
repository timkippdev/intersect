<?php

namespace TimKipp\Intersect\Validation;

/**
 * Interface Validator
 * @package TimKipp\Intersect\Validation
 */
interface Validator {

    /**
     * @param $obj
     * @return mixed
     */
    public function validateCreate($obj);

    /**
     * @param $obj
     * @return mixed
     */
    public function validateDelete($obj);

    /**
     * @param $obj
     * @return mixed
     */
    public function validateUpdate($obj);

}