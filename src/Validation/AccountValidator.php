<?php

namespace TimKipp\Intersect\Validation;

use TimKipp\Intersect\Domain\Account;

/**
 * Class AccountValidator
 * @package TimKipp\Intersect\Validation
 */
class AccountValidator extends AbstractValidator {

    /**
     * @param $account
     * @return bool
     * @throws ValidationException
     */
    public function validateCreate($account)
    {
        /** @var Account $account */

        if ($this->isBlank($account->getEmail()) || !$this->validateEmail($account->getEmail()))
        {
            throw new ValidationException('Please enter a valid email address');
        }
        if ($this->isBlank($account->getPassword()))
        {
            throw new ValidationException('Please enter a password');
        }

        return true;
    }

    /**
     * @param $account
     * @return bool
     * @throws ValidationException
     */
    public function validateCreateWithoutPassword($account)
    {
        /** @var Account $account */

        if ($this->isBlank($account->getEmail()) || !$this->validateEmail($account->getEmail()))
        {
            throw new ValidationException('Please enter a valid email address');
        }

        return true;
    }

    /**
     * @param $account
     * @return bool
     * @throws ValidationException
     */
    public function validateDelete($account)
    {
        /** @var Account $account */

        if (!$this->isGreaterThanZero($account->getAccountId()))
        {
            throw new ValidationException('A valid account ID is required');
        }

        return true;
    }

    /**
     * @param $account
     * @return bool
     * @throws ValidationException
     */
    public function validateUpdate($account)
    {
        /** @var Account $account */

        if (!$this->isGreaterThanZero($account->getAccountId()))
        {
            throw new ValidationException('A valid account ID is required');
        }

        return true;
    }

    /**
     * @param $email
     * @return mixed
     */
    protected function validateEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}