<?php

class Migration_3_0_0 extends \TimKipp\Intersect\Migration\AbstractMigration {

    public function getVersion()
    {
        return '3.0.0-intersect';
    }

    public function migrateUp()
    {
        $this->getAdapter()->query("
            CREATE TABLE IF NOT EXISTS `social_provider` (
              `provider_id` INT(11) NOT NULL AUTO_INCREMENT,
              `provider_name` VARCHAR(255) NOT NULL,
              PRIMARY KEY (`provider_id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;
        ");

        $this->getAdapter()->query("
            CREATE TABLE IF NOT EXISTS `social_account` (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `provider_id` INT(4) NOT NULL,
              `provider_account_id` VARCHAR(255),
              `provider_access_token` TEXT,
              `provider_access_token_secret` TEXT,
              `account_id` INT(11) NOT NULL,
              `email` VARCHAR(255) NOT NULL,
              `status` TINYINT(2) NOT NULL DEFAULT 1,
              `date_created` DATETIME NOT NULL,
              `date_updated` DATETIME,
              PRIMARY KEY (`id`),
              UNIQUE KEY unique_idx_email_provider_id (`email`, `provider_id`),
              FOREIGN KEY (`account_id`) REFERENCES account (`account_id`),
              FOREIGN KEY (`provider_id`) REFERENCES social_provider (`provider_id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;
        ");

        $this->getAdapter()->query("INSERT INTO `social_provider` (`provider_name`) VALUES ('facebook')");
        $this->getAdapter()->query("INSERT INTO `social_provider` (`provider_name`) VALUES ('twitter')");
        $this->getAdapter()->query("INSERT INTO `social_provider` (`provider_name`) VALUES ('google')");

        $this->getAdapter()->query("ALTER TABLE `account` MODIFY COLUMN `password` VARCHAR(72)");
        $this->getAdapter()->query("ALTER TABLE `account` ADD COLUMN `date_last_login` DATETIME");
        $this->getAdapter()->query("ALTER TABLE `account` ADD COLUMN `registration_source` VARCHAR(100)");
        $this->getAdapter()->query("ALTER TABLE `account` ADD COLUMN `ip_registration` VARCHAR(50)");
        $this->getAdapter()->query("ALTER TABLE `account` ADD COLUMN `ip_last_login` VARCHAR(50)");
        $this->getAdapter()->query("ALTER TABLE `account` ADD COLUMN `salt` VARCHAR(64) AFTER `password`");
        $this->getAdapter()->query("ALTER TABLE `account` ADD COLUMN `password_reset_expiry` DATETIME AFTER `password_reset_code`");

        $this->getAdapter()->query("ALTER TABLE `account` CHANGE COLUMN `token` `session_token` VARCHAR(64)");
        $this->getAdapter()->query("ALTER TABLE `account` ADD COLUMN `auto_login_token` VARCHAR(64) NOT NULL AFTER `session_token`");
        $this->getAdapter()->query("ALTER TABLE `account` ADD UNIQUE INDEX unique_idx_auto_login_token (`auto_login_token`)");

        $this->getAdapter()->query("
            CREATE TABLE IF NOT EXISTS `email_queue` (
              `email_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
              `recipient` VARCHAR (255) NOT NULL,
              `subject` VARCHAR (255),
              `message` TEXT NOT NULL,
              `headers` TEXT NOT NULL,
              `type` VARCHAR (100) NOT NULL,
              `date_created` DATETIME NOT NULL,
              `date_sent` DATETIME,
              PRIMARY KEY (`email_id`),
              INDEX `idx_type` (`type`),
              INDEX `idx_date_sent` (`date_sent`),
              INDEX `idx_recipient` (`recipient`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;
        ");

        $this->getAdapter()->query("
            CREATE TABLE IF NOT EXISTS `order_payment_type` (
              `payment_type_id` INT (11) NOT NULL AUTO_INCREMENT,
              `name` VARCHAR (50) NOT NULL,
              `date_created` DATETIME NOT NULL,
              PRIMARY KEY (`payment_type_id`),
              UNIQUE KEY unique_idx_name (`name`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;
        ");

        $this->getAdapter()->query("INSERT INTO `order_payment_type` (`name`, `date_created`) VALUES ('Cash', NOW())");
        $this->getAdapter()->query("INSERT INTO `order_payment_type` (`name`, `date_created`) VALUES ('Stripe', NOW())");

        $this->getAdapter()->query("
            CREATE TABLE IF NOT EXISTS `order_payment` (
              `payment_id` INT (11) NOT NULL AUTO_INCREMENT,
              `payment_type_id` INT (11) NOT NULL,
              `amount` DECIMAL (7, 2) NOT NULL,
              `confirmation_number` VARCHAR (100) NOT NULL,
              `external_confirmation_number` VARCHAR (255),
              `date_created` DATETIME NOT NULL,
              PRIMARY KEY (`payment_id`),
              INDEX `idx_confirmation_number` (`confirmation_number`),
              INDEX `idx_payment_type_id` (`payment_type_id`),
              UNIQUE KEY unique_idx_confirmation_number (`confirmation_number`),
              FOREIGN KEY (`payment_type_id`) REFERENCES order_payment_type (`payment_type_id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;
        ");

        $this->getAdapter()->query("
            CREATE TABLE IF NOT EXISTS `order` (
              `order_id` INT (11) NOT NULL AUTO_INCREMENT,
              `account_id` INT (11),
              `order_number` VARCHAR (255) NOT NULL,
              `type` VARCHAR (100),
              `details` TEXT NOT NULL,
              `total` DECIMAL (7, 2) NOT NULL,
              `status` TINYINT (2) NOT NULL,
              `payment_id` INT (11),
              `date_created` DATETIME NOT NULL,
              `date_updated` DATETIME,
              `date_payment_received` DATETIME,
              `date_fulfilled` DATETIME,
              `date_cancelled` DATETIME,
              PRIMARY KEY (`order_id`),
              INDEX `idx_account_id` (`account_id`),
              INDEX `idx_type` (`type`),
              UNIQUE KEY unique_idx_order_number (`order_number`),
              FOREIGN KEY (`account_id`) REFERENCES account (`account_id`),
              FOREIGN KEY (`payment_id`) REFERENCES order_payment (`payment_id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;
        ");
    }

    public function migrateDown()
    {
        $this->getAdapter()->query("ALTER TABLE `account` DROP COLUMN `auto_login_token`");
        $this->getAdapter()->query("ALTER TABLE `account` CHANGE COLUMN `session_token` `token` VARCHAR(64)");
        $this->getAdapter()->query("ALTER TABLE `account` DROP COLUMN `registration_source`");

        $this->getAdapter()->dropTable("order");
        $this->getAdapter()->dropTable("order_payment");
        $this->getAdapter()->dropTable("order_payment_type");
        $this->getAdapter()->dropTable("email_queue");
        $this->getAdapter()->dropTable("social_account");
        $this->getAdapter()->dropTable("social_provider");
    }

}