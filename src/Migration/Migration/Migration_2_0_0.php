<?php

class Migration_2_0_0 extends \TimKipp\Intersect\Migration\AbstractMigration {

    public function getVersion()
    {
        return '2.0.0-intersect';
    }

    public function migrateUp()
    {
        $this->getAdapter()->query("
            ALTER TABLE `account` 
            ADD COLUMN `date_created` DATETIME,
            ADD COLUMN `date_updated` DATETIME
        ");

        // apply migration table path column if table not created yet
        $record = $this->getAdapter()->query("SHOW COLUMNS FROM `migrations` LIKE 'path'")->getFirstRecord();
        if (is_null($record))
        {
            $this->getAdapter()->query("ALTER TABLE `migrations` ADD COLUMN `path` VARCHAR(255)");
        }
    }

    public function migrateDown()
    {
        $this->getAdapter()->query("
            ALTER TABLE `account` 
            DROP COLUMN `date_created`,
            DROP COLUMN `date_updated`
        ");
    }

}