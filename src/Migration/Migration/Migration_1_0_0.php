<?php

class Migration_1_0_0 extends \TimKipp\Intersect\Migration\AbstractMigration {

    public function getVersion()
    {
        return '1.0.0-intersect';
    }

    public function migrateUp()
    {
        $this->getAdapter()->query("
            CREATE TABLE IF NOT EXISTS `account` (
              `account_id` INT(11) NOT NULL AUTO_INCREMENT,
              `email` VARCHAR(255) NOT NULL,
              `username` VARCHAR(32),
              `first_name` VARCHAR (50),
              `last_name` VARCHAR(50),
              `password` VARCHAR(64),
              `password_reset_code` VARCHAR(64),
              `token` VARCHAR(64),
              `status` TINYINT(2) NOT NULL DEFAULT 1,
              `role` TINYINT(2) DEFAULT 1,
              `verification_code` VARCHAR(64),
              PRIMARY KEY (`account_id`),
              UNIQUE KEY unique_idx_email (`email`),
              UNIQUE KEY unique_idx_username (`username`),
              UNIQUE KEY unique_idx_token (`token`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;
        ");
    }

    public function migrateDown()
    {
        $this->getAdapter()->query("DROP TABLE IF EXISTS `account`;");
    }

}