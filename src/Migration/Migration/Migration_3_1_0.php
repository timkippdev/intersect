<?php

class Migration_3_1_0 extends \TimKipp\Intersect\Migration\AbstractMigration {

    public function getVersion()
    {
        return '3.1.0-intersect';
    }

    public function migrateUp()
    {
        $this->getAdapter()->query("ALTER TABLE `account` DROP COLUMN `session_token`;");
    }

    public function migrateDown()
    {
        $this->getAdapter()->query("ALTER TABLE `account` ADD COLUMN `session_token` VARCHAR(64);");
    }

}