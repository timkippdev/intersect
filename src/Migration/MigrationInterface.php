<?php

namespace TimKipp\Intersect\Migration;

/**
 * Interface MigrationInterface
 * @package TimKipp\Intersect\Migration
 */
interface MigrationInterface {

    /**
     * @return mixed
     */
    public function migrateUp();

    /**
     * @return mixed
     */
    public function migrateDown();

}