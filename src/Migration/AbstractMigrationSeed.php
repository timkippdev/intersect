<?php

namespace TimKipp\Intersect\Migration;

/**
 * Class AbstractMigrationSeed
 * @package TimKipp\Intersect\Migration
 */
abstract class AbstractMigrationSeed extends AbstractMigration {

    /**
     * @throws \Exception
     */
    public function migrateDown()
    {
        throw new \Exception('Not implemented');
    }

}