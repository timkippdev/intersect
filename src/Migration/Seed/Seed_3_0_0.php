<?php

class Seed_3_0_0 extends \TimKipp\Intersect\Migration\AbstractMigrationSeed {

    private $accountService;
    private $orderService;

    public function __construct(\TimKipp\Intersect\Database\Adapters\AdapterInterface $adapter)
    {
        parent::__construct($adapter);

        $this->accountService = new \TimKipp\Intersect\Services\AccountService($adapter);
        $this->orderService = new \TimKipp\Intersect\Order\Services\OrderService($adapter);
    }

    public function getVersion()
    {
        return '3.0.0-intersect';
    }

    public function migrateUp()
    {
        $seedAccount = $this->createSeedAccount();

        $this->createSeedOrder($seedAccount->getAccountId());
        $this->createSeedOrderWithPaymentType($seedAccount->getAccountId(), \TimKipp\Intersect\Order\Domain\PaymentType::CASH);
    }

    /**
     * @return \TimKipp\Intersect\Domain\Account
     */
    private function createSeedAccount()
    {
        $account = new \TimKipp\Intersect\Domain\Account();
        $account->setEmail('admin@intersect.com');
        $account->setPassword('password');
        $account->setStatus(\TimKipp\Intersect\Types\AccountStatusType::ACTIVE);
        $account->setRole(2);
        $account->setFirstName('Admin');
        $account->setLastName('Account');
        $account->setRegistrationSource('seed-data');

        return $this->accountService->create($account);
    }

    private function createSeedOrder($accountId)
    {
        $order = new \TimKipp\Intersect\Order\Domain\Order();
        $order->setAccountId($accountId);
        $order->setType(\TimKipp\Intersect\Order\OrderType::ONLINE);

        $shippingAddress = new \TimKipp\Intersect\Order\Domain\ShippingAddress();
        $shippingAddress->setStreet('123 Jamie Road');
        $shippingAddress->setCity('Douglas');
        $shippingAddress->setState('WI');
        $shippingAddress->setZipCode(12345);

        $billingAddress = new \TimKipp\Intersect\Order\Domain\BillingAddress();
        $billingAddress->setStreet('123 Rose Lane');
        $billingAddress->setCity('Douglas');
        $billingAddress->setState('WI');
        $billingAddress->setZipCode(12345);

        $orderDetails = new \TimKipp\Intersect\Order\Domain\OrderDetails();
        $orderDetails->setSubtotal(19.99);
        $orderDetails->setShipping(3.45);
        $orderDetails->setTax(2.99);
        $orderDetails->setDiscount(1.99);
        $orderDetails->setShippingAddress($shippingAddress);
        $orderDetails->setBillingAddress($billingAddress);

        $order->setDetails($orderDetails);

        return $this->orderService->create($order);
    }

    private function createSeedOrderWithPaymentType($accountId, $paymentType)
    {
        $order = $this->createSeedOrder($accountId);

        $payment = new \TimKipp\Intersect\Order\Domain\Payment();
        $payment->setPaymentTypeId($paymentType);
        $payment->setAmount(12.34);
        $payment->setExternalConfirmationNumber('external-seed-data');

        $this->orderService->createWithPayment($order, $payment);
    }

}