<?php

class Seed_1_0_0 extends \TimKipp\Intersect\Migration\AbstractMigrationSeed {

    public function getVersion()
    {
        return '1.0.0-intersect';
    }

    public function migrateUp()
    {
        // account Seed data
        // commented out in 3.0.0 and moved into Seed_3_0_0 to utilize account service methods
        //$this->getAdapter()->query("INSERT INTO `account` (`email`, `username`, `password`, `token`, `status`, `role`, `first_name`, `last_name`) VALUES ('admin@test.com', 'admin', 'password', 'admin-token', 2, 2, 'Admin', 'Account');");
    }

}