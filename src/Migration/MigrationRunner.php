<?php

namespace TimKipp\Intersect\Migration;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;

/**
 * Class MigrationRunner
 * @package TimKipp\Intersect\Migration
 */
class MigrationRunner {

    public $tableName = 'migrations';

    /**
     * @var AdapterInterface $databaseAdapter
     */
    private $databaseAdapter;
    private $migrationPaths = array();
    private $seedPaths = array();
    private $availableMigrationVersions = array();
    private $migrations = array();
    private $migrationsRan = 0;
    private $seeds = array();
    private $lastVersionMigrated;
    private $migrationType;
    private $desiredVersion;

    /**
     * MigrationRunner constructor.
     * @param AdapterInterface $databaseAdapter
     * @param null $migrationPaths
     */
    public function __construct(AdapterInterface $databaseAdapter, $migrationPaths = null)
    {
        $this->databaseAdapter = $databaseAdapter;

        if (!is_null($migrationPaths))
        {
            if (!is_array($migrationPaths))
            {
                $migrationPaths = array($migrationPaths);
            }

            $this->setMigrationPaths($migrationPaths);
        }
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param $tableName
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    /**
     * @return mixed
     */
    public function getMigrationType()
    {
        return $this->migrationType;
    }

    /**
     * @return int
     */
    public function getMigrationsRan()
    {
        return $this->migrationsRan;
    }

    /**
     * @param $migrationPath
     */
    public function setMigrationPath($migrationPath)
    {
        $this->migrationPaths = array();

        if (!is_null($migrationPath))
        {
            $this->setMigrationPaths(array($migrationPath));
        }
    }

    /**
     * @param array $migrationPaths
     */
    public function setMigrationPaths(array $migrationPaths)
    {
        $this->migrationPaths = $migrationPaths;
        $this->seedPaths = $migrationPaths;
    }

    /**
     * @return array
     */
    public function getMigrationPaths()
    {
        return $this->migrationPaths;
    }

    /**
     * @param $seedPath
     */
    public function setSeedPath($seedPath)
    {
        $this->seedPaths = array();

        if (!is_null($seedPath))
        {
            $this->setSeedPaths(array($seedPath));
        }
    }

    /**
     * @param array $seedPaths
     */
    public function setSeedPaths(array $seedPaths)
    {
        $this->seedPaths = $seedPaths;
    }

    public function getSeedPaths()
    {
        return $this->seedPaths;
    }

    /**
     * @throws \Exception
     */
    public function upgradeToLatest()
    {
        $this->upgradeTo(null);
    }

    /**
     * @throws \Exception
     */
    public function downgradeCompletely()
    {
        $this->downgradeTo(null);
    }

    /**
     * @param $version
     * @throws \Exception
     */
    public function upgradeTo($version)
    {
        $this->migrationType = MigrationType::UP;
        $this->run($version);
    }

    /**
     * @param $version
     * @throws \Exception
     */
    public function downgradeTo($version)
    {
        $this->migrationType = MigrationType::DOWN;
        $this->run($version);
    }

    /**
     * @param null $desiredVersion
     * @throws \Exception
     */
    private function run($desiredVersion = null)
    {
        if (count($this->migrationPaths) == 0)
        {
            throw new \Exception('Migration paths must be set');
        }

        if (!is_null($desiredVersion))
        {
            $this->desiredVersion = $desiredVersion;
        }

        $migrationTypeText = (($this->migrationType == MigrationType::UP) ? 'upgrade' : 'downgrade');
        if (!is_null($this->desiredVersion))
        {
            echo 'Starting Database ' . $migrationTypeText . ' to version ' . $this->desiredVersion . PHP_EOL;
        }
        else
        {
            echo 'Starting full Database ' . $migrationTypeText . PHP_EOL;
        }

        $this->verifyDatabase();
        $this->loadMigrations();
        $this->loadSeeds();
        $this->runMigrations();

        echo 'Finished Database ' . $migrationTypeText . PHP_EOL;
    }

    /**
     *
     */
    private function verifyDatabase()
    {
        if (!$this->databaseAdapter->tableExists($this->tableName))
        {
            $this->createMigrationTable();
        }
    }

    /**
     *
     */
    private function createMigrationTable()
    {
        $this->databaseAdapter->query("
            CREATE TABLE " . $this->tableName . " (
                id INT NOT NULL AUTO_INCREMENT,
                version VARCHAR(20) NOT NULL,
                path VARCHAR(255),
                migrated_on DATETIME,
                PRIMARY KEY (`id`),
                UNIQUE KEY `version` (`version`),
                INDEX `version_idx` (`version`)
            )
        ");
    }

    /**
     * @param $path
     * @return null
     */
    private function getLastMigrationRanForPath($path)
    {
        $versionRow = $this->databaseAdapter->query("SELECT version FROM " . $this->tableName . " WHERE migrated_on IS NOT NULL AND path=? ORDER BY version DESC LIMIT 1", array($path));
        $lastMigrationRan = (!is_null($versionRow) && $versionRow->getCount() == 1) ? $versionRow->getFirstRecord()['version'] : null;
        return $lastMigrationRan;
    }

    /**
     * @throws \Exception
     */
    private function loadMigrations()
    {
        $this->migrations = array();

        foreach ($this->migrationPaths as $migrationPath)
        {
            $migrationDirectory = rtrim($migrationPath, '/');

            if (!is_dir($migrationDirectory))
            {
                throw new \Exception('Migration directory could not be found: ' . $migrationDirectory);
            }

            foreach (glob($migrationDirectory . '/Migration_*.php') as $migrationFilePath)
            {
                require_once $migrationFilePath;

                $migrationFilePathParts = explode('/', $migrationFilePath);
                $migrationClass = str_replace('.php', '', end($migrationFilePathParts));

                $migration = new $migrationClass($this->databaseAdapter);

                if (!$migration instanceof AbstractMigration)
                {
                    continue;
                }

                $migrationVersion = $migration->getVersion();

                if (is_null($migrationVersion) || trim($migrationVersion) == '')
                {
                    continue;
                }

                $migrationObject = new Migration();
                $migrationObject->setPath($migrationPath);
                $migrationObject->setMigration($migration);

                $this->migrations[] = $migrationObject;
                $this->availableMigrationVersions[] = $migrationVersion;
            }

            if (!is_null($this->desiredVersion) && !in_array($this->desiredVersion, $this->availableMigrationVersions))
            {
                throw new \Exception('Desired migration version is not available: ' . $this->desiredVersion);
            }

            if ($this->migrationType != MigrationType::UP)
            {
                $this->migrations = array_reverse($this->migrations);
            }
        }
    }

    /**
     * @throws \Exception
     */
    private function loadSeeds()
    {
        $this->seeds = array();

        foreach ($this->seedPaths as $seedPath)
        {
            $seedDirectory = rtrim($seedPath, '/');

            if (!is_dir($seedDirectory))
            {
                throw new \Exception('Seed path directory could not be found: ' . $seedDirectory);
            }

            foreach (glob($seedDirectory . '/Seed*.php') as $seedFilePath)
            {
                require_once $seedFilePath;

                $seedFilePathParts = explode('/', $seedFilePath);
                $seedClass = str_replace('.php', '', end($seedFilePathParts));

                $seed = new $seedClass($this->databaseAdapter);

                if (!$seed instanceof AbstractMigrationSeed)
                {
                    continue;
                }

                $seedVersion = $seed->getVersion();

                if (is_null($seedVersion) || trim($seedVersion) == '')
                {
                    continue;
                }

                $seedObject = new Seed();
                $seedObject->setPath($seedFilePath);
                $seedObject->setSeed($seed);

                $this->seeds[$seedVersion] = $seedObject;
            }
        }
    }

    /**
     *
     */
    private function runMigrations()
    {
        /**
         * @var Migration $migration
         */
        foreach ($this->migrations as $migration)
        {
            $migrationVersion = $migration->getMigration()->getVersion();
            $this->lastVersionMigrated = $this->getLastMigrationRanForPath($migration->getPath());

            if ($this->migrationType == MigrationType::UP)
            {
                $this->performUpgrade($migration);
                if ($migrationVersion == $this->desiredVersion)
                {
                    break;
                }
            }
            else if ($this->migrationType == MigrationType::DOWN)
            {
                if ($migrationVersion == $this->desiredVersion)
                {
                    break;
                }
                $this->performDowngrade($migration);
            }
        }
    }

    /**
     * @param Migration $migration
     */
    private function performUpgrade(Migration $migration)
    {
        $migrationVersion = $migration->getMigration()->getVersion();

        if ($migrationVersion <= $this->lastVersionMigrated)
        {
            return;
        }

        echo 'Upgrading Database to version: ' . $migrationVersion . PHP_EOL;

        $migration->getMigration()->migrateUp();

        $this->migrationsRan++;

        $this->insertMigration($migration);

        $this->insertSeedData($migration);
    }

    /**
     * @param Migration $migration
     */
    private function performDowngrade(Migration $migration)
    {
        $migrationVersion = $migration->getMigration()->getVersion();

        if ($migrationVersion > $this->lastVersionMigrated || $migrationVersion == $this->desiredVersion)
        {
            return;
        }

        echo 'Reverting changes applied in version: ' . $migrationVersion . PHP_EOL;

        $migration->getMigration()->migrateDown();

        $this->migrationsRan++;

        $this->clearMigrationCompletion($migration);
    }

    /**
     * @param Migration $migration
     */
    private function insertMigration(Migration $migration)
    {
        $this->databaseAdapter->query("
            INSERT INTO " . $this->tableName . "
                (version, migrated_on, path)
            VALUES
                (:version, NOW(), :path)
            ON DUPLICATE KEY UPDATE migrated_on=NOW(), path=:path
        ", array(
            'version' => $migration->getMigration()->getVersion(),
            'path' => $migration->getPath()
        ));
    }

    /**
     * @param Migration $migration
     */
    private function clearMigrationCompletion(Migration $migration)
    {
        $this->databaseAdapter->query("
            UPDATE " . $this->tableName . "
            SET 
                migrated_on=NULL 
            WHERE 
                version=:version
                AND path=:path
            LIMIT 1
        ", array(
            'version' => $migration->getMigration()->getVersion(),
            'path' => $migration->getPath()
        ));
    }

    /**
     * @param Migration $migration
     */
    private function insertSeedData(Migration $migration)
    {
        if (array_key_exists($migration->getMigration()->getVersion(), $this->seeds))
        {
            /**
             * @var Seed $seed
             */
            $seed = $this->seeds[$migration->getMigration()->getVersion()];

            echo 'Applying seed data for version: ' . $migration->getMigration()->getVersion() . PHP_EOL;

            $seed->getSeed()->migrateUp();
        }
    }

}

/**
 * Class MigrationType
 * @package TimKipp\Intersect\Migration
 */
class MigrationType {

    const UP = 'UP';
    const DOWN = 'DOWN';

}

/**
 * Class Migration
 * @package TimKipp\Intersect\Migration
 */
class Migration {

    private $migration;
    private $path;

    /**
     * @return AbstractMigration
     */
    public function getMigration()
    {
        return $this->migration;
    }

    /**
     * @param AbstractMigration $migration
     */
    public function setMigration(AbstractMigration $migration)
    {
        $this->migration = $migration;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }
}

/**
 * Class Seed
 * @package TimKipp\Intersect\Migration
 */
class Seed {

    private $seed;
    private $path;

    /**
     * @return AbstractMigrationSeed
     */
    public function getSeed()
    {
        return $this->seed;
    }

    /**
     * @param AbstractMigrationSeed $seed
     */
    public function setSeed(AbstractMigrationSeed $seed)
    {
        $this->seed = $seed;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }
}