<?php

namespace TimKipp\Intersect\Migration;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;

/**
 * Class IntersectBackendMigrationRunner
 * @package TimKipp\Intersect\Migration
 */
class IntersectMigrationRunner {

    /**
     * @param AdapterInterface $databaseAdapter
     * @param $pathToVendorDirectory
     * @param bool $applySeedData
     * @param null $vendorInstallationPathOverride
     * @return MigrationRunner
     */
    public static function get(AdapterInterface $databaseAdapter, $pathToVendorDirectory, $applySeedData = false, $vendorInstallationPathOverride = null)
    {
        if (is_null($vendorInstallationPathOverride))
        {
            $pathToVendorDirectory = rtrim($pathToVendorDirectory, '/');
            $vendorInstallationPath = $pathToVendorDirectory . '/timkipp/intersect';
        }
        else
        {
            $vendorInstallationPath = rtrim($vendorInstallationPathOverride, '/');
        }

        $migrationPath = $vendorInstallationPath . '/src/Migration/Migration';

        $mr = new MigrationRunner($databaseAdapter, array($migrationPath));

        if ($applySeedData)
        {
            $seedPath = $vendorInstallationPath . '/src/Migration/Seed';
            $mr->setSeedPath($seedPath);
        }

        return $mr;
    }

}