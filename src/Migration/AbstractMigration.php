<?php

namespace TimKipp\Intersect\Migration;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;

/**
 * Class AbstractMigration
 * @package TimKipp\Intersect\Migration
 */
abstract class AbstractMigration implements MigrationInterface {

    private $adapter;

    /**
     * AbstractMigration constructor.
     * @param AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    // TODO: remove once helper methods are added
    /**
     * @return AdapterInterface
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * @return mixed
     */
    abstract public function getVersion();

}