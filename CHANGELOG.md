# Changelog
All changes to the **Intersect** will be documented in this file.


## [3.1.1] - 2018-04-09
- Moved `GoogleClient` scopes into `GoogleConfiguration` to allow for better extensibility


## [3.1.0] - 2018-01-14
- Extended `BaseEmail` to accept CC and BCC recipients
- Added events for `EMAIL_SENT` and `EMAIL_SENT_FAILED`
- Fixed issue where you could not log in on multiple devices and removed `session_token` column from account table


## [3.0.0] - 2018-01-04
##### Breaking Changes from v2.0.2
- Renamed namespace from `\TimKipp\IntersectBackend` to `\TimKipp\Intersect`
- Added `getPrimaryKeyColumn` method to `AbstractDomain` classes to allow for all database column-related details to be outside of DAO classes
- Replaced `AbstractAccountService` class with normal `AccountService` class and removed abstract methods, `generatePassword`, `generateAutoLoginTokenHash`, `generateSessionTokenHash`, into backend to provide default encryption behaviour 

##### Other Changes
- Cleaned up CHANGELOG file
- Added `TemporalDomain` interface which will automatically set create/update DateTime columns during domain create/update. As a convenience, the `AbstractTemporalDomain` class was added to use default column values of `date_created` and `date_updated` instead of having to implement these methods in each domain class 
- Added social account integration which allows account creation/login through Facebook, Twitter, and Google
- Added `Event`, `Listener`, and `Dispatcher` classes to support clients hooking into various backend events like account creation, password reset, login, logout, and social account creation/linking/login
    - `Event::ACCOUNT_CREATED`
    - `Event::ACCOUNT_CREATED_FAILED`
    - `Event::ACCOUNT_LOGIN`
    - `Event::ACCOUNT_LOGIN_FAILED`
    - `Event::ACCOUNT_LOGOUT`
    - `Event::ACCOUNT_PASSWORD_RESET`
    - `Event::ORDER_CANCELLED`
    - `Event::ORDER_CREATED`
    - `Event::PAYMENT_CREATED`
    - `Event::SOCIAL_ACCOUNT_CREATED`
    - `Event::SOCIAL_ACCOUNT_LINKED`
    - `Event::SOCIAL_ACCOUNT_LOGIN`
    - `Event::SOCIAL_ACCOUNT_LOGIN_FAILED`
- Added `dropTable` method in `AdapterInterface` to allow easier dropping of database tables 
- Added generic `getAllBy` method in `AbstractService` and `AbstractDao` to allow for easier simple queries
- Added an ordering system to allow creating custom orders and accepting payments
    - Acceptable payment types
        - Cash
        - Stripe (credit card payments)
- Added account `registration_source` column to set custom sources during registration
- Renamed account table column `token` to `session_token`
- Added account table column `auto_login_token` to store generated token during account creation
- Added `loginWithAutoLoginToken` method to `AbstractAccountService` to allow "auto login" functionality
- Added account table columns `ip_registration` and `ip_last_login` to tracked IP addresses for each action respectively
- Added account table column `date_last_login` that is updated automatically when account is logged in either from form or cookie
- Removed the `PostgresAdapter`


## [2.0.2] - 2017-09-18
- Fixed issue in `AbstractRouter` where root level was not detected if the url parameter was not explicitly set 


## [2.0.1] - 2017-09-08
- Improvements for `SessionManager` by adding a "defaultValue" parameter as part of the `read` method and also added a `readAndClear` method for retrieving the session value and clearing instantly afterwards
- Added support for `MigrationRunner` to handle multiple paths to migrate
- Improved usability of `AbstractRouter` so now users only have to call the `getResponse` method which returns a `Response` object
- Fixed issue in `MigrationRunner` where the `migrated_on` column was not nulled out when performing a downgrade 


## [2.0.0] - 2017-09-07
- Updated `MySQLAdapter` and `PostgresAdapter` to return new `DatabaseResult`
- Moved all DAO column and nullable column mappings to domain layer instead of DAO layer
- Added `AbstractDomain` object to be used in conjunction with `AbstractDao` methods
- Added functionality to pass `AbstractDomain` class into DAO constructor to automatically convert `DatabaseResult` records to domain objects
- DAO methods now return `AbstractDomain` objects instead of `DatabaseResult` objects if domainClass is passed into the constructor. `AbstractAccountService` defaults to return `Account` domain objects from the `AccountDao`
- Added `getByIds` DAO and service methods to pull data from array of IDs 
- Added `SessionManager` and `CookieManager` classes to help access/manage sessions and cookies
- Fixed `MigrationRunner` issue that caused lower version migrations to not run if a higher version was already ran in the past 
- Removed `execute`, `getInsertId`, `fetch`, and `fetchAll` methods from `AdapterInterface` and all classes with implementation
- Removed `AbstractMappableDao` and related classes (use `AbstractDao` and `AbstractDomain` for existing functionality)


## [1.0.0] - 2017-08-28
- Database Adapters
  - `MySQLAdapter` - wrapper for MySQL interaction using PDO
  - `PostgresAdapter` - wrapper for Postgres interaction (WIP)
- Database Access Objects (DAO)
  - `AbstractAssocationDao` - extend to easily interact with an association table
  - `AbstractMappableDao` - extend to easily interact with a table by using providing a database->domain mapping and a few other simple details (extended from `AbstractDao`)
  - `AbstractDao` - extend to easily interact with a table. provides helper methods for getting all records, and getting records by ID
  - `AccountDao` - built-in DAO for interacting with the included account table
- Domain
  - `Account` - domain object based on the built-in account table schema
- Migrations
  - `MigrationRunner` - used to easily downgrade/upgrade through migration versions to apply database migration/seed files
- Routing
  - `AbstractRouter` - extend to create your own router with ability to create custom url patterns pointing to controllers namespace methods
- Services
  - `AbstractAccountService` - extend to add a built-in account service used for adding accounts to your database. includes `Account` domain object, account validation, migration scripts, and simple service methods exposed for generating custom passwords and tokens