<?php

namespace TimKipp\Intersect\Tests\Unit\Database\Adapters;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Tests\Stubs\TestAdapter;

class AbstractAdapterTest extends TestCase {

    /**
     * @var TestAdapter $testAdapter
     */
    private $testAdapter;

    public function setUp()
    {
        $this->testAdapter = new TestAdapter('host', 'username', 'password', 'Database');
    }

    public function test_verifyCredentials()
    {
        $this->assertEquals('host', $this->testAdapter->getHost());
        $this->assertEquals('username', $this->testAdapter->getUsername());
        $this->assertEquals('password', $this->testAdapter->getPassword());
        $this->assertEquals('Database', $this->testAdapter->getDatabase());
    }

}