<?php

namespace TimKipp\Intersect\Tests\Unit\Database\Dao;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Database\Dao\AbstractDao;
use TimKipp\Intersect\Database\DatabaseResult;
use TimKipp\Intersect\Tests\Stubs\TestDao;
use TimKipp\Intersect\Tests\Stubs\TestDomain;
use TimKipp\Intersect\Utility\DatabaseUtility;

class AbstractDaoTest extends TestCase {

    /** @var AbstractDao $testDao */
    private $testDao;

    /** @var \PHPUnit_Framework_MockObject_MockObject $adapterMock */
    private $adapterMock;

    protected function setUp()
    {
        $this->adapterMock = $this->getMockBuilder('TimKipp\Intersect\Database\Adapters\AbstractAdapter')->setConstructorArgs(array('', '', '', ''))->getMock();

        $this->testDao = new TestDao($this->adapterMock, TestDomain::class);
    }

    public function test_getAll()
    {
        $fakeResult = new DatabaseResult();
        $fakeResult->setRecords(array(array('id' => 3)));

        $this->adapterMock->expects($this->once())
            ->method('query')
            ->with($this->equalTo("SELECT * FROM `test`"))
            ->will($this->returnValue($fakeResult));

        $actualResponse = $this->testDao->getAll();
        $this->assertTrue($actualResponse[0] instanceof TestDomain);
        $this->assertEquals( 3, $actualResponse[0]->id);
    }

    public function test_getAllBy()
    {
        $fakeResult = new DatabaseResult();
        $fakeResult->setRecords(array(array('id' => 1)));

        $this->adapterMock->expects($this->once())
            ->method('query')
            ->with($this->equalTo("SELECT * FROM `test` WHERE id=:id"))
            ->will($this->returnValue($fakeResult));

        $actualResponse = $this->testDao->getAllBy('id', 3);
        $this->assertTrue(is_array($actualResponse));
        $this->assertTrue($actualResponse[0] instanceof TestDomain);
        $this->assertEquals( 1, $actualResponse[0]->id);
    }

    public function test_getAllBy_withLimit()
    {
        $fakeResult = new DatabaseResult();
        $fakeResult->setRecords(array(array('id' => 1)));

        $this->adapterMock->expects($this->once())
            ->method('query')
            ->with($this->equalTo("SELECT * FROM `test` WHERE id=:id LIMIT 10"))
            ->will($this->returnValue($fakeResult));

        $actualResponse = $this->testDao->getAllBy('id', 3, 10);
        $this->assertTrue(is_array($actualResponse));
        $this->assertTrue($actualResponse[0] instanceof TestDomain);
        $this->assertEquals( 1, $actualResponse[0]->id);
    }

    public function test_getAllBy_withLimitOne()
    {
        $fakeResult = new DatabaseResult();
        $fakeResult->setRecords(array(array('id' => 1)));

        $this->adapterMock->expects($this->once())
            ->method('query')
            ->with($this->equalTo("SELECT * FROM `test` WHERE id=:id LIMIT 1"))
            ->will($this->returnValue($fakeResult));

        $actualResponse = $this->testDao->getAllBy('id', 3, 1);
        $this->assertTrue($actualResponse instanceof TestDomain);
        $this->assertEquals( 1, $actualResponse->id);
    }

    public function test_getAllBy_withNullValue()
    {
        $fakeResult = new DatabaseResult();
        $fakeResult->setRecords(array(array('id' => 1)));

        $this->adapterMock->expects($this->once())
            ->method('query')
            ->with($this->equalTo("SELECT * FROM `test` WHERE id IS NULL"))
            ->will($this->returnValue($fakeResult));

        $actualResponse = $this->testDao->getAllBy('id', null);
        $this->assertTrue($actualResponse[0] instanceof TestDomain);
        $this->assertEquals( 1, $actualResponse[0]->id);
    }

    public function test_getById()
    {
        $fakeResult = new DatabaseResult();
        $fakeResult->setRecords(array(array('id' => 1)));

        $this->adapterMock->expects($this->once())
            ->method('query')
            ->with($this->equalTo("SELECT * FROM `test` WHERE id=:id LIMIT 1"))
            ->will($this->returnValue($fakeResult));

        $actualResponse = $this->testDao->getById(3);
        $this->assertTrue($actualResponse instanceof TestDomain);
        $this->assertEquals( 1, $actualResponse->id);
    }

    public function test_getByIds()
    {
        $fakeResult = new DatabaseResult();
        $fakeResult->setRecords(array(array('id' => 1)));

        $this->adapterMock->expects($this->once())
            ->method('query')
            ->with($this->equalTo("SELECT * FROM `test` WHERE id IN (1,2,3)"))
            ->will($this->returnValue($fakeResult));

        $actualResponse = $this->testDao->getByIds(array(1,2,3));
        $this->assertTrue($actualResponse[0] instanceof TestDomain);
        $this->assertEquals( 1, $actualResponse[0]->id);
    }

    public function test_getByIds_emptyArray()
    {
        $this->assertEmpty($this->testDao->getByIds(array()));
    }

    public function test_createRecord_nullValuesAreNotQuoted()
    {
        $testDomain = new TestDomain();
        $testDomain->id = 1;
        $testDomain->email = 'test@test.com';

        $this->adapterMock
            ->expects($this->at(0))
            ->method('query')
            ->will($this->returnCallback(function($query) {
                $formattedQuery = DatabaseUtility::removeQueryLineBreaks($query);

                $this->assertEquals("INSERT INTO test (email, username, nullable) VALUES (:email, :username, :nullable)", $formattedQuery);
            }))
            ->will($this->returnValue(new DatabaseResult()));

        $this->adapterMock
            ->expects($this->at(1))
            ->method('query')
            ->will($this->returnCallback(function($query) {
                $formattedQuery = DatabaseUtility::removeQueryLineBreaks($query);

                $this->assertEquals("SELECT * FROM `test` WHERE id=? LIMIT 1", $formattedQuery);
            }))
            ->will($this->returnValue(new DatabaseResult()));

        $this->testDao->createRecord($testDomain);
    }

    public function test_updateRecord_nullValuesAreNotQuoted()
    {
        $testDomain = new TestDomain();
        $testDomain->id = 1;
        $testDomain->email = 'test@test.com';

        $this->adapterMock
            ->expects($this->at(0))
            ->method('query')
            ->will($this->returnCallback(function($query) {
                $formattedQuery = DatabaseUtility::removeQueryLineBreaks($query);

                $this->assertEquals("UPDATE `test` SET email=:email, nullable=:nullable WHERE id=:id LIMIT 1", $formattedQuery);
            }));

        $this->adapterMock
            ->expects($this->at(1))
            ->method('query')
            ->will($this->returnCallback(function($query) {
                $formattedQuery = DatabaseUtility::removeQueryLineBreaks($query);

                $this->assertEquals("SELECT * FROM `test` WHERE id=? LIMIT 1", $formattedQuery);
            }))
            ->will($this->returnValue(new DatabaseResult()));

        $this->testDao->updateRecord($testDomain, 1);
    }

    public function test_updateRecord_nonNullableValuesDoNotShowUpIfNull()
    {
        $testDomain = new TestDomain();
        $testDomain->id = 1;
        $testDomain->email = 'test@test.com';

        $this->adapterMock
            ->expects($this->at(0))
            ->method('query')
            ->will($this->returnCallback(function($query) {
                $formattedQuery = DatabaseUtility::removeQueryLineBreaks($query);

                $this->assertEquals("UPDATE `test` SET email=:email, nullable=:nullable WHERE id=:id LIMIT 1", $formattedQuery);
            }));

        $this->adapterMock
            ->expects($this->at(1))
            ->method('query')
            ->will($this->returnCallback(function($query) {
                $formattedQuery = DatabaseUtility::removeQueryLineBreaks($query);

                $this->assertEquals("SELECT * FROM `test` WHERE id=? LIMIT 1", $formattedQuery);
            }))
            ->will($this->returnValue(new DatabaseResult()));

        $this->testDao->updateRecord($testDomain, 1);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage domain must have a valid primary key value
     */
    public function test_deleteRecord_invalidPrimaryKeyValue()
    {
        $testDomain = new TestDomain();
        $testDomain->id = 'invalid-primary-key';

        $this->testDao->deleteRecord($testDomain);
    }

    public function test_deleteRecord()
    {
        $testDomain = new TestDomain();
        $testDomain->id = 1;

        $this->adapterMock
            ->expects($this->at(0))
            ->method('query')
            ->will($this->returnCallback(function($query) {
                $formattedQuery = DatabaseUtility::removeQueryLineBreaks($query);

                $this->assertEquals("DELETE FROM `test` WHERE id=? LIMIT 1", $formattedQuery);
            }));

        $this->testDao->deleteRecord($testDomain);
    }
}