<?php

namespace TimKipp\Intersect\Tests\Unit\Database\Dao;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Database\Adapters\AbstractAdapter;
use TimKipp\Intersect\Database\DatabaseResult;
use TimKipp\Intersect\Tests\Stubs\TestAssociationDao;
use TimKipp\Intersect\Utility\DatabaseUtility;

class AbstractAssociationDaoTest extends TestCase {

    /** @var TestAssociationDao $testAssociationDao */
    private $testAssociationDao;

    /** @var \PHPUnit_Framework_MockObject_MockObject $databaseMock */
    private $databaseMock;

    public function setUp()
    {
        $this->databaseMock = $this->getMockBuilder(AbstractAdapter::class)->setConstructorArgs(array('', '', '', ''))->getMock();
        $this->testAssociationDao = new TestAssociationDao($this->databaseMock);
    }

    public function test_createAssociation()
    {
        $this->databaseMock
            ->expects($this->at(0))
            ->method('query')
            ->will($this->returnCallback(function($query) {
                $formattedQuery = DatabaseUtility::removeQueryLineBreaks($query);

                $this->assertEquals("INSERT INTO association_test (column_one, column_two) VALUES (:columnOne, :columnTwo)", $formattedQuery);
            }));

        $this->testAssociationDao->createAssociation(1, 1);
    }

    public function test_deleteAssociation()
    {
        $this->databaseMock
            ->expects($this->at(0))
            ->method('query')
            ->will($this->returnCallback(function($query) {
                $formattedQuery = DatabaseUtility::removeQueryLineBreaks($query);

                $this->assertEquals("DELETE FROM association_test WHERE column_one=:columnOne AND column_two=:columnTwo LIMIT 1", $formattedQuery);
            }));

        $this->testAssociationDao->deleteAssociation(1, 1);
    }

    public function test_getAssociation()
    {
        $this->databaseMock
            ->expects($this->at(0))
            ->method('query')
            ->will($this->returnCallback(function($query) {
                $formattedQuery = DatabaseUtility::removeQueryLineBreaks($query);

                $this->assertEquals("SELECT * FROM association_test WHERE column_one=:columnOne AND column_two=:columnTwo LIMIT 1", $formattedQuery);
            }));

        $this->testAssociationDao->getAssociation(1, 1);
    }

    public function test_getAssociationsForColumnOne()
    {
        $result = new DatabaseResult();
        $result->setRecords(array(
            array('column_one' => 111, 'column_two' => 222),
            array('column_one' => 333, 'column_two' => 444)
        ));

        $this->databaseMock
            ->expects($this->at(0))
            ->method('query')
            ->will($this->returnCallback(function($query) {
                $formattedQuery = DatabaseUtility::removeQueryLineBreaks($query);

                $this->assertEquals("SELECT * FROM association_test WHERE column_one=:columnOne", $formattedQuery);
            }))
            ->will($this->returnValue($result));

        $associations = $this->testAssociationDao->getAssociationsForColumnOne( 1);

        $this->assertCount(2, $associations);
        $this->assertEquals(222, $associations[0]);
        $this->assertEquals(444, $associations[1]);
    }

    public function test_getAssociationsForColumnTwo()
    {
        $result = new DatabaseResult();
        $result->setRecords(array(
            array('column_one' => 111, 'column_two' => 222),
            array('column_one' => 333, 'column_two' => 444)
        ));

        $this->databaseMock
            ->expects($this->at(0))
            ->method('query')
            ->will($this->returnCallback(function($query) {
                $formattedQuery = DatabaseUtility::removeQueryLineBreaks($query);

                $this->assertEquals("SELECT * FROM association_test WHERE column_two=:columnTwo", $formattedQuery);
            }))
            ->will($this->returnValue($result));

        $associations = $this->testAssociationDao->getAssociationsForColumnTwo( 1);

        $this->assertCount(2, $associations);
        $this->assertEquals(111, $associations[0]);
        $this->assertEquals(333, $associations[1]);
    }

}