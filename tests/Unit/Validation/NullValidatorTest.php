<?php

namespace TimKipp\Intersect\Tests\Unit\Validation;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Validation\NullValidator;

class NullValidatorTest extends TestCase {

    /** @var NullValidator $testValidator */
    private $nullValidator;

    protected function setUp()
    {
        $this->nullValidator = new NullValidator();
    }

    public function test_validateCreate()
    {
        $this->assertTrue($this->nullValidator->validateCreate(null));
    }

    public function test_validateDelete()
    {
        $this->assertTrue($this->nullValidator->validateDelete(null));
    }

    public function test_validateUpdate()
    {
        $this->assertTrue($this->nullValidator->validateUpdate(null));
    }

}