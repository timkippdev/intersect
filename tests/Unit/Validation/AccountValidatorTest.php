<?php

namespace TimKipp\Intersect\Tests\Unit\Services;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Validation\AccountValidator;

class AccountValidatorTest extends TestCase {

    /** @var AccountValidator $accountValidator */
    private $accountValidator;

    protected function setUp()
    {
        $this->accountValidator = new AccountValidator();
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Please enter a valid email address
     */
    public function test_validateCreate_missingEmail()
    {
        $account = new Account();
        $this->accountValidator->validateCreate($account);
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Please enter a valid email address
     */
    public function test_validateCreate_invalidEmail()
    {
        $account = new Account();
        $account->setEmail('test@test');
        $this->accountValidator->validateCreate($account);
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Please enter a password
     */
    public function test_validateCreate_invalidPassword()
    {
        $account = new Account();
        $account->setEmail('test@test.com');
        $this->accountValidator->validateCreate($account);
    }

    public function test_validateCreate()
    {
        $account = new Account();
        $account->setEmail('test@test.com');
        $account->setPassword('password');
        $this->assertTrue($this->accountValidator->validateCreate($account));
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage A valid account ID is required
     */
    public function test_validateDelete_invalidAccountID()
    {
        $account = new Account();
        $this->accountValidator->validateDelete($account);
    }

    public function test_validateDelete()
    {
        $account = new Account();
        $account->setAccountId(1);
        $this->assertTrue($this->accountValidator->validateDelete($account));
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage A valid account ID is required
     */
    public function test_validateUpdate_invalidAccountID()
    {
        $account = new Account();
        $this->accountValidator->validateUpdate($account);
    }

    public function test_validateUpdate()
    {
        $account = new Account();
        $account->setAccountId(1);
        $this->assertTrue($this->accountValidator->validateUpdate($account));
    }

}