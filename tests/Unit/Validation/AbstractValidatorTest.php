<?php

namespace TimKipp\Intersect\Tests\Unit\Validation;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Tests\Stubs\TestValidator;

class AbstractValidatorTest extends TestCase {

    /** @var TestValidator $testValidator */
    private $testValidator;

    protected function setUp()
    {
        $this->testValidator = new TestValidator();
    }

    public function test_isBlank()
    {
        $this->assertTrue($this->testValidator->isBlank(null));
        $this->assertTrue($this->testValidator->isBlank(''));
        $this->assertFalse($this->testValidator->isBlank('notblank'));
    }

    public function test_isEmpty()
    {
        $this->assertTrue($this->testValidator->isEmpty(null));
        $this->assertTrue($this->testValidator->isEmpty(''));
        $this->assertTrue($this->testValidator->isEmpty(array()));
        $this->assertFalse($this->testValidator->isEmpty(array('notempty')));
    }

    public function test_isGreaterThanZero()
    {
        $this->assertFalse($this->testValidator->isGreaterThanZero(null));
        $this->assertFalse($this->testValidator->isGreaterThanZero(''));
        $this->assertFalse($this->testValidator->isGreaterThanZero(-1));
        $this->assertFalse($this->testValidator->isGreaterThanZero(0));
        $this->assertTrue($this->testValidator->isGreaterThanZero(1));
    }

}