<?php

namespace TimKipp\Intersect\Tests\Unit;

use Abraham\TwitterOAuth\TwitterOAuth;
use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Database\Adapters\AbstractAdapter;
use TimKipp\Intersect\Helper\SessionManager;
use TimKipp\Intersect\Social\Facebook\FacebookClient;
use TimKipp\Intersect\Social\Facebook\FacebookConfiguration;
use TimKipp\Intersect\Social\Google\GoogleConfiguration;
use TimKipp\Intersect\Social\Twitter\TwitterClient;
use TimKipp\Intersect\Social\Twitter\TwitterConfiguration;
use TimKipp\Intersect\Tests\Stubs\TestAccountService;
use TimKipp\Intersect\Tests\Stubs\TestFacebookClient;
use TimKipp\Intersect\Tests\Stubs\TestGoogleClient;
use TimKipp\Intersect\Tests\Stubs\TestGoogleServiceInvalidUserInfo;
use TimKipp\Intersect\Tests\Stubs\TestGoogleServiceUserInfo;
use TimKipp\Intersect\Tests\Stubs\TestGoogleServiceNullUserInfo;
use TimKipp\Intersect\Tests\Stubs\TestTwitterClient;

abstract class BaseUnitTest extends TestCase {

    /** @var \PHPUnit_Framework_MockObject_MockObject $accountServiceMock */
    protected $accountServiceMock;

    /** @var \PHPUnit_Framework_MockObject_MockObject $databaseAdapterMock */
    protected $databaseAdapterMock;

    /** @var \PHPUnit_Framework_MockObject_MockObject $sessionManagerMock */
    protected $sessionManagerMock;

    protected function setUp()
    {
        parent::setUp();

        unset($_POST);
        unset($_GET);

        $this->databaseAdapterMock = $this->getMockBuilder(AbstractAdapter::class)->setConstructorArgs(array('', '', '', ''))->getMock();

        $this->accountServiceMock = $this->getMockBuilder(TestAccountService::class)->setConstructorArgs(array($this->databaseAdapterMock))->getMock();
        $this->sessionManagerMock = $this->getMockBuilder(SessionManager::class)->getMock();
    }

    /**
     * @return TestFacebookClient
     */
    protected function getFacebookClientWithMocks()
    {
        $facebookConfiguration = new FacebookConfiguration('app', 'secret', 'http://localhost');
        $facebookClient = new TestFacebookClient($facebookConfiguration);

        $facebookMock = $this->getMockBuilder('Facebook\Facebook')->setConstructorArgs(array($facebookConfiguration->toFacebookArray()))->getMock();
        $facebookSessionPersistentDataHandlerMock = $this->getMockBuilder('Facebook\PersistentData\FacebookSessionPersistentDataHandler')->setConstructorArgs(array(false))->getMock();

        $facebookRedirectLoginHelperMock = $this->getMockBuilder('Facebook\Helpers\FacebookRedirectLoginHelper')->setConstructorArgs(array(
            $facebookClient->getFacebook()->getOAuth2Client(),
            $facebookSessionPersistentDataHandlerMock,
            null,
            null
        ))->getMock();

        $facebookClient->setFacebook($facebookMock);
        $facebookClient->setFacebookRedirectLoginHelper($facebookRedirectLoginHelperMock);

        return $facebookClient;
    }

    /**
     * @return TestTwitterClient
     */
    protected function getTwitterClientWithMocks()
    {
        $twitterConfiguration = new TwitterConfiguration('app', 'secret', 'http://localhost');
        $twitterClient = new TestTwitterClient($twitterConfiguration, $this->sessionManagerMock);

        $twitterOauthMock = $this->getMockBuilder(TwitterOAuth::class)->setConstructorArgs(array($twitterConfiguration->getConsumerKey(), $twitterConfiguration->getConsumerSecret()))->getMock();

        $twitterClient->setTwitterOAuth($twitterOauthMock);

        return $twitterClient;
    }

    /**
     * @return TestGoogleClient
     */
    protected function getGoogleClientWithMocks($sendNullResponse = false, $invalidUser = false)
    {
        $googleConfiguration = new GoogleConfiguration('app', 'secret', 'http://localhost');
        $googleClient = new TestGoogleClient($googleConfiguration);

        $googleOAuthClientMock = $this->getMockBuilder(\Google_Client::class)->getMock();
        $googleOAuthServiceMock = $this->getMockBuilder(\Google_Service_Oauth2::class)->setConstructorArgs(array($googleOAuthClientMock))->getMock();

        if ($sendNullResponse)
        {
            $googleOAuthServiceMock->userinfo = new TestGoogleServiceNullUserInfo();
        }
        else
        {
            if ($invalidUser)
            {
                $googleOAuthServiceMock->userinfo = new TestGoogleServiceInvalidUserInfo();
            }
            else
            {
                $googleOAuthServiceMock->userinfo = new TestGoogleServiceUserInfo();
            }
        }

        $googleClient->setOAuthClient($googleOAuthClientMock);
        $googleClient->setOAuthService($googleOAuthServiceMock);

        return $googleClient;
    }

}