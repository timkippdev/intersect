<?php

namespace TimKipp\Intersect\Tests\Unit\Migration;

use TimKipp\Intersect\Migration\IntersectMigrationRunner;
use TimKipp\Intersect\Tests\IT\TestDatabaseAdapter;
use TimKipp\Intersect\Tests\Unit\BaseUnitTest;

class IntersectMigrationRunnerTest extends BaseUnitTest {

    public function test_get()
    {
        $mr = IntersectMigrationRunner::get(new TestDatabaseAdapter(), '../test', true);

        $this->assertNotNull($mr);
        $this->assertEquals('../test/timkipp/intersect/src/Migration/Migration', $mr->getMigrationPaths()[0]);
        $this->assertEquals('../test/timkipp/intersect/src/Migration/Seed', $mr->getSeedPaths()[0]);
    }

    public function test_get_withVendorOverride()
    {
        $mr = IntersectMigrationRunner::get(new TestDatabaseAdapter(), '../test', true, '../vendor/ib');

        $this->assertNotNull($mr);
        $this->assertEquals('../vendor/ib/src/Migration/Migration', $mr->getMigrationPaths()[0]);
        $this->assertEquals('../vendor/ib/src/Migration/Seed', $mr->getSeedPaths()[0]);
    }

}