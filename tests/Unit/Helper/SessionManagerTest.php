<?php

namespace TimKipp\Intersect\Tests\Unit\Helper;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Helper\SessionManager;

class SessionManagerTest extends TestCase {

    /** @var SessionManager $sessionManager */
    private $sessionManager;

    protected function setUp()
    {
        parent::setUp();

        $this->sessionManager = new SessionManager();
        unset($_SESSION);
    }

    public function test_read()
    {
        $_SESSION['unit'] = 'test';
        $sessionValue = $this->sessionManager->read('unit');
        $this->assertNotNull($sessionValue);
        $this->assertEquals('test', $sessionValue);
    }

    public function test_readAndClear()
    {
        $_SESSION['unit'] = 'test';
        $sessionValue = $this->sessionManager->readAndClear('unit');
        $this->assertNotNull($sessionValue);
        $this->assertEquals('test', $sessionValue);
        $this->assertFalse(isset($_SESSION['unit']));
    }

    public function test_readAndClear_withDefaultValue()
    {
        $sessionValue = $this->sessionManager->readAndClear('notfound', 'default');
        $this->assertNotNull($sessionValue);
        $this->assertEquals('default', $sessionValue);
        $this->assertFalse(isset($_SESSION['notfound']));
    }

    public function test_read_notFound()
    {
        $_SESSION['unit'] = 'test';
        $sessionValue = $this->sessionManager->read('notfound');
        $this->assertNull($sessionValue);
    }

    public function test_read_notFoundWithDefaultValue()
    {
        $sessionValue = $this->sessionManager->read('notfound', 'default');
        $this->assertNotNull($sessionValue);
        $this->assertEquals('default', $sessionValue);
    }

    public function test_write()
    {
        $this->assertFalse(isset($_SESSION['unit']));
        $this->sessionManager->write('unit', 'test');
        $this->assertTrue(isset($_SESSION['unit']));
        $this->assertEquals('test', $_SESSION['unit']);
    }

    public function test_clear()
    {
        $_SESSION['unit'] = 'test';
        $this->sessionManager->clear('unit');
        $this->assertFalse(isset($_SESSION['unit']));
    }

}