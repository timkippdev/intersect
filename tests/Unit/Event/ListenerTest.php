<?php

namespace TimKipp\Intersect\Tests\Unit\Event;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Tests\Stubs\TestEvent;
use TimKipp\Intersect\Tests\Stubs\TestListener;

class ListenerTest extends TestCase {

    /** @var TestListener $listener */
    private $listener;

    protected function setUp()
    {
        parent::setUp();

        $this->listener = new TestListener();
    }

    /**
     * @expectedException \TypeError
     */
    public function test_handle_typeError()
    {
        $this->listener->handle(null);
    }

    public function test_handle()
    {
        $result = $this->listener->handle(new TestEvent());
        $this->assertEquals('handled event - test', $result);
    }

}