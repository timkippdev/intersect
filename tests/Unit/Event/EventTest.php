<?php

namespace TimKipp\Intersect\Tests\Unit\Event;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Tests\Stubs\TestEvent;

class EventTest extends TestCase {

    /** @var TestEvent $event */
    private $event;

    protected function setUp()
    {
        parent::setUp();

        $this->event = new TestEvent();
    }

    public function test_getName()
    {
        $this->assertEquals('test', $this->event->getName());
    }

}