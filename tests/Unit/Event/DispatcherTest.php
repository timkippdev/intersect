<?php

namespace TimKipp\Intersect\Tests\Unit\Event;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Event\Dispatcher;
use TimKipp\Intersect\Tests\Stubs\TestEvent;
use TimKipp\Intersect\Tests\Stubs\TestListener;

class DispatcherTest extends TestCase {

    /** @var Dispatcher $dispatcher */
    private $dispatcher;

    protected function setUp()
    {
        parent::setUp();

        $this->dispatcher = Dispatcher::get();
    }

    public function test_getListenersByEventName_notFound()
    {
        $listeners = $this->dispatcher->getListenersByEventName('notFound');
        $this->assertCount(0, $listeners);
    }

    public function test_getListenersByEventName()
    {
        $this->dispatcher->addListener('getListenersByEventName', new TestListener());

        $listeners = $this->dispatcher->getListenersByEventName('getListenersByEventName');
        $this->assertCount(1, $listeners);
    }

    public function test_hasListenersForEventName()
    {
        $this->dispatcher->addListener('hasListenersForEventName', new TestListener());

        $this->assertFalse($this->dispatcher->hasListenersForEventName('notFound'));
        $this->assertTrue($this->dispatcher->hasListenersForEventName('hasListenersForEventName'));
    }

    public function test_addListener()
    {
        $this->assertArrayNotHasKey('addListener', $this->dispatcher->getListeners());

        $this->dispatcher->addListener('addListener', new TestListener());

        $this->assertArrayHasKey('addListener', $this->dispatcher->getListeners());
    }

    public function test_dispatch()
    {
        $listenerMock = $this->createMock(TestListener::class);

        $this->dispatcher->addListener('test', $listenerMock);

        $listenerMock->expects($this->once())
            ->method('handle');

        $this->dispatcher->dispatch(new TestEvent());
    }

}