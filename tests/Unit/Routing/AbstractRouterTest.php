<?php

namespace TimKipp\Intersect\Tests\Unit\Routing;

use TimKipp\Intersect\Tests\Stubs\TestRouter;

class AbstractRouterTest extends \PHPUnit\Framework\TestCase {

    /**
     * @var TestRouter $testRouter
     */
    private $testRouter;

    public function setUp()
    {
        $this->testRouter = new TestRouter();
    }

    public function test_verifyRequestMethod()
    {
        $this->testRouter->setCurrentRequestMethod('GET');
        $route = $this->testRouter->getRouteFromUrl('');
        $this->assertEquals('GET', $route->getRequestMethod());

        $this->testRouter->setCurrentRequestMethod('GET');
        $route = $this->testRouter->getRouteFromUrl('/get');
        $this->assertEquals('GET', $route->getRequestMethod());

        $this->testRouter->setCurrentRequestMethod('POST');
        $route = $this->testRouter->getRouteFromUrl('/post');
        $this->assertEquals('POST', $route->getRequestMethod());

        $this->testRouter->setCurrentRequestMethod('PUT');
        $route = $this->testRouter->getRouteFromUrl('/put');
        $this->assertEquals('PUT', $route->getRequestMethod());

        $this->testRouter->setCurrentRequestMethod('DELETE');
        $route = $this->testRouter->getRouteFromUrl('/delete');
        $this->assertEquals('DELETE', $route->getRequestMethod());
    }

    public function test_verifyControllerName()
    {
        $this->testRouter->setCurrentRequestMethod('GET');
        $route = $this->testRouter->getRouteFromUrl('');
        $this->assertEquals('\TimKipp\Intersect\Tests\Stubs\TestController', $route->getController());

        $this->testRouter->setCurrentRequestMethod('GET');
        $route = $this->testRouter->getRouteFromUrl('/get');
        $this->assertEquals('\TimKipp\Intersect\Tests\Stubs\TestController', $route->getController());

        $this->testRouter->setCurrentRequestMethod('POST');
        $route = $this->testRouter->getRouteFromUrl('/post');
        $this->assertEquals('\TimKipp\Intersect\Tests\Stubs\TestController', $route->getController());

        $this->testRouter->setCurrentRequestMethod('PUT');
        $route = $this->testRouter->getRouteFromUrl('/put');
        $this->assertEquals('\TimKipp\Intersect\Tests\Stubs\TestController', $route->getController());

        $this->testRouter->setCurrentRequestMethod('DELETE');
        $route = $this->testRouter->getRouteFromUrl('/delete');
        $this->assertEquals('\TimKipp\Intersect\Tests\Stubs\TestController', $route->getController());
    }

    public function test_verifyControllerMethod()
    {
        $this->testRouter->setCurrentRequestMethod('GET');
        $route = $this->testRouter->getRouteFromUrl('');
        $this->assertEquals('testGetAsHomepage', $route->getMethod());

        $this->testRouter->setCurrentRequestMethod('GET');
        $route = $this->testRouter->getRouteFromUrl('/get');
        $this->assertEquals('testGetAsString', $route->getMethod());

        $this->testRouter->setCurrentRequestMethod('POST');
        $route = $this->testRouter->getRouteFromUrl('/post');
        $this->assertEquals('testPost', $route->getMethod());

        $this->testRouter->setCurrentRequestMethod('PUT');
        $route = $this->testRouter->getRouteFromUrl('/put');
        $this->assertEquals('testPut', $route->getMethod());

        $this->testRouter->setCurrentRequestMethod('DELETE');
        $route = $this->testRouter->getRouteFromUrl('/delete');
        $this->assertEquals('testDelete', $route->getMethod());
    }

    public function test_verifyRouteNotFound()
    {
        $this->testRouter->setCurrentRequestMethod('GET');
        $route = $this->testRouter->getRouteFromUrl('/not-found');
        $this->assertNull($route);
    }

    public function test_verifyParameterMatching()
    {
        $this->testRouter->setCurrentRequestMethod('GET');
        $route = $this->testRouter->getRouteFromUrl('/parameter/2/test/unit-test');
        $routeParameters = $route->getParameters();
        $this->assertCount(2, $routeParameters);
        $this->assertEquals('2', $routeParameters[0]);
        $this->assertEquals('unit-test', $routeParameters[1]);
    }

}