<?php

namespace TimKipp\Intersect\Tests\Unit\Services;

use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use TimKipp\Intersect\Database\Adapters\AbstractAdapter;
use TimKipp\Intersect\Database\Dao\AccountDao;
use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Domain\SocialAccount;
use TimKipp\Intersect\Domain\SocialProvider;
use TimKipp\Intersect\Event\Event;
use TimKipp\Intersect\Helper\CookieManager;
use TimKipp\Intersect\Services\EncryptionService;
use TimKipp\Intersect\Services\SocialAccountService;
use TimKipp\Intersect\Social\SocialAccessToken;
use TimKipp\Intersect\Social\SocialClientException;
use TimKipp\Intersect\Tests\Stubs\TestAccount;
use TimKipp\Intersect\Tests\Stubs\TestAccountService;
use TimKipp\Intersect\Tests\Stubs\TestListener;
use TimKipp\Intersect\Tests\Stubs\TestSocialAccount;
use TimKipp\Intersect\Tests\Stubs\TestTwitterResponse;
use TimKipp\Intersect\Tests\Unit\BaseUnitTest;
use TimKipp\Intersect\Types\AccountStatusType;

class AccountServiceTest extends BaseUnitTest {

    /** @var TestAccountService $accountService */
    private $accountService;

    /** @var \PHPUnit_Framework_MockObject_MockObject $accountDaoMock */
    private $accountDaoMock;

    /** @var \PHPUnit_Framework_MockObject_MockObject $cookieManagerMock */
    private $cookieManagerMock;

    /** @var \PHPUnit_Framework_MockObject_MockObject $encryptorServiceMock */
    private $encryptorServiceMock;

    /** @var \PHPUnit_Framework_MockObject_MockObject $validatorMock */
    private $validatorMock;

    protected function setUp()
    {
        parent::setUp();

        $databaseMock = $this->getMockBuilder(AbstractAdapter::class)->setConstructorArgs(array('', '', '', ''))->getMock();

        $this->accountDaoMock = $this->getMockBuilder(AccountDao::class)->setConstructorArgs(array($databaseMock, null))->getMock();
        $this->validatorMock = $this->getMockBuilder(\TimKipp\Intersect\Validation\AccountValidator::class)->getMock();
        $this->cookieManagerMock = $this->getMockBuilder(CookieManager::class)->getMock();
        $this->encryptorServiceMock = $this->getMockBuilder(EncryptionService::class)->getMock();

        $this->accountService = new TestAccountService($databaseMock);
        $this->accountService->setDao($this->accountDaoMock);
        $this->accountService->setCookieManager($this->cookieManagerMock);
        $this->accountService->setSessionManager($this->sessionManagerMock);
        $this->accountService->setEncryptor($this->encryptorServiceMock);

        unset($_GET);

        $_SERVER['HTTP_USER_AGENT'] = 'Intersect Tests';
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
    }

    public function test_create()
    {
        $account = new Account();

        $this->accountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn($account);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($this->validatorMock);

        $this->validatorMock
            ->expects($this->once())
            ->method('validateCreate');

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);
    }

    public function test_createWithoutPassword()
    {
        $account = new Account();

        $this->validatorMock
            ->expects($this->once())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn($account);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($this->validatorMock);

        $createdAccount = $this->accountService->createWithoutPassword($account);

        $this->assertNotNull($createdAccount);
    }

    public function test_getAll_noRecords()
    {
        $this->accountDaoMock
            ->expects($this->any())
            ->method('getAll')
            ->willReturn(array());

        $allAccounts = $this->accountService->getAll();

        $this->assertTrue(is_array($allAccounts));
        $this->assertEmpty($allAccounts);
    }

    public function test_getAll()
    {
        $accounts = array(
            new TestAccount()
        );

        $this->accountDaoMock
            ->expects($this->any())
            ->method('getAll')
            ->willReturn($accounts);

        $allAccounts = $this->accountService->getAll();

        $this->assertTrue(is_array($allAccounts));
        $this->assertCount(1, $allAccounts);
        $this->assertEquals(1, $allAccounts[0]->getAccountId());
    }

    public function test_getById_noRecord()
    {
        $this->accountDaoMock
            ->expects($this->any())
            ->method('getById')
            ->willReturn(null);

        $account = $this->accountService->getById(123);

        $this->assertNull($account);
    }

    public function test_getById()
    {
        $this->accountDaoMock
            ->expects($this->any())
            ->method('getById')
            ->willReturn(new TestAccount());

        $account = $this->accountService->getById(123);

        $this->assertNotNull($account);
        $this->assertEquals(1, $account->getAccountId());
    }

    public function test_getByIds()
    {
        $accounts = array(
            new TestAccount()
        );

        $this->accountDaoMock
            ->expects($this->any())
            ->method('getByIds')
            ->willReturn($accounts);

        $allAccounts = $this->accountService->getByIds(array(1));

        $this->assertTrue(is_array($allAccounts));
        $this->assertCount(1, $allAccounts);
        $this->assertEquals(1, $allAccounts[0]->getAccountId());
    }

    public function test_update()
    {
        $this->accountDaoMock
            ->expects($this->any())
            ->method('updateRecord')
            ->willReturn(new TestAccount());

        $account = new Account();

        $updatedAccount = $this->accountService->update($account, $account->getAccountId());

        $this->assertNotNull($updatedAccount);
        $this->assertEquals(1, $updatedAccount->getAccountId());
    }

    public function test_deactivate()
    {
        $this->accountDaoMock
            ->expects($this->any())
            ->method('updateRecord')
            ->willReturn(new TestAccount());

        $account = new Account();

        $updatedAccount = $this->accountService->deactivate($account);

        $this->assertNotNull($updatedAccount);
        $this->assertEquals(1, $updatedAccount->getAccountId());
    }

    public function test_activate()
    {
        $this->accountDaoMock
            ->expects($this->any())
            ->method('updateRecord')
            ->willReturn(new TestAccount());

        $account = new Account();

        $updatedAccount = $this->accountService->activate($account);

        $this->assertNotNull($updatedAccount);
        $this->assertEquals(1, $updatedAccount->getAccountId());
    }

    public function test_resetPassword()
    {
        $this->accountDaoMock
            ->expects($this->any())
            ->method('updateRecord')
            ->willReturn(new TestAccount());

        $account = new Account();

        $updatedAccount = $this->accountService->resetPassword($account, 'new-password');

        $this->assertNotNull($updatedAccount);
        $this->assertTrue($updatedAccount instanceof Account);
    }

    public function test_verify_nonPendingAccount()
    {
        $account = new Account();
        $account->setVerificationCode('code');
        $account->setStatus(AccountStatusType::ACTIVE);

        $verifiedAccount = $this->accountService->verify($account, 'code');

        $this->assertEquals($account, $verifiedAccount);
    }

    public function test_verify_noVerificationCode()
    {
        $account = new Account();
        $account->setVerificationCode(null);

        $verifiedAccount = $this->accountService->verify($account, 'code');

        $this->assertEquals($account, $verifiedAccount);
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Verification code does not match stored value
     */
    public function test_verify_invalidVerificationCode()
    {
        $account = new Account();
        $account->setVerificationCode('match-me');
        $account->setStatus(AccountStatusType::PENDING);

        $this->accountService->verify($account, 'invalid-match');
    }

    public function test_verify()
    {
        $this->accountDaoMock
            ->expects($this->any())
            ->method('updateRecord')
            ->willReturn(new TestAccount());

        $account = new Account();
        $account->setVerificationCode('match-me');
        $account->setStatus(AccountStatusType::PENDING);

        $verifiedAccount = $this->accountService->verify($account, 'match-me');

        $this->assertNotNull($verifiedAccount);
        $this->assertTrue($verifiedAccount instanceof Account);
    }

    public function test_logout()
    {
        $sessionValue = $this->accountService->generateSessionValue(1, time());

        $this->sessionManagerMock
            ->expects($this->once())
            ->method('read')
            ->willReturn($sessionValue);

        $account = new Account();

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getById')
            ->willReturn($account);

        $this->sessionManagerMock
            ->expects($this->once())
            ->method('clear');

        $this->cookieManagerMock
            ->expects($this->once())
            ->method('clear');

        $this->accountService->logout();
    }

    public function test_getCurrentSession_noSessionOrCookie()
    {
        $this->sessionManagerMock
            ->expects($this->once())
            ->method('read')
            ->willReturn(null);

        $this->assertNull($this->accountService->getCurrentSession());
    }

    public function test_getCurrentSession_usingSession()
    {
        $sessionValue = $this->accountService->generateSessionValue(1, time());

        $this->sessionManagerMock
            ->expects($this->once())
            ->method('read')
            ->willReturn($sessionValue);

        $account = new Account();

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getById')
            ->willReturn($account);

        $sessionAccount = $this->accountService->getCurrentSession();

        $this->assertNotNull($sessionAccount);
        $this->assertEquals($account, $sessionAccount);
    }

    public function test_getCurrentSession_usingCookie_invalid()
    {
        $this->sessionManagerMock
            ->expects($this->once())
            ->method('read')
            ->willReturn(null);

        $this->cookieManagerMock
            ->expects($this->once())
            ->method('read')
            ->willReturn('invalid');

        $this->cookieManagerMock
            ->expects($this->once())
            ->method('clear');

        $cookieAccount = $this->accountService->getCurrentSession();

        $this->assertNull($cookieAccount);
    }

    public function test_getCurrentSession_usingCookie_valid()
    {
        $sessionValue = $this->accountService->generateSessionValue(1, time());

        $this->sessionManagerMock
            ->expects($this->once())
            ->method('read')
            ->willReturn(null);

        $this->cookieManagerMock
            ->expects($this->once())
            ->method('read')
            ->willReturn($sessionValue);

        $account = new Account();

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getById')
            ->with(1)
            ->willReturn($account);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('updateRecord')
            ->with($this->callback(function(Account $account) {
                $this->assertNotNull($account->getLastLoginIP());
                $this->assertNotNull($account->getDateLastLogin());

                return true;
            }));

        $this->sessionManagerMock
            ->expects($this->once())
            ->method('write');

        $cookieAccount = $this->accountService->getCurrentSession();

        $this->assertNotNull($cookieAccount);
        $this->assertEquals($account, $cookieAccount);
    }

    public function test_login_invalidNotActive()
    {
        $this->accountDaoMock
            ->expects($this->once())
            ->method('checkIfEmailOrUsernameValidAndActive')
            ->willReturn(false);

        $this->assertFalse($this->accountService->login(new Account()));
    }

    public function test_login()
    {
        $this->addValidLoginExpects();

        $this->assertTrue($this->accountService->login(new Account()));
    }

    public function test_login_storeCookie()
    {
        $this->accountDaoMock
            ->expects($this->once())
            ->method('checkIfEmailOrUsernameValidAndActive')
            ->willReturn(true);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getAllBy')
            ->willReturn(new Account());

        $this->encryptorServiceMock
            ->expects($this->once())
            ->method('isValid')
            ->willReturn(true);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('updateRecord')
            ->willReturn(new Account());

        $this->sessionManagerMock
            ->expects($this->once())
            ->method('write');

        $this->cookieManagerMock
            ->expects($this->once())
            ->method('write');

        $this->assertTrue($this->accountService->login(new Account(), true));
    }

    public function test_loginWithAutoLoginToken()
    {
        $this->addValidLoginExpects();

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByAutoLoginToken')
            ->willReturn(new Account());

        $this->assertTrue($this->accountService->loginWithAutoLoginToken('token'));
    }

    public function test_loginWithAutoLoginToken_notFound()
    {
        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByAutoLoginToken')
            ->willReturn(null);

        $this->assertFalse($this->accountService->loginWithAutoLoginToken('invalid'));
    }

    public function test_getByAutoLoginToken()
    {
        $account = new TestAccount();

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByAutoLoginToken')
            ->willReturn($account);

        $rawAccount = $this->accountService->getByAutoLoginToken('token');
        $this->assertNotNull($rawAccount);
        $this->assertEquals($account, $rawAccount);
    }

    public function test_getByEmail()
    {
        $account = new TestAccount();

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn($account);

        $rawAccount = $this->accountService->getByEmail('email');
        $this->assertNotNull($rawAccount);
        $this->assertEquals($account, $rawAccount);
    }

    public function test_getByVerificationCode()
    {
        $account = new TestAccount();

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByVerificationCode')
            ->willReturn($account);

        $rawAccount = $this->accountService->getByVerificationCode('verificationCode');
        $this->assertNotNull($rawAccount);
        $this->assertEquals($account, $rawAccount);
    }

    public function test_resetPasswordWithVerification_noPasswordResetCode()
    {
        $account = new TestAccount();
        $generatedPasswordResetCode = sha1($account->getAccountId() . $account->getEmail() . $account->getPassword());

        $this->accountDaoMock
            ->expects($this->once())
            ->method('updateRecord');

        $passwordResetCode = $this->accountService->resetPasswordWithVerification($account, 'new-password');
        $this->assertEquals($generatedPasswordResetCode, $passwordResetCode);
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Password reset code does not match stored value
     */
    public function test_resetPasswordWithVerification_passwordMismatch()
    {
        $account = new TestAccount();
        $this->accountService->resetPasswordWithVerification($account, 'new-password', 'invalid-password-reset-code');
    }

    public function test_resetPasswordWithVerification()
    {
        $account = new TestAccount();
        $account->setPasswordResetCode('code');
        $account->setPasswordResetExpiry($this->accountService->getMySQLNow(time() + (60 + 10)));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('updateRecord')
            ->willReturn($account);

        $updatedAccount = $this->accountService->resetPasswordWithVerification($account, 'new-password', 'code');
        $this->assertNotNull($updatedAccount);
        $this->assertEquals($account, $updatedAccount);
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Password reset code has expired
     */
    public function test_resetPasswordWithVerification_expired()
    {
        $account = new TestAccount();
        $account->setPasswordResetCode('code');
        $account->setPasswordResetExpiry($this->accountService->getMySQLNow(time() - (60 + 10)));

        $updatedAccount = $this->accountService->resetPasswordWithVerification($account, 'new-password', 'code');
        $this->assertNotNull($updatedAccount);
        $this->assertEquals($account, $updatedAccount);
    }

    public function test_delete()
    {
        $account = new TestAccount();

        $this->accountDaoMock
            ->expects($this->once())
            ->method('updateRecord')
            ->willReturn($account);

        $deletedAccount = $this->accountService->delete($account);
        $this->assertNotNull($deletedAccount);
        $this->assertEquals($account, $deletedAccount);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage FacebookClient not set properly
     */
    public function test_loginWithFacebook_invalidFacebookClient()
    {
        $this->accountService->loginWithFacebook();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage something went wrong
     */
    public function test_loginWithFacebook_getAccessToken_handleException()
    {
        $facebookClient = $this->getFacebookClientWithMocks();

        $this->accountService->setFacebookClient($facebookClient);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->throwException(new SocialClientException('something went wrong')));

        $this->accountService->loginWithFacebook();
    }

    public function test_loginWithFacebook_getAccessToken_null()
    {
        $facebookClient = $this->getFacebookClientWithMocks();

        $this->accountService->setFacebookClient($facebookClient);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue(null));

        $this->assertFalse($this->accountService->loginWithFacebook());
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong retrieving user details from Facebook
     */
    public function test_loginWithFacebook_getResponseFromFacebook_null()
    {
        $facebookClient = $this->getFacebookClientWithMocks();

        $this->accountService->setFacebookClient($facebookClient);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue('test'));

        $facebookClient->getFacebook()->expects($this->once())
            ->method('get')
            ->with('/me?fields=id,email,first_name,last_name', 'test')
            ->will($this->returnValue(null));

        $this->accountService->loginWithFacebook();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage A valid Facebook email could not be found
     */
    public function test_loginWithFacebook_getResponseFromFacebook_emptyFacebookUserEmail()
    {
        $facebookClient = $this->getFacebookClientWithMocks();

        $this->accountService->setFacebookClient($facebookClient);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue('test'));

        $facebookClient->getFacebook()->expects($this->once())
            ->method('get')
            ->with('/me?fields=id,email,first_name,last_name', 'test')
            ->will($this->returnValue(new FacebookResponse(new FacebookRequest(), '{"email":""}')));

        $this->accountService->loginWithFacebook();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Social account (1) does not have an existing account associated with it. This should not happen ever.
     */
    public function test_loginWithFacebook_accountChecks_existingSocialAccountWithoutNormalAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $facebookClient = $this->getFacebookClientWithMocks();

        $this->accountService->setFacebookClient($facebookClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue('test'));

        $facebookClient->getFacebook()->expects($this->once())
            ->method('get')
            ->with('/me?fields=id,email,first_name,last_name', 'test')
            ->will($this->returnValue(new FacebookResponse(new FacebookRequest(), '{"email":"test@test.com"}')));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(new TestSocialAccount(1)));

        $this->accountService->loginWithFacebook();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage There was a problem creating your account.
     */
    public function test_loginWithFacebook_accountChecks_noExistingSocialOrNormalAccount_problemCreatingNewNormalAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $facebookClient = $this->getFacebookClientWithMocks();

        $this->accountService->setFacebookClient($facebookClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue('test'));

        $facebookClient->getFacebook()->expects($this->once())
            ->method('get')
            ->with('/me?fields=id,email,first_name,last_name', 'test')
            ->will($this->returnValue(new FacebookResponse(new FacebookRequest(), '{"email":"test@test.com"}')));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->once())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn(null);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($this->validatorMock);

        $this->accountService->loginWithFacebook();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong creating the new social account
     */
    public function test_loginWithFacebook_accountChecks_noExistingSocialOrNormalAccount_problemCreatingNewSocialAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $facebookClient = $this->getFacebookClientWithMocks();

        $createdAccount = new TestAccount();

        $this->accountService->setFacebookClient($facebookClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue('test'));

        $facebookClient->getFacebook()->expects($this->once())
            ->method('get')
            ->with('/me?fields=id,email,first_name,last_name', 'test')
            ->will($this->returnValue(new FacebookResponse(new FacebookRequest(), '{"id":"123","email":"test@test.com"}')));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->once())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn($createdAccount);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($this->validatorMock);

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_FACEBOOK, '123', new SocialAccessToken('test'))
            ->willReturn(null);

        $this->accountService->loginWithFacebook();
    }

    public function test_loginWithFacebook_accountChecks_noExistingSocialOrNormalAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $facebookClient = $this->getFacebookClientWithMocks();

        $createdAccount = new TestAccount();

        $this->accountService->setFacebookClient($facebookClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue('test'));

        $facebookClient->getFacebook()->expects($this->once())
            ->method('get')
            ->with('/me?fields=id,email,first_name,last_name', 'test')
            ->will($this->returnValue(new FacebookResponse(new FacebookRequest(), '{"id":"123","email":"test@test.com"}')));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->once())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($this->validatorMock);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn($createdAccount);

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_FACEBOOK, '123', new SocialAccessToken('test'))
            ->willReturn(new TestSocialAccount(1));

        $this->addValidLoginExpects();

        $this->assertTrue($this->accountService->loginWithFacebook());
    }

    public function test_loginWithFacebook_accountChecks_noExistingSocialOrNormalAccount_shouldNotCreateNewAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $facebookClient = $this->getFacebookClientWithMocks();

        $this->accountService->setFacebookClient($facebookClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue('test'));

        $facebookClient->getFacebook()->expects($this->once())
            ->method('get')
            ->with('/me?fields=id,email,first_name,last_name', 'test')
            ->will($this->returnValue(new FacebookResponse(new FacebookRequest(), '{"id":"123","email":"test@test.com"}')));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->never())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->never())
            ->method('getValidator');

        $this->accountDaoMock
            ->expects($this->never())
            ->method('createRecord');

        $socialAccountServiceMock->expects($this->never())
            ->method('createFromAccount');

        $this->assertFalse($this->accountService->loginWithFacebook(true, false));
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong creating the new social account
     */
    public function test_loginWithFacebook_accountChecks_noExistingSocialButNormalAccountExists_problemCreatingNewSocialAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $facebookClient = $this->getFacebookClientWithMocks();

        $createdAccount = new TestAccount();

        $this->accountService->setFacebookClient($facebookClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue('test'));

        $facebookClient->getFacebook()->expects($this->once())
            ->method('get')
            ->with('/me?fields=id,email,first_name,last_name', 'test')
            ->will($this->returnValue(new FacebookResponse(new FacebookRequest(), '{"id":"123","email":"test@test.com"}')));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn($createdAccount);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(null));

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_FACEBOOK, '123', new SocialAccessToken('test'))
            ->willReturn(null);

        $this->accountService->loginWithFacebook();
    }

    public function test_loginWithFacebook_accountChecks_noExistingSocialButNormalAccountExists()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $facebookClient = $this->getFacebookClientWithMocks();

        $createdAccount = new TestAccount();

        $this->accountService->setFacebookClient($facebookClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue('test'));

        $facebookClient->getFacebook()->expects($this->once())
            ->method('get')
            ->with('/me?fields=id,email,first_name,last_name', 'test')
            ->will($this->returnValue(new FacebookResponse(new FacebookRequest(), '{"id":"123","email":"test@test.com"}')));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn($createdAccount);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(null));

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_FACEBOOK, '123', new SocialAccessToken('test'))
            ->willReturn(new SocialAccount());

        $this->addValidLoginExpects();

        $this->assertTrue($this->accountService->loginWithFacebook());
    }

    public function test_loginWithFacebook_eventDispatched()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $facebookClient = $this->getFacebookClientWithMocks();

        $createdAccount = new TestAccount();
        $testListener = new TestListener();

        $this->accountService->getEventDispatcher()->addListener(Event::SOCIAL_ACCOUNT_LOGIN, $testListener);
        $this->accountService->setFacebookClient($facebookClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue('test'));

        $facebookClient->getFacebook()->expects($this->once())
            ->method('get')
            ->with('/me?fields=id,email,first_name,last_name', 'test')
            ->will($this->returnValue(new FacebookResponse(new FacebookRequest(), '{"id":"123","email":"test@test.com"}')));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->once())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn($createdAccount);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($this->validatorMock);

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_FACEBOOK, '123', new SocialAccessToken('test'))
            ->willReturn(new TestSocialAccount(1));

        $this->addValidLoginExpects();

        $this->assertTrue($this->accountService->loginWithFacebook());
        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Social account logged in successfully.', $testListener->getReceivedMessage());
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage TwitterClient not set properly
     */
    public function test_loginWithTwitter_invalidTwitterClient()
    {
        $this->accountService->loginWithTwitter();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage something went wrong
     */
    public function test_loginWithTwitter_getAccessToken_handleException()
    {
        $twitterClient = $this->getTwitterClientWithMocks();

        $this->accountService->setTwitterClient($twitterClient);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->will($this->throwException(new SocialClientException('something went wrong')));

        $this->accountService->loginWithTwitter();
    }

    public function test_loginWithTwitter_getAccessToken_null()
    {
        $twitterClient = $this->getTwitterClientWithMocks();

        $this->accountService->setTwitterClient($twitterClient);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->willReturn(null);

        $this->assertFalse($this->accountService->loginWithTwitter());
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong retrieving user details from Twitter
     */
    public function test_loginWithTwitter_getResponseFromTwitter_null()
    {
        $twitterClient = $this->getTwitterClientWithMocks();

        $this->accountService->setTwitterClient($twitterClient);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->exactly(2))
            ->method('setOauthToken');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->willReturn(array(
                'oauth_token' => 'token',
                'oauth_token_secret' => 'secret'
            ));

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('get')
            ->with('account/verify_credentials', array(
                'include_email' => 'true',
                'include_entities' => 'false',
                'skip_status' => 'true'
            ))
            ->willReturn(null);

        $this->accountService->loginWithTwitter();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage A valid Twitter email could not be found
     */
    public function test_loginWithTwitter_getResponseFromFacebook_emptyTwitterUserEmail()
    {
        $twitterClient = $this->getTwitterClientWithMocks();

        $this->accountService->setTwitterClient($twitterClient);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->exactly(2))
            ->method('setOauthToken');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->willReturn(array(
                'oauth_token' => 'token',
                'oauth_token_secret' => 'secret'
            ));

        $twitterResponse = new TestTwitterResponse();
        $twitterResponse->email = '';

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('get')
            ->with('account/verify_credentials', array(
                'include_email' => 'true',
                'include_entities' => 'false',
                'skip_status' => 'true'
            ))
            ->willReturn($twitterResponse);

        $this->accountService->loginWithTwitter();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Social account (1) does not have an existing account associated with it. This should not happen ever.
     */
    public function test_loginWithTwitter_accountChecks_existingSocialAccountWithoutNormalAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $twitterClient = $this->getTwitterClientWithMocks();

        $this->accountService->setTwitterClient($twitterClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->exactly(2))
            ->method('setOauthToken');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->willReturn(array(
                'oauth_token' => 'token',
                'oauth_token_secret' => 'secret'
            ));

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('get')
            ->with('account/verify_credentials', array(
                'include_email' => 'true',
                'include_entities' => 'false',
                'skip_status' => 'true'
            ))
            ->willReturn(new TestTwitterResponse());

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_TWITTER)
            ->will($this->returnValue(new TestSocialAccount(1)));

        $this->accountService->loginWithTwitter();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage There was a problem creating your account.
     */
    public function test_loginWithTwitter_accountChecks_noExistingSocialOrNormalAccount_problemCreatingNewNormalAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $twitterClient = $this->getTwitterClientWithMocks();

        $this->accountService->setTwitterClient($twitterClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->exactly(2))
            ->method('setOauthToken');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->willReturn(array(
                'oauth_token' => 'token',
                'oauth_token_secret' => 'secret'
            ));

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('get')
            ->with('account/verify_credentials', array(
                'include_email' => 'true',
                'include_entities' => 'false',
                'skip_status' => 'true'
            ))
            ->willReturn(new TestTwitterResponse());

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_TWITTER)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->once())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn(null);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($this->validatorMock);

        $this->accountService->loginWithTwitter();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong creating the new social account
     */
    public function test_loginWithTwitter_accountChecks_noExistingSocialOrNormalAccount_problemCreatingNewSocialAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $twitterClient = $this->getTwitterClientWithMocks();

        $createdAccount = new TestAccount();

        $this->accountService->setTwitterClient($twitterClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->exactly(2))
            ->method('setOauthToken');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->willReturn(array(
                'oauth_token' => 'token',
                'oauth_token_secret' => 'secret'
            ));

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('get')
            ->with('account/verify_credentials', array(
                'include_email' => 'true',
                'include_entities' => 'false',
                'skip_status' => 'true'
            ))
            ->willReturn(new TestTwitterResponse());

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_TWITTER)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->once())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn($createdAccount);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($this->validatorMock);

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_TWITTER, '123', new SocialAccessToken('token', 'secret'))
            ->willReturn(null);

        $this->accountService->loginWithTwitter();
    }

    public function test_loginWithTwitter_accountChecks_noExistingSocialOrNormalAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $twitterClient = $this->getTwitterClientWithMocks();

        $createdAccount = new TestAccount();

        $this->accountService->setTwitterClient($twitterClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->exactly(2))
            ->method('setOauthToken');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->willReturn(array(
                'oauth_token' => 'token',
                'oauth_token_secret' => 'secret'
            ));

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('get')
            ->with('account/verify_credentials', array(
                'include_email' => 'true',
                'include_entities' => 'false',
                'skip_status' => 'true'
            ))
            ->willReturn(new TestTwitterResponse());

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_TWITTER)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->once())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($this->validatorMock);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn($createdAccount);

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_TWITTER, '123', new SocialAccessToken('token', 'secret'))
            ->willReturn(new TestSocialAccount(1));

        $this->addValidLoginExpects();

        $this->assertTrue($this->accountService->loginWithTwitter());
    }

    public function test_loginWithTwitter_accountChecks_noExistingSocialOrNormalAccount_shouldNotCreateNewAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $twitterClient = $this->getTwitterClientWithMocks();

        $this->accountService->setTwitterClient($twitterClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->exactly(2))
            ->method('setOauthToken');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->willReturn(array(
                'oauth_token' => 'token',
                'oauth_token_secret' => 'secret'
            ));

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('get')
            ->with('account/verify_credentials', array(
                'include_email' => 'true',
                'include_entities' => 'false',
                'skip_status' => 'true'
            ))
            ->willReturn(new TestTwitterResponse());

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_TWITTER)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->never())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->never())
            ->method('getValidator');

        $this->accountDaoMock
            ->expects($this->never())
            ->method('createRecord');

        $socialAccountServiceMock->expects($this->never())
            ->method('createFromAccount');

        $this->assertFalse($this->accountService->loginWithTwitter(true, false));
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong creating the new social account
     */
    public function test_loginWithTwitter_accountChecks_noExistingSocialButNormalAccountExists_problemCreatingNewSocialAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $twitterClient = $this->getTwitterClientWithMocks();

        $createdAccount = new TestAccount();

        $this->accountService->setTwitterClient($twitterClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->exactly(2))
            ->method('setOauthToken');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->willReturn(array(
                'oauth_token' => 'token',
                'oauth_token_secret' => 'secret'
            ));

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('get')
            ->with('account/verify_credentials', array(
                'include_email' => 'true',
                'include_entities' => 'false',
                'skip_status' => 'true'
            ))
            ->willReturn(new TestTwitterResponse());

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn($createdAccount);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_TWITTER)
            ->will($this->returnValue(null));

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_TWITTER, '123', new SocialAccessToken('token', 'secret'))
            ->willReturn(null);

        $this->accountService->loginWithTwitter();
    }

    public function test_loginWithTwitter_accountChecks_noExistingSocialButNormalAccountExists()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $twitterClient = $this->getTwitterClientWithMocks();

        $createdAccount = new TestAccount();

        $this->accountService->setTwitterClient($twitterClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->exactly(2))
            ->method('setOauthToken');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->willReturn(array(
                'oauth_token' => 'token',
                'oauth_token_secret' => 'secret'
            ));

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('get')
            ->with('account/verify_credentials', array(
                'include_email' => 'true',
                'include_entities' => 'false',
                'skip_status' => 'true'
            ))
            ->willReturn(new TestTwitterResponse());

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn($createdAccount);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_TWITTER)
            ->will($this->returnValue(null));

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_TWITTER, '123', new SocialAccessToken('token', 'secret'))
            ->willReturn(new SocialAccount());

        $this->addValidLoginExpects();

        $this->assertTrue($this->accountService->loginWithTwitter());
    }

    public function test_loginWithTwitter_eventDispatched()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $twitterClient = $this->getTwitterClientWithMocks();

        $createdAccount = new TestAccount();
        $testListener = new TestListener();

        $this->accountService->getEventDispatcher()->addListener(Event::SOCIAL_ACCOUNT_LOGIN, $testListener);
        $this->accountService->setTwitterClient($twitterClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->exactly(2))
            ->method('setOauthToken');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->willReturn(array(
                'oauth_token' => 'token',
                'oauth_token_secret' => 'secret'
            ));

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('get')
            ->with('account/verify_credentials', array(
                'include_email' => 'true',
                'include_entities' => 'false',
                'skip_status' => 'true'
            ))
            ->willReturn(new TestTwitterResponse());

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_TWITTER)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->once())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn($createdAccount);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($this->validatorMock);

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_TWITTER, '123', new SocialAccessToken('token', 'secret'))
            ->willReturn(new TestSocialAccount(1));

        $this->addValidLoginExpects();

        $this->assertTrue($this->accountService->loginWithTwitter());
        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Social account logged in successfully.', $testListener->getReceivedMessage());
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage GoogleClient not set properly
     */
    public function test_loginWithGoogle_invalidGoogleClient()
    {
        $this->accountService->loginWithGoogle();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage something went wrong
     */
    public function test_loginWithGoogle_getAccessToken_handleException()
    {
        $googleClient = $this->getGoogleClientWithMocks();

        $this->accountService->setGoogleClient($googleClient);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->will($this->throwException(new SocialClientException('something went wrong')));

        $this->accountService->loginWithGoogle();
    }

    public function test_loginWithGoogle_getAccessToken_null()
    {
        $googleClient = $this->getGoogleClientWithMocks();

        $this->accountService->setGoogleClient($googleClient);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(null);

        $this->assertFalse($this->accountService->loginWithGoogle());
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong retrieving user details from Google
     */
    public function test_loginWithGoogle_getResponseFromGoogle_null()
    {
        $googleClient = $this->getGoogleClientWithMocks(true);

        $this->accountService->setGoogleClient($googleClient);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(array(
                'access_token' => 'token'
            ));

        $this->accountService->loginWithGoogle();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage A valid Google email could not be found
     */
    public function test_loginWithGoogle_getResponseFromGoogle_emptyGoogleEmail()
    {
        $googleClient = $this->getGoogleClientWithMocks(false, true);

        $this->accountService->setGoogleClient($googleClient);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(array(
                'access_token' => 'token'
            ));

        $this->accountService->loginWithGoogle();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Social account (1) does not have an existing account associated with it. This should not happen ever.
     */
    public function test_loginWithGoogle_accountChecks_existingSocialAccountWithoutNormalAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $googleClient = $this->getGoogleClientWithMocks();

        $this->accountService->setGoogleClient($googleClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(array(
                'access_token' => 'token'
            ));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_GOOGLE)
            ->will($this->returnValue(new TestSocialAccount(1)));

        $this->accountService->loginWithGoogle();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage There was a problem creating your account.
     */
    public function test_loginWithGoogle_accountChecks_noExistingSocialOrNormalAccount_problemCreatingNewNormalAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $googleClient = $this->getGoogleClientWithMocks();

        $this->accountService->setGoogleClient($googleClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(array(
                'access_token' => 'token'
            ));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_GOOGLE)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->once())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn(null);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($this->validatorMock);

        $this->accountService->loginWithGoogle();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong creating the new social account
     */
    public function test_loginWithGoogle_accountChecks_noExistingSocialOrNormalAccount_problemCreatingNewSocialAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $googleClient = $this->getGoogleClientWithMocks();

        $createdAccount = new TestAccount();

        $this->accountService->setGoogleClient($googleClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(array(
                'access_token' => 'token'
            ));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_GOOGLE)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->once())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn($createdAccount);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($this->validatorMock);

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_GOOGLE, '123', new SocialAccessToken('token'))
            ->willReturn(null);

        $this->accountService->loginWithGoogle();
    }

    public function test_loginWithGoogle_accountChecks_noExistingSocialOrNormalAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $googleClient = $this->getGoogleClientWithMocks();

        $createdAccount = new TestAccount();

        $this->accountService->setGoogleClient($googleClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(array(
                'access_token' => 'token'
            ));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_GOOGLE)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->once())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($this->validatorMock);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn($createdAccount);

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_GOOGLE, '123', new SocialAccessToken('token'))
            ->willReturn(new TestSocialAccount(1));

        $this->addValidLoginExpects();

        $this->assertTrue($this->accountService->loginWithGoogle());
    }

    public function test_loginWithGoogle_accountChecks_noExistingSocialOrNormalAccount_shouldNotCreateNewAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $googleClient = $this->getGoogleClientWithMocks();

        $this->accountService->setGoogleClient($googleClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(array(
                'access_token' => 'token'
            ));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_GOOGLE)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->never())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->never())
            ->method('getValidator');

        $this->accountDaoMock
            ->expects($this->never())
            ->method('createRecord');

        $socialAccountServiceMock->expects($this->never())
            ->method('createFromAccount');

        $this->assertFalse($this->accountService->loginWithGoogle(true, false));
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong creating the new social account
     */
    public function test_loginWithGoogle_accountChecks_noExistingSocialButNormalAccountExists_problemCreatingNewSocialAccount()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $googleClient = $this->getGoogleClientWithMocks();

        $createdAccount = new TestAccount();

        $this->accountService->setGoogleClient($googleClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(array(
                'access_token' => 'token'
            ));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn($createdAccount);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_GOOGLE)
            ->will($this->returnValue(null));

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_GOOGLE, '123', new SocialAccessToken('token'))
            ->willReturn(null);

        $this->accountService->loginWithGoogle();
    }

    public function test_loginWithGoogle_accountChecks_noExistingSocialButNormalAccountExists()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $googleClient = $this->getGoogleClientWithMocks();

        $createdAccount = new TestAccount();

        $this->accountService->setGoogleClient($googleClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(array(
                'access_token' => 'token'
            ));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn($createdAccount);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_GOOGLE)
            ->will($this->returnValue(null));

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_GOOGLE, '123', new SocialAccessToken('token'))
            ->willReturn(new SocialAccount());

        $this->addValidLoginExpects();

        $this->assertTrue($this->accountService->loginWithGoogle());
    }

    public function test_loginWitGoogle_eventDispatched()
    {
        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);

        $googleClient = $this->getGoogleClientWithMocks();

        $createdAccount = new TestAccount();
        $testListener = new TestListener();

        $this->accountService->getEventDispatcher()->addListener(Event::SOCIAL_ACCOUNT_LOGIN, $testListener);
        $this->accountService->setGoogleClient($googleClient);
        $this->accountService->setSocialAccountService($socialAccountServiceMock);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(array(
                'access_token' => 'token'
            ));

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getByEmail')
            ->willReturn(null);

        $socialAccountServiceMock->expects($this->once())
            ->method('getByEmailAndProviderName')
            ->with('test@test.com', SocialProvider::PROVIDER_NAME_GOOGLE)
            ->will($this->returnValue(null));

        $this->validatorMock
            ->expects($this->once())
            ->method('validateCreateWithoutPassword');

        $this->accountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn($createdAccount);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($this->validatorMock);

        $socialAccountServiceMock->expects($this->once())
            ->method('createFromAccount')
            ->with($createdAccount, SocialProvider::PROVIDER_NAME_GOOGLE, '123', new SocialAccessToken('token'))
            ->willReturn(new TestSocialAccount(1));

        $this->addValidLoginExpects();

        $this->assertTrue($this->accountService->loginWithGoogle());
        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Social account logged in successfully.', $testListener->getReceivedMessage());
    }

    public function test_isLoginWithFacebookDetected()
    {
        $this->assertFalse($this->accountService->isLoginWithFacebookDetected());

        $_GET['code'] = 'code';
        $_GET['state'] = 'state';

        $this->assertTrue($this->accountService->isLoginWithFacebookDetected());
    }

    public function test_isLoginWithTwitterDetected()
    {
        $this->assertFalse($this->accountService->isLoginWithTwitterDetected());

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->assertTrue($this->accountService->isLoginWithTwitterDetected());
    }

    public function test_isLoginWithGoogleDetected()
    {
        $this->assertFalse($this->accountService->isLoginWithGoogleDetected());

        $_GET['code'] = 'code';

        $this->assertTrue($this->accountService->isLoginWithGoogleDetected());
    }

    private function addValidLoginExpects()
    {
        $this->accountDaoMock
            ->expects($this->once())
            ->method('checkIfEmailOrUsernameValidAndActive')
            ->willReturn(true);

        $this->accountDaoMock
            ->expects($this->any())
            ->method('getAllBy')
            ->willReturn(new Account());

        $this->encryptorServiceMock
            ->expects($this->any())
            ->method('isValid')
            ->willReturn(true);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('updateRecord')
            ->willReturn(new Account());

        $this->sessionManagerMock
            ->expects($this->once())
            ->method('write');
    }

}