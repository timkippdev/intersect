<?php

namespace TimKipp\Intersect\Tests\Unit\Services;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Database\Adapters\AbstractAdapter;
use TimKipp\Intersect\Database\Dao\AbstractDao;
use TimKipp\Intersect\Tests\Stubs\TestDomain;
use TimKipp\Intersect\Tests\Stubs\TestService;

class AbstractServiceTest extends TestCase {

    /** @var TestService $testService */
    private $testService;

    /** @var \PHPUnit_Framework_MockObject_MockObject $daoMock */
    private $daoMock;

    /** @var \PHPUnit_Framework_MockObject_MockObject $validatorMock */
    private $validatorMock;

    protected function setUp()
    {
        $databaseMock = $this->getMockBuilder(AbstractAdapter::class)->setConstructorArgs(array('', '', '', ''))->getMock();

        $this->daoMock = $this->getMockBuilder(AbstractDao::class)->setConstructorArgs(array($databaseMock, null))->getMock();
        $this->validatorMock = $this->getMockBuilder(\TimKipp\Intersect\Validation\AbstractValidator::class)->getMock();

        $this->testService = new TestService($databaseMock);
        //$this->testService->setValidator($this->validatorMock);
        $this->testService->setDao($this->daoMock);
    }

    public function test_getAll()
    {
        $this->daoMock
            ->expects($this->any())
            ->method('getAll')
            ->willReturn(array());

        $allAccounts = $this->testService->getAll();

        $this->assertTrue(is_array($allAccounts));
        $this->assertEmpty($allAccounts);
    }

    public function test_getById()
    {
        $this->daoMock
            ->expects($this->any())
            ->method('getById')
            ->willReturn('test');

        $account = $this->testService->getById(1);

        $this->assertEquals('test', $account);
    }

    public function test_getByIds()
    {
        $this->daoMock
            ->expects($this->any())
            ->method('getByIds')
            ->willReturn(array(new TestDomain()));

        $accounts = $this->testService->getByIds(array());

        $this->assertCount(1, $accounts);
    }

    public function test_update()
    {
        $this->validatorMock
            ->expects($this->any())
            ->method('validateUpdate');

        $this->daoMock
            ->expects($this->any())
            ->method('updateRecord')
            ->willReturn('test');

        $account = $this->testService->update(new TestDomain(), 1);

        $this->assertEquals('test', $account);
    }

    public function test_create()
    {
        $this->validatorMock
            ->expects($this->any())
            ->method('validateCreate');

        $this->daoMock
            ->expects($this->any())
            ->method('createRecord')
            ->willReturn('test');

        $account = $this->testService->create(new TestDomain());

        $this->assertEquals('test', $account);
    }

    public function test_delete()
    {
        $this->validatorMock
            ->expects($this->any())
            ->method('validateDelete');

        $this->daoMock
            ->expects($this->any())
            ->method('deleteRecord')
            ->willReturn('test');

        $account = $this->testService->delete(new TestDomain());

        $this->assertEquals('test', $account);
    }

}