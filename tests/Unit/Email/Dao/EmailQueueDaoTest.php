<?php

namespace TimKipp\Intersect\Tests\Unit\Email\Dao;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Email\Dao\EmailQueueDao;
use TimKipp\Intersect\Tests\Stubs\TestDomain;

class EmailQueueDaoTest extends TestCase {

    /** @var \PHPUnit_Framework_MockObject_MockObject $adapterMock */
    private $adapterMock;

    /** @var EmailQueueDao */
    private $emailQueueDao;

    protected function setUp()
    {
        parent::setUp();

        $this->adapterMock = $this->getMockBuilder('TimKipp\Intersect\Database\Adapters\AbstractAdapter')->setConstructorArgs(array('', '', '', ''))->getMock();

        $this->emailQueueDao = new EmailQueueDao($this->adapterMock, TestDomain::class);
    }

    public function test_deleteAll()
    {
        $this->adapterMock->expects($this->once())
            ->method('query')
            ->with($this->equalTo("DELETE FROM email_queue"));

        $this->emailQueueDao->deleteAll();
    }

    public function test_deleteAllSent()
    {
        $this->adapterMock->expects($this->once())
            ->method('query')
            ->with($this->equalTo("DELETE FROM email_queue WHERE date_sent IS NOT NULL"));

        $this->emailQueueDao->deleteAllSent();
    }

}