<?php

namespace TimKipp\Intersect\Tests\Unit\Controllers;

use TimKipp\Intersect\Controllers\LoginController;
use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Domain\SocialProvider;
use TimKipp\Intersect\Tests\Unit\BaseUnitTest;

class LoginControllerTest extends BaseUnitTest {

    /** @var LoginController $signUpController */
    private $loginController;

    protected function setUp()
    {
        parent::setUp();

        $this->loginController = new LoginController(
            array(
                'databaseAdapter' => $this->databaseAdapterMock,
                'accountService' => $this->accountServiceMock
            )
        );
    }

    public function test_processLogout()
    {
        $this->accountServiceMock
            ->expects($this->once())
            ->method('logout');

        $this->loginController->processLogout();
    }

    /**
     * @throws \Exception
     */
    public function test_processSocialLoginCallback_facebookDetected()
    {
        $this->accountServiceMock
            ->expects($this->once())
            ->method('isLoginWithFacebookDetected')
            ->willReturn(true);

        $this->accountServiceMock
            ->expects($this->once())
            ->method('loginWithFacebook');

        $this->loginController->processSocialLoginCallback();
    }

    /**
     * @throws \Exception
     */
    public function test_processSocialLoginCallback_twitterDetected()
    {
        $this->accountServiceMock
            ->expects($this->once())
            ->method('isLoginWithTwitterDetected')
            ->willReturn(true);

        $this->accountServiceMock
            ->expects($this->once())
            ->method('loginWithTwitter');

        $this->loginController->processSocialLoginCallback();
    }

    /**
     * @throws \Exception
     */
    public function test_processSocialLoginCallback_googleDetected()
    {
        $this->accountServiceMock
            ->expects($this->once())
            ->method('isLoginWithGoogleDetected')
            ->willReturn(true);

        $this->accountServiceMock
            ->expects($this->once())
            ->method('loginWithGoogle');

        $this->loginController->processSocialLoginCallback();
    }

    /**
     * @throws \Exception
     */
    public function test_processSocialLoginCallback_noProviderDetected()
    {
        $this->accountServiceMock
            ->expects($this->once())
            ->method('isLoginWithFacebookDetected')
            ->willReturn(false);

        $this->accountServiceMock
            ->expects($this->once())
            ->method('isLoginWithTwitterDetected')
            ->willReturn(false);

        $this->accountServiceMock
            ->expects($this->once())
            ->method('isLoginWithGoogleDetected')
            ->willReturn(false);

        $this->accountServiceMock
            ->expects($this->never())
            ->method('loginWithFacebook');

        $this->accountServiceMock
            ->expects($this->never())
            ->method('loginWithTwitter');

        $this->accountServiceMock
            ->expects($this->never())
            ->method('loginWithGoogle');

        $this->loginController->processSocialLoginCallback();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Unsupported social provider
     * @throws \Exception
     */
    public function test_redirectToSocialProvider_unsupportedProvider()
    {
        $this->loginController->redirectToSocialProvider('invalid', true);
    }

    /**
     * @throws \Exception
     */
    public function test_redirectToSocialProvider_facebook()
    {
        $expectedRedirectUrl = 'http://test.com';

        $this->accountServiceMock
            ->expects($this->once())
            ->method('getFacebookLoginUrl')
            ->willReturn($expectedRedirectUrl);

        $rawRedirectUrl = $this->loginController->redirectToSocialProvider(SocialProvider::PROVIDER_NAME_FACEBOOK, true);

        $this->assertEquals($expectedRedirectUrl, $rawRedirectUrl);
    }

    /**
     * @throws \Exception
     */
    public function test_redirectToSocialProvider_twitter()
    {
        $expectedRedirectUrl = 'http://test.com';

        $this->accountServiceMock
            ->expects($this->once())
            ->method('getTwitterLoginUrl')
            ->willReturn($expectedRedirectUrl);

        $rawRedirectUrl = $this->loginController->redirectToSocialProvider(SocialProvider::PROVIDER_NAME_TWITTER, true);

        $this->assertEquals($expectedRedirectUrl, $rawRedirectUrl);
    }

    /**
     * @throws \Exception
     */
    public function test_redirectToSocialProvider_google()
    {
        $expectedRedirectUrl = 'http://test.com';

        $this->accountServiceMock
            ->expects($this->once())
            ->method('getGoogleLoginUrl')
            ->willReturn($expectedRedirectUrl);

        $rawRedirectUrl = $this->loginController->redirectToSocialProvider(SocialProvider::PROVIDER_NAME_GOOGLE, true);

        $this->assertEquals($expectedRedirectUrl, $rawRedirectUrl);
    }

    public function test_processLogin_missingPassword()
    {
        $this->accountServiceMock
            ->expects($this->never())
            ->method('login');

        $this->loginController->processLogin();
    }

    public function test_processLogin_missingEmailAndUsername()
    {
        $_POST['password'] = 'password';

        $this->accountServiceMock
            ->expects($this->never())
            ->method('login');

        $this->loginController->processLogin();
    }

    public function test_processLogin_emailOnly()
    {
        $_POST['password'] = 'password';
        $_POST['email'] = 'test@test.com';

        $this->accountServiceMock
            ->expects($this->once())
            ->method('login')
            ->with($this->callback(
                function (Account $account){
                    $this->assertEquals('test@test.com', $account->getEmail());
                    $this->assertEquals('password', $account->getPassword());
                    $this->assertNull($account->getUsername());

                    return true;
                })
            );

        $this->loginController->processLogin();
    }

    public function test_processLogin_usernameOnly()
    {
        $_POST['password'] = 'password';
        $_POST['username'] = 'test';

        $this->accountServiceMock
            ->expects($this->once())
            ->method('login')
            ->with($this->callback(
                function (Account $account){
                    $this->assertEquals('test', $account->getUsername());
                    $this->assertEquals('password', $account->getPassword());
                    $this->assertNull($account->getEmail());

                    return true;
                })
            );

        $this->loginController->processLogin();
    }

    public function test_processLogin_emailAndUsername()
    {
        $_POST['password'] = 'password';
        $_POST['email'] = 'test@test.com';
        $_POST['username'] = 'test';

        $this->accountServiceMock
            ->expects($this->once())
            ->method('login')
            ->with($this->callback(
                function (Account $account){
                    $this->assertEquals('test@test.com', $account->getEmail());
                    $this->assertEquals('test', $account->getUsername());
                    $this->assertEquals('password', $account->getPassword());

                    return true;
                })
            );

        $this->loginController->processLogin();
    }

}