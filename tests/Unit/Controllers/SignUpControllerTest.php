<?php

namespace TimKipp\Intersect\Tests\Unit\Controllers;

use TimKipp\Intersect\Controllers\SignUpController;
use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Tests\Unit\BaseUnitTest;

class SignUpControllerTest extends BaseUnitTest {

    /** @var SignUpController $signUpController */
    private $signUpController;

    protected function setUp()
    {
        parent::setUp();

        $this->signUpController = new SignUpController(
            array(
                'databaseAdapter' => $this->databaseAdapterMock,
                'accountService' => $this->accountServiceMock
            )
        );
    }

    public function test_processSignUp_simple()
    {
        $_POST['email'] = 'test@test.com';
        $_POST['password'] = 'password';

        $this->accountServiceMock
            ->expects($this->once())
            ->method('create')
            ->with($this->callback(
                function (Account $account){
                    $this->assertEquals('test@test.com', $account->getEmail());
                    $this->assertEquals('password', $account->getPassword());
                    $this->assertNull($account->getFirstName());
                    $this->assertNull($account->getLastName());
                    $this->assertNull($account->getUsername());

                    return true;
                })
            );

        $this->signUpController->processSignUp();
    }

    public function test_processSignUp_advanced()
    {
        $_POST['email'] = 'test@test.com';
        $_POST['password'] = 'password';
        $_POST['username'] = 'username';
        $_POST['firstName'] = 'first';
        $_POST['lastName'] = 'last';
        $_POST['source'] = 'source';

        $this->accountServiceMock
            ->expects($this->once())
            ->method('create')
            ->with($this->callback(
                function (Account $account){
                    $this->assertEquals('test@test.com', $account->getEmail());
                    $this->assertEquals('password', $account->getPassword());
                    $this->assertEquals('username', $account->getUsername());
                    $this->assertEquals('first', $account->getFirstName());
                    $this->assertEquals('last', $account->getLastName());
                    $this->assertEquals('source', $account->getRegistrationSource());

                    return true;
                })
            );

        $this->signUpController->processSignUp();
    }

    public function test_processSignUp_passwordLess()
    {
        $_POST['email'] = 'test@test.com';
        $_POST['isPasswordLess'] = true;

        $this->accountServiceMock
            ->expects($this->once())
            ->method('createWithoutPassword')
            ->with($this->callback(
                function (Account $account){
                    $this->assertEquals('test@test.com', $account->getEmail());

                    return true;
                })
            );

        $this->signUpController->processSignUp();
    }

    public function test_processSignUp_missingEmail()
    {
        $this->accountServiceMock
            ->expects($this->never())
            ->method('create');

        $this->signUpController->processSignUp();
    }

}