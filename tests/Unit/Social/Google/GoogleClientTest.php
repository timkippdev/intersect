<?php

namespace TimKipp\Intersect\Tests\Unit\Social\Google;

use TimKipp\Intersect\Social\Google\GoogleClient;
use TimKipp\Intersect\Social\Google\GoogleConfiguration;
use TimKipp\Intersect\Tests\Stubs\TestGoogleClient;
use TimKipp\Intersect\Tests\Unit\BaseUnitTest;

class TwitterClientTest extends BaseUnitTest {

    /** @var TestGoogleClient $googleClient */
    private $googleClient;

    protected function setUp()
    {
        parent::setUp();

         $this->googleClient= $this->getGoogleClientWithMocks();
    }

    /**
     * @expectedException \TimKipp\Intersect\Social\SocialClientException
     * @expectedExceptionMessage GoogleConfiguration clientId is not set
     */
    public function test_googleClient_requiredFields_missingConsumerKey()
    {
         new GoogleClient(new GoogleConfiguration('', '', ''));
    }

    /**
     * @expectedException \TimKipp\Intersect\Social\SocialClientException
     * @expectedExceptionMessage GoogleConfiguration clientSecret is not set
     */
    public function test_googleClient_requiredFields_missingConsumerSecret()
    {
        new GoogleClient(new GoogleConfiguration('123', '', ''));
    }

    /**
     * @expectedException \TimKipp\Intersect\Social\SocialClientException
     * @expectedExceptionMessage GoogleConfiguration callbackUrl is not set
     */
    public function test_googleClient_requiredFields_missingCallbackUrl()
    {
        new GoogleClient(new GoogleConfiguration('123', '456', ''));
    }

    public function test_getLoginUrl()
    {
        $expectedLoginUrl = 'https://accounts.google.com/o/oauth2/auth';

        $this->googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('createAuthUrl')
            ->willReturn($expectedLoginUrl);

        $this->assertEquals($expectedLoginUrl, $this->googleClient->getLoginUrl());
    }

    public function test_getAccessToken()
    {
        $_GET['code'] = 'code';

        $this->googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(array(
                'access_token' => 'token'
            ));

        $accessToken = $this->googleClient->getAccessToken();
        $this->assertNotNull($accessToken);
        $this->assertEquals('token', $accessToken->getAccessToken());
    }

    /**
     * @expectedException \TimKipp\Intersect\Social\SocialClientException
     * @expectedExceptionMessage something went wrong
     */
    public function test_getAccessToken_handleException()
    {
        $_GET['code'] = 'code';

        $this->googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(array(
                'error' => 'error',
                'error_description' => 'something went wrong'
            ));

        $this->googleClient->getAccessToken();
    }

}