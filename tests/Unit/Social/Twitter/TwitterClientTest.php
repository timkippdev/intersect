<?php

namespace TimKipp\Intersect\Tests\Unit\Social\Twitter;

use Abraham\TwitterOAuth\TwitterOAuthException;
use TimKipp\Intersect\Social\Twitter\TwitterClient;
use TimKipp\Intersect\Social\Twitter\TwitterConfiguration;
use TimKipp\Intersect\Tests\Stubs\TestTwitterClient;
use TimKipp\Intersect\Tests\Unit\BaseUnitTest;

class TwitterClientTest extends BaseUnitTest {

    /** @var TestTwitterClient $twitterClient */
    private $twitterClient;

    protected function setUp()
    {
        parent::setUp();

         $this->twitterClient= $this->getTwitterClientWithMocks();
    }

    /**
     * @expectedException \TimKipp\Intersect\Social\SocialClientException
     * @expectedExceptionMessage TwitterConfiguration consumerKey is not set
     */
    public function test_twitterClient_requiredFields_missingConsumerKey()
    {
         new TwitterClient(new TwitterConfiguration('', '', ''), $this->sessionManagerMock);
    }

    /**
     * @expectedException \TimKipp\Intersect\Social\SocialClientException
     * @expectedExceptionMessage TwitterConfiguration consumerSecret is not set
     */
    public function test_twitterClient_requiredFields_missingConsumerSecret()
    {
        new TwitterClient(new TwitterConfiguration('123', '', ''), $this->sessionManagerMock);
    }

    /**
     * @expectedException \TimKipp\Intersect\Social\SocialClientException
     * @expectedExceptionMessage TwitterConfiguration callbackUrl is not set
     */
    public function test_twitterClient_requiredFields_missingCallbackUrl()
    {
        new TwitterClient(new TwitterConfiguration('123', '456', ''), $this->sessionManagerMock);
    }

    public function test_getLoginUrl()
    {
        $expectedLoginUrl = 'https://api.twitter.com/oauth/authorize?oauth_token=pfUxqAAAAAAA3wTgAAABYIJT91Y';

        $this->twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/request_token', array('oauth_callback' => 'http://localhost'))
            ->will($this->returnValue(array(
                'oauth_token' => 'token',
                'oauth_token_secret' => 'secret'
            )));

        $this->sessionManagerMock
            ->expects($this->at(0))
            ->method('write')
            ->with('twitter_oauth_token', 'token');

        $this->sessionManagerMock
            ->expects($this->at(1))
            ->method('write')
            ->with('twitter_oauth_token_secret', 'secret');

        $this->twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('url')
            ->with('oauth/authorize', array('oauth_token' => 'token'))
            ->will($this->returnValue($expectedLoginUrl));

        $this->assertEquals($expectedLoginUrl, $this->twitterClient->getLoginUrl());
    }

    public function test_getAccessToken()
    {
        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $this->twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('setOauthToken');

        $this->twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->willReturn(array(
                'oauth_token' => 'token',
                'oauth_token_secret' => 'secret'
            ));

        $accessToken = $this->twitterClient->getAccessToken();
        $this->assertNotNull($accessToken);
        $this->assertEquals('token', $accessToken->getAccessToken());
        $this->assertEquals('secret', $accessToken->getAccessTokenSecret());
    }

    /**
     * @expectedException \TimKipp\Intersect\Social\SocialClientException
     * @expectedExceptionMessage something went wrong
     */
    public function test_getAccessToken_handleException()
    {
        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $this->twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('setOauthToken');

        $this->twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->will($this->throwException(new TwitterOAuthException('something went wrong')));

        $this->twitterClient->getAccessToken();
    }

}