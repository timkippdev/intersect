<?php

namespace TimKipp\Intersect\Tests\Unit\Social\Facebook;

use Facebook\Exceptions\FacebookSDKException;
use TimKipp\Intersect\Social\Facebook\FacebookClient;
use TimKipp\Intersect\Social\Facebook\FacebookConfiguration;
use TimKipp\Intersect\Tests\Stubs\TestFacebookClient;
use TimKipp\Intersect\Tests\Unit\BaseUnitTest;

class FacebookClientTest extends BaseUnitTest {

    /** @var TestFacebookClient $facebookClient */
    private $facebookClient;

    private $facebookConfiguration;

    protected function setUp()
    {
        $this->facebookConfiguration = new FacebookConfiguration('123', '456', 'http://localhost');

         $this->facebookClient = $this->getFacebookClientWithMocks();
    }

    /**
     * @expectedException \TimKipp\Intersect\Social\SocialClientException
     * @expectedExceptionMessage FacebookConfiguration applicationId is not set
     */
    public function test_facebookClient_requiredFields_missingApplicationId()
    {
         new FacebookClient(new FacebookConfiguration('', '', ''));
    }

    /**
     * @expectedException \TimKipp\Intersect\Social\SocialClientException
     * @expectedExceptionMessage FacebookConfiguration applicationSecret is not set
     */
    public function test_facebookClient_requiredFields_missingApplicationSecret()
    {
        new FacebookClient(new FacebookConfiguration('123', '', ''));
    }

    public function test_getLoginUrl()
    {
        $this->facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getLoginUrl')
            ->will($this->returnValue('https://www.facebooks.com?scope=email'));

        $loginUrl = $this->facebookClient->getLoginUrl(array(
            'email'
        ));

        $this->assertEquals('https://www.facebooks.com?scope=email', $loginUrl);
    }

    public function test_getAccessToken()
    {
        $this->facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue('test'));

        $accessToken = $this->facebookClient->getAccessToken();
        $this->assertNotNull($accessToken);
        $this->assertEquals('test', $accessToken->getAccessToken());
    }

    /**
     * @expectedException \TimKipp\Intersect\Social\SocialClientException
     * @expectedExceptionMessage something went wrong
     */
    public function test_getAccessToken_handleException()
    {
        $this->facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->throwException(new FacebookSDKException('something went wrong')));

        $this->facebookClient->getAccessToken();
    }

}