<?php

namespace TimKipp\Intersect\Tests\Unit\Social\Facebook;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Social\Facebook\FacebookConfiguration;

class FacebookConfigurationTest extends TestCase {

    /** @var FacebookConfiguration $facebookConfiguration */
    private $facebookConfiguration;

    protected function setUp()
    {
        $this->facebookConfiguration = new FacebookConfiguration('123', '456', 'http://localhost');
    }

    public function test_toFacebookArray()
    {
        $facebookArray = $this->facebookConfiguration->toFacebookArray();

        $this->assertTrue(is_array($facebookArray));

        $this->assertArrayHasKey('app_id', $facebookArray);
        $this->assertArrayHasKey('app_secret', $facebookArray);
        $this->assertArrayHasKey('default_graph_version', $facebookArray);
        $this->assertArrayHasKey('enable_beta_mode', $facebookArray);
        $this->assertArrayHasKey('http_client_handler', $facebookArray);
        $this->assertArrayHasKey('persistent_data_handler', $facebookArray);
        $this->assertArrayHasKey('pseudo_random_string_generator', $facebookArray);
        $this->assertArrayHasKey('url_detection_handler', $facebookArray);
    }

}