<?php

namespace TimKipp\Intersect\Tests\IT\Services;

use TimKipp\Intersect\Tests\IT\BaseITTest;
use TimKipp\Intersect\Tests\Stubs\TestGenericDomain;

class GenericServiceTest extends BaseITTest {

    public function test_create()
    {
        $genericDomain = new TestGenericDomain();

        $domain = $this->genericService->create($genericDomain);

        $this->assertNotNull($domain);
        $this->assertTrue($domain instanceof TestGenericDomain);
    }

    public function test_getById()
    {
        $genericDomain = new TestGenericDomain();

        $domain = $this->genericService->create($genericDomain);

        $this->assertEquals($domain, $this->genericService->getById($domain->getId()));
    }

    public function test_delete()
    {
        $genericDomain = new TestGenericDomain();

        /** @var TestGenericDomain $createdDomain */
        $createdDomain = $this->genericService->create($genericDomain);
        $this->assertNotNull($createdDomain);

        $idValue = $createdDomain->getId();

        $this->genericService->delete($createdDomain);

        $this->assertNull($this->genericService->getById($idValue));
    }

    public function test_update()
    {
        $genericDomain = new TestGenericDomain();

        /** @var TestGenericDomain $createdDomain */
        $createdDomain = $this->genericService->create($genericDomain);
        $this->assertNotNull($createdDomain);

        $createdDomain->setMessage('updated from test');

        /** @var TestGenericDomain $updatedDomain */
        $updatedDomain = $this->genericService->update($createdDomain, $createdDomain->getId());

        $this->assertNotNull($updatedDomain);

        $this->assertEquals('updated from test', $updatedDomain->getMessage());
    }

}