<?php

namespace TimKipp\Intersect\Tests\IT\Services;

use TimKipp\Intersect\Services\SocialProviderService;
use TimKipp\Intersect\Tests\IT\BaseITTest;
use TimKipp\Intersect\Tests\Stubs\TestSocialProvider;

class SocialProviderServiceTest extends BaseITTest {

    /** @var SocialProviderService $socialProviderService */
    private $socialProviderService;

    protected function setUp()
    {
        parent::setUp();

        $this->socialProviderService = new SocialProviderService(self::$DB);
    }

    public function test_create()
    {
        $sampleSocialProvider = new TestSocialProvider();

        $createdSocialProvider = $this->socialProviderService->create($sampleSocialProvider);

        $this->assertNotNull($createdSocialProvider);
        $this->assertNotNull($createdSocialProvider->getProviderId());
        $this->assertEquals($sampleSocialProvider->getProviderName(), $createdSocialProvider->getProviderName());
    }

    public function test_getByName()
    {
        $createdSocialProvider = $this->socialProviderService->create(new TestSocialProvider());

        $existingSocialProvider = $this->socialProviderService->getByName($createdSocialProvider->getProviderName());

        $this->assertNotNull($existingSocialProvider);
        $this->assertEquals($createdSocialProvider->getProviderId(), $existingSocialProvider->getProviderId());
        $this->assertEquals($createdSocialProvider->getProviderName(), $existingSocialProvider->getProviderName());
    }

}