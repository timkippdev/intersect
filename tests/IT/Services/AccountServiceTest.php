<?php

namespace TimKipp\Intersect\Tests\IT\Services;

use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Domain\TemporalDomain;
use TimKipp\Intersect\Event\Event;
use TimKipp\Intersect\Tests\IT\BaseITTest;
use TimKipp\Intersect\Tests\Stubs\TestAccount;
use TimKipp\Intersect\Tests\Stubs\TestAccountService;
use TimKipp\Intersect\Tests\Stubs\TestListener;
use TimKipp\Intersect\Types\AccountStatusType;
use TimKipp\Intersect\Utility\IPAddressUtility;

class AccountServiceTest extends BaseITTest {

    protected function setUp()
    {
        parent::setUp();

        $this->accountService->setCookieManager($this->cookieManager);
        $this->accountService->setSessionManager($this->sessionManager);
    }

    public function test_create()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);
        $this->assertNotNull($createdAccount->getAccountId());
        $this->assertEquals($account->getEmail(), $createdAccount->getEmail());
        $this->assertEquals($account->getPassword(), $createdAccount->getPassword());
        $this->assertEquals($account->getUsername(), $createdAccount->getUsername());
        $this->assertEquals($account->getFirstName(), $createdAccount->getFirstName());
        $this->assertEquals($account->getLastName(), $createdAccount->getLastName());
        $this->assertNotNull($createdAccount->getStatus());
        $this->assertEquals(AccountStatusType::PENDING, $createdAccount->getStatus());
        $this->assertEquals(1, $createdAccount->getRole());
        $this->assertNotNull($createdAccount->getDateCreated());
    }

    public function test_create_generatedAutoLoginToken()
    {
        $account = new TestAccount();
        $account->setAutoLoginToken(null);

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);
        $this->assertNotNull($createdAccount->getAutoLoginToken());
    }

    public function test_create_defaultRole()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);
        $this->assertNotNull($createdAccount->getAccountId());
        $this->assertEquals(1, $createdAccount->getRole());
    }

    public function test_create_suppliedRole()
    {
        $account = new TestAccount();
        $account->setRole(4);

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);
        $this->assertNotNull($createdAccount->getAccountId());
        $this->assertEquals(4, $createdAccount->getRole());
    }

    public function test_create_eventDispatched()
    {
        $account = new TestAccount();
        $testListener = new TestListener();

        $dispatcher = $this->accountService->getEventDispatcher();
        $dispatcher->addListener(Event::ACCOUNT_CREATED, $testListener);

        $this->accountService->create($account);

        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Account created successfully.', $testListener->getReceivedMessage());
    }

    public function test_create_failed_eventDispatched()
    {
        $account = new TestAccount();
        $testListener = new TestListener();

        $this->accountService->create($account);

        $dispatcher = $this->accountService->getEventDispatcher();
        $dispatcher->addListener(Event::ACCOUNT_CREATED_FAILED, $testListener);

        $this->accountService->create($account);

        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('There was a problem creating your account.', $testListener->getReceivedMessage());
    }

    public function test_createWithoutPassword()
    {
        $account = new TestAccount();
        $account->setPassword(null);

        $createdAccount = $this->accountService->createWithoutPassword($account);

        $this->assertNotNull($createdAccount);
        $this->assertNotNull($createdAccount->getAccountId());
        $this->assertEquals($account->getEmail(), $createdAccount->getEmail());
        $this->assertNull($createdAccount->getPassword());
        $this->assertEquals($account->getUsername(), $createdAccount->getUsername());
        $this->assertEquals($account->getFirstName(), $createdAccount->getFirstName());
        $this->assertEquals($account->getLastName(), $createdAccount->getLastName());
        $this->assertNotNull($createdAccount->getStatus());
        $this->assertEquals(AccountStatusType::PENDING, $createdAccount->getStatus());
        $this->assertEquals(1, $createdAccount->getRole());
    }

    public function test_createWithoutPassword_eventDispatched()
    {
        $account = new TestAccount();
        $account->setPassword(null);

        $testListener = new TestListener();

        $dispatcher = $this->accountService->getEventDispatcher();
        $dispatcher->addListener(Event::ACCOUNT_CREATED, $testListener);

        $this->accountService->createWithoutPassword($account);

        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Account created successfully.', $testListener->getReceivedMessage());
    }

    public function test_getAll()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);

        $allAccounts = $this->accountService->getAll();

        $this->assertNotEmpty($allAccounts);
    }

    public function test_getById()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);

        $singleAccount = $this->accountService->getById($createdAccount->getAccountId());

        $this->assertNotNull($singleAccount);
        $this->assertEquals($createdAccount->getAccountId(), $singleAccount->getAccountId());
    }

    public function test_getByIds()
    {
        $createdAccountIds = array();

        $createdAccount1 = $this->accountService->create(new TestAccount());
        $createdAccountIds[] = $createdAccount1->getAccountId();

        $createdAccount2 = $this->accountService->create(new TestAccount());
        $createdAccountIds[] = $createdAccount2->getAccountId();

        $this->assertNotEmpty($createdAccountIds);
        $this->assertCount(2, $createdAccountIds);

        $allAccountsByIds = $this->accountService->getByIds($createdAccountIds);

        $this->assertNotEmpty($allAccountsByIds);
        $this->assertCount(2, $allAccountsByIds);
        $this->assertEquals($createdAccount1, $allAccountsByIds[0]);
        $this->assertEquals($createdAccount2, $allAccountsByIds[1]);
    }

    public function test_update()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);

        $uniqueEmail = 'email-' . uniqid();
        $uniqueUsername = 'username-' . uniqid();

        $createdAccount->setUsername($uniqueUsername);
        $createdAccount->setEmail($uniqueEmail);
        $createdAccount->setStatus(AccountStatusType::DEACTIVATED);

        $updatedAccount = $this->accountService->update($createdAccount, $createdAccount->getAccountId());

        $this->assertNotNull($updatedAccount);
        $this->assertEquals($createdAccount->getAccountId(), $updatedAccount->getAccountId());
        $this->assertEquals($uniqueEmail, $updatedAccount->getEmail());
        $this->assertEquals($uniqueUsername, $updatedAccount->getUsername());
        $this->assertEquals(AccountStatusType::DEACTIVATED, $updatedAccount->getStatus());
    }

    public function test_delete()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);
        $this->assertEquals(AccountStatusType::PENDING, $createdAccount->getStatus());

        $deactivatedAccount = $this->accountService->delete($createdAccount);

        $this->assertNotNull($deactivatedAccount);
        $this->assertEquals(AccountStatusType::DEACTIVATED, $deactivatedAccount->getStatus());
    }

    public function test_deactivate()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);
        $this->assertEquals(AccountStatusType::PENDING, $createdAccount->getStatus());

        $deactivatedAccount = $this->accountService->deactivate($createdAccount);

        $this->assertNotNull($deactivatedAccount);
        $this->assertEquals(AccountStatusType::DEACTIVATED, $deactivatedAccount->getStatus());
    }

    public function test_activate()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);
        $this->assertEquals(AccountStatusType::PENDING, $createdAccount->getStatus());

        $activatedAccount = $this->accountService->activate($createdAccount);

        $this->assertNotNull($activatedAccount);
        $this->assertEquals(AccountStatusType::ACTIVE, $activatedAccount->getStatus());
    }

    public function test_resetPassword()
    {
        $account = new TestAccount();

        /** @var Account $createdAccount */
        $createdAccount = $this->accountService->create($account);

        $oldPassword = $createdAccount->getPassword();
        $oldSalt = $createdAccount->getSalt();

        $this->assertNotNull($createdAccount);
        $this->assertEquals(AccountStatusType::PENDING, $createdAccount->getStatus());

        $updatedAccount = $this->accountService->resetPassword($createdAccount, 'new-password');
        $this->assertNotNull($updatedAccount);

        $this->assertNotNull($updatedAccount->getPassword());
        $this->assertNotNull($updatedAccount->getSalt());
        $this->assertNotEquals('new-password', $updatedAccount->getPassword());
        $this->assertNotEquals($oldPassword, $updatedAccount->getPassword());
        $this->assertNotEquals($oldSalt, $updatedAccount->getSalt());
    }

    public function test_resetPassword_eventDispatched()
    {
        $account = new TestAccount();
        $testListener = new TestListener();

        $dispatcher = $this->accountService->getEventDispatcher();
        $dispatcher->addListener(Event::ACCOUNT_PASSWORD_RESET, $testListener);

        $createdAccount = $this->accountService->create($account);
        $this->assertNotNull($createdAccount);

        $updatedAccount = $this->accountService->resetPassword($createdAccount, 'new-password');
        $this->assertNotNull($updatedAccount);

        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Account password was reset successfully.', $testListener->getReceivedMessage());
    }

    public function test_resetPasswordWithVerification()
    {
        $account = new TestAccount();

        /** @var Account $createdAccount */
        $createdAccount = $this->accountService->create($account);

        $oldPassword = $createdAccount->getPassword();
        $oldSalt = $createdAccount->getSalt();

        $this->assertNotNull($createdAccount);
        $this->assertEquals(AccountStatusType::PENDING, $createdAccount->getStatus());

        // first test generation of password_reset_code
        $passwordResetCode = $this->accountService->resetPasswordWithVerification($createdAccount, 'new-password');

        /** @var Account $account */
        $account = $this->accountService->getById($createdAccount->getAccountId());

        $this->assertNotNull($passwordResetCode);
        $this->assertNotNull($account->getPasswordResetCode());
        $this->assertNotNull($account->getPasswordResetExpiry());

        $updatedAccount = $this->accountService->resetPasswordWithVerification($createdAccount, 'new-password', $passwordResetCode);

        $this->assertNotNull($updatedAccount->getPassword());
        $this->assertNotNull($updatedAccount->getSalt());
        $this->assertNotEquals('new-password', $updatedAccount->getPassword());
        $this->assertNotEquals($oldPassword, $updatedAccount->getPassword());
        $this->assertNotEquals($oldSalt, $updatedAccount->getSalt());
    }

    /**
     * @expectedException  \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Password reset code has expired
     */
    public function test_resetPasswordWithVerification_expired()
    {
        $account = new TestAccount();
        $testListener = new TestListener();

        $dispatcher = $this->accountService->getEventDispatcher();
        $dispatcher->addListener(Event::ACCOUNT_PASSWORD_RESET_FAILED, $testListener);

        /** @var Account $createdAccount */
        $createdAccount = $this->accountService->create($account);
        $this->assertNotNull($createdAccount);

        $passwordResetCode = $this->accountService->resetPasswordWithVerification($createdAccount, 'new-password');

        $createdAccount->setPasswordResetExpiry($this->getMySQLNow(time() - (60 + 10)));
        $this->accountService->update($createdAccount, $createdAccount->getAccountId());

        $updatedAccount = $this->accountService->resetPasswordWithVerification($createdAccount, 'new-password', $passwordResetCode);
        $this->assertNull($updatedAccount->getPasswordResetCode());
        $this->assertNull($updatedAccount->getPasswordResetExpiry());

        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Password reset code has expired', $testListener->getReceivedMessage());
    }

    public function test_resetPasswordWithVerification_eventDispatched()
    {
        $account = new TestAccount();
        $testListener = new TestListener();

        $dispatcher = $this->accountService->getEventDispatcher();
        $dispatcher->addListener(Event::ACCOUNT_PASSWORD_RESET, $testListener);

        $createdAccount = $this->accountService->create($account);
        $this->assertNotNull($createdAccount);

        $passwordResetCode = $this->accountService->resetPasswordWithVerification($createdAccount, 'new-password');
        $updatedAccount = $this->accountService->resetPasswordWithVerification($createdAccount, 'new-password', $passwordResetCode);
        $this->assertNull($updatedAccount->getPasswordResetCode());

        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Account password was reset successfully.', $testListener->getReceivedMessage());
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Password reset code does not match stored value
     */
    public function test_resetPasswordWithVerification_invalidPasswordResetCode()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);
        $this->assertEquals(AccountStatusType::PENDING, $createdAccount->getStatus());

        // first test generation of password_reset_code
        $passwordResetCode = $this->accountService->resetPasswordWithVerification($createdAccount, 'new-password');
        $this->assertNotNull($passwordResetCode);

        $this->accountService->resetPasswordWithVerification($createdAccount, 'new-password', 'invalid-password-reset-code');
    }

    public function test_verify_nonPendingAccount()
    {
        $account = new TestAccount();
        $account->setStatus(AccountStatusType::ACTIVE);

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);
        $this->assertNotNull($createdAccount->getVerificationCode());

        $verifiedAccount = $this->accountService->verify($createdAccount, '');

        $this->assertEquals($createdAccount, $verifiedAccount);
    }

    public function test_verify_noVerificationCode()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);
        $createdAccount->setVerificationCode(null);

        $this->assertNotNull($createdAccount);

        $verifiedAccount = $this->accountService->verify($createdAccount, '');

        $this->assertEquals($createdAccount, $verifiedAccount);
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Verification code does not match stored value
     */
    public function test_verify_invalidVerificationCode()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);

        $this->accountService->verify($createdAccount, 'invalid-verification-code');
    }

    public function test_verify()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);

        $verifiedAccount = $this->accountService->verify($createdAccount, $createdAccount->getVerificationCode());

        $this->assertNotNull($verifiedAccount);
        $this->assertEquals(AccountStatusType::ACTIVE, $verifiedAccount->getStatus());
        $this->assertNull($verifiedAccount->getVerificationCode());
    }

    public function test_getByAutoLoginToken()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);

        $accountFromToken = $this->accountService->getByAutoLoginToken($createdAccount->getAutoLoginToken());
        $this->assertNotNull($accountFromToken);
        $this->assertEquals($createdAccount, $accountFromToken);
    }

    public function test_getByAutoLoginToken_invalidToken()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);

        $accountFromToken = $this->accountService->getByAutoLoginToken('invalid');
        $this->assertNull($accountFromToken);
    }

    public function test_getByEmail()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);

        $accountFromEmail = $this->accountService->getByEmail($createdAccount->getEmail());
        $this->assertNotNull($accountFromEmail);
        $this->assertEquals($createdAccount, $accountFromEmail);
    }

    public function test_getByEmail_invalidEmail()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);

        $accountFromEmail = $this->accountService->getByEmail('invalid');
        $this->assertNull($accountFromEmail);
    }

    public function test_getByVerificationCode()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);

        $accountFromVerificationCode = $this->accountService->getByVerificationCode($createdAccount->getVerificationCode());
        $this->assertNotNull($accountFromVerificationCode);
        $this->assertEquals($createdAccount, $accountFromVerificationCode);
    }

    public function test_getByVerificationCode_invalidVerificationCode()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);

        $accountFromVerificationCode = $this->accountService->getByVerificationCode('invalid');
        $this->assertNull($accountFromVerificationCode);
    }

    public function test_getCurrentSession_fromSession()
    {
        $account = new TestAccount();
        $createdAccount = $this->accountService->create($account);

        $sessionValue = $this->accountService->generateSessionValue($createdAccount->getAccountId(), time());

        $this->sessionManager->write(TestAccountService::$SESSION_NAME, $sessionValue);

        $accountFromSession = $this->accountService->getCurrentSession();
        $this->assertNotNull($accountFromSession);
        $this->assertEquals($createdAccount, $accountFromSession);
    }

    public function test_getCurrentSession_fromCookie()
    {
        $account = new TestAccount();
        $createdAccount = $this->accountService->create($account);

        $sessionValue = $this->accountService->generateSessionValue($createdAccount->getAccountId(), time());

        $this->sessionManager->write(TestAccountService::$SESSION_NAME, $sessionValue);

        $accountFromCookie = $this->accountService->getCurrentSession();
        $this->assertNotNull($accountFromCookie);
        $this->assertEquals($createdAccount->getAccountId(), $accountFromCookie->getAccountId());

        $this->assertEquals($sessionValue, $this->sessionManager->read(TestAccountService::$SESSION_NAME));
    }

    public function test_getCurrentSession_fromCookieNotFound()
    {
        $account = new TestAccount();
        $this->accountService->create($account);

        $_COOKIE[TestAccountService::$SESSION_NAME] = 'invalid';

        $this->assertNull($this->accountService->getCurrentSession());
    }

    public function test_getCurrentSession_notFound()
    {
        $this->assertNull($this->accountService->getCurrentSession());
    }

    public function test_login_invalidCredentials()
    {
        $account = new TestAccount();
        $account->setStatus(AccountStatusType::ACTIVE);

        $this->accountService->create($account);

        $account->setPassword('invalid');

        $this->assertFalse($this->accountService->login($account));
    }

    public function test_login()
    {
        $accountToCreate = new TestAccount();
        $accountToCreate->setStatus(AccountStatusType::ACTIVE);

        $accountToLogin = new Account();
        $accountToLogin->setEmail($accountToCreate->getEmail());
        $accountToLogin->setPassword($accountToCreate->getPassword());

        $createdAccount = $this->accountService->create($accountToCreate);

        $this->assertNotNull($createdAccount);

        $this->assertTrue($this->accountService->login($accountToLogin));

        $updatedAccount = $this->accountService->getById($createdAccount->getAccountId());

        $this->assertEquals($createdAccount->getAccountId(), $updatedAccount->getAccountId());
        $this->assertNotNull($this->sessionManager->read(TestAccountService::$SESSION_NAME));
    }

    public function test_login_dateLastLoginUpdated()
    {
        $accountToCreate = new TestAccount();
        $accountToCreate->setStatus(AccountStatusType::ACTIVE);

        $accountToLogin = new Account();
        $accountToLogin->setEmail($accountToCreate->getEmail());
        $accountToLogin->setPassword($accountToCreate->getPassword());

        $createdAccount = $this->accountService->create($accountToCreate);
        $this->assertNotNull($createdAccount);

        $dateLastUpdated = $createdAccount->getDateLastLogin();

        $this->assertTrue($this->accountService->login($accountToLogin));

        $updatedAccount = $this->accountService->getById($createdAccount->getAccountId());

        $this->assertNotNull($updatedAccount->getDateLastLogin());
        $this->assertNotEquals($dateLastUpdated, $updatedAccount->getDateLastLogin());
    }

    public function test_login_lastLoginIPUpdated()
    {
        $accountToCreate = new TestAccount();
        $accountToCreate->setStatus(AccountStatusType::ACTIVE);

        $accountToLogin = new Account();
        $accountToLogin->setEmail($accountToCreate->getEmail());
        $accountToLogin->setPassword($accountToCreate->getPassword());

        $ipAddress = IPAddressUtility::get();

        $createdAccount = $this->accountService->create($accountToCreate);
        $this->assertNotNull($createdAccount);

        $this->assertNull($createdAccount->getLastLoginIP());

        $this->assertTrue($this->accountService->login($accountToLogin));

        $updatedAccount = $this->accountService->getById($createdAccount->getAccountId());

        $this->assertNotNull($updatedAccount->getLastLoginIP());
        $this->assertEquals($ipAddress, $updatedAccount->getLastLoginIP());
    }

    public function test_loginWithAutoLoginCookie()
    {
        $account = new TestAccount();
        $account->setStatus(AccountStatusType::ACTIVE);

        $createdAccount = $this->accountService->create($account);

        $this->assertNull($this->sessionManager->read(TestAccountService::$SESSION_NAME));

        $this->assertNotNull($createdAccount->getAutoLoginToken());
        $this->assertTrue($this->accountService->loginWithAutoLoginToken($createdAccount->getAutoLoginToken()));

        $this->assertNotNull($this->sessionManager->read(TestAccountService::$SESSION_NAME));
    }

    public function test_login_eventDispatched()
    {
        $accountToCreate = new TestAccount();
        $accountToCreate->setStatus(AccountStatusType::ACTIVE);

        $accountToLogin = new Account();
        $accountToLogin->setEmail($accountToCreate->getEmail());
        $accountToLogin->setPassword($accountToCreate->getPassword());

        $testListener = new TestListener();

        $this->accountService->getEventDispatcher()->addListener(Event::ACCOUNT_LOGIN, $testListener);
        $createdAccount = $this->accountService->create($accountToCreate);
        $this->assertNotNull($createdAccount);

        $this->assertTrue($this->accountService->login($accountToLogin));
        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Account logged in successfully.', $testListener->getReceivedMessage());
    }

    public function test_logout()
    {
        $account = new TestAccount();
        $account->setStatus(AccountStatusType::ACTIVE);

        $createdAccount = $this->accountService->create($account);

        $sessionValue = $this->accountService->generateSessionValue($createdAccount->getAccountId(), time());

        $this->cookieManager->write(TestAccountService::$SESSION_NAME, $sessionValue);

        $this->assertNotNull($this->cookieManager->read(TestAccountService::$SESSION_NAME));

        $this->accountService->logout();

        $this->assertNull($this->sessionManager->read(TestAccountService::$SESSION_NAME));
        $this->assertNull($this->cookieManager->read(TestAccountService::$SESSION_NAME));
    }

    public function test_logout_eventDispatched()
    {
        $account = new TestAccount();
        $account->setStatus(AccountStatusType::ACTIVE);

        $testListener = new TestListener();

        $createdAccount = $this->accountService->create($account);

        $sessionValue = $this->accountService->generateSessionValue($createdAccount->getAccountId(), time());

        $this->sessionManager->write(TestAccountService::$SESSION_NAME, $sessionValue);

        $this->accountService->getEventDispatcher()->addListener(Event::ACCOUNT_LOGOUT, $testListener);
        $this->accountService->logout();

        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Account was logged out successfully.', $testListener->getReceivedMessage());
    }

    public function test_dateColumns()
    {
        $account = new TestAccount();

        $this->assertNull($account->getDateCreated());

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount->getDateCreated());
        $this->assertNull($createdAccount->getDateUpdated());

        $updatedAccount = $this->accountService->update($createdAccount, $createdAccount->getAccountId());

        $this->assertNotNull($updatedAccount->getDateUpdated());
    }

    public function test_create_registrationSource()
    {
        $account = new TestAccount();
        $account->setRegistrationSource('test');

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);
        $this->assertNotNull($createdAccount->getRegistrationSource());
        $this->assertEquals('test', $createdAccount->getRegistrationSource());
    }

    public function test_create_registrationSource_verifyLowercase()
    {
        $account = new TestAccount();
        $account->setRegistrationSource('UNITTest');

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);
        $this->assertNotNull($createdAccount->getRegistrationSource());
        $this->assertEquals('unittest', $createdAccount->getRegistrationSource());
    }

    public function test_create_dateCreatedNotOverwritten()
    {
        $account = new TestAccount();

        $now = $this->getMySQLNow();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);
        $this->assertEquals($now, $createdAccount->getDateCreated());
    }

    public function test_update_dateUpdatedNotOverwritten()
    {
        $account = new TestAccount();

        $now = $this->getMySQLNow();
        $account->setDateUpdated($now);

        $createdAccount = $this->accountService->create($account);

        $updatedAccount = $this->accountService->update($createdAccount, $createdAccount->getAccountId());

        $this->assertNotNull($updatedAccount);
        $this->assertEquals($now, $updatedAccount->getDateUpdated());
    }

    public function test_update_autoLoginTokenNotOverwritten()
    {
        $account = new TestAccount();

        $createdAccount = $this->accountService->create($account);

        $updatedAccount = $this->accountService->update($createdAccount, $createdAccount->getAccountId());

        $this->assertNotNull($updatedAccount);
        $this->assertEquals($createdAccount->getAutoLoginToken(), $updatedAccount->getAutoLoginToken());
    }

    public function test_create_registrationIP()
    {
        $account = new TestAccount();
        $ipAddress = IPAddressUtility::get();

        $createdAccount = $this->accountService->create($account);

        $this->assertNotNull($createdAccount);
        $this->assertNotNull($createdAccount->getRegistrationIP());
        $this->assertEquals($ipAddress, $createdAccount->getRegistrationIP());
    }

    public function test_verifyTemporalColumns()
    {
        $account = new TestAccount();
        $this->assertTrue($account instanceof TemporalDomain);
        $this->assertNotNull($account->getDateCreatedColumn());
        $this->assertNotNull($account->getDateUpdatedColumn());
        $this->assertEquals('date_created', $account->getDateCreatedColumn());
        $this->assertEquals('date_updated', $account->getDateUpdatedColumn());
    }

}