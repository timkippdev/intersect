<?php

namespace TimKipp\Intersect\Tests\IT;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Email\Services\EmailQueueService;
use TimKipp\Intersect\Email\Services\EmailService;
use TimKipp\Intersect\Helper\CookieManager;
use TimKipp\Intersect\Helper\SessionManager;
use TimKipp\Intersect\Order\Services\OrderService;
use TimKipp\Intersect\Order\Services\PaymentTypeService;
use TimKipp\Intersect\Services\AccountService;
use TimKipp\Intersect\Services\GenericService;
use TimKipp\Intersect\Tests\Stubs\TestAccountService;
use TimKipp\Intersect\Tests\Stubs\TestCookieManager;
use TimKipp\Intersect\Tests\Stubs\TestEmailSender;
use TimKipp\Intersect\Tests\Stubs\TestGenericDomain;

abstract class BaseITTest extends TestCase {

    /** @var TestDatabaseAdapter $DB */
    protected static $DB;

    /** @var TestAccountService $accountService */
    protected $accountService;

    /** @var SessionManager $sessionManager */
    protected $sessionManager;

    /** @var CookieManager $cookieManager */
    protected $cookieManager;

    /** @var EmailService $emailService */
    protected $emailService;

    /** @var EmailQueueService $emailQueueService */
    protected $emailQueueService;

    /** @var TestEmailSender $emailSender */
    protected $emailSender;

    /** @var GenericService $genericService  */
    protected $genericService;

    /** @var OrderService $orderService */
    protected $orderService;

    /** @var PaymentTypeService $paymentTypeService */
    protected $paymentTypeService;

    public static function setUpBeforeClass()
    {
        self::$DB = new TestDatabaseAdapter();
        self::$DB->selectDatabase(DB_NAME);
    }

    protected function setUp()
    {
        parent::setUp();

        unset($_POST);
        unset($_GET);

        $this->accountService = new TestAccountService(self::$DB);
        $this->sessionManager = new SessionManager();
        $this->cookieManager = new TestCookieManager();

        $this->emailSender = new TestEmailSender();
        $this->emailService = new EmailService(self::$DB, $this->emailSender);
        $this->emailQueueService = new EmailQueueService(self::$DB);

        $this->orderService = new OrderService(self::$DB);
        $this->paymentTypeService = new PaymentTypeService(self::$DB);

        $this->genericService = new GenericService(self::$DB, TestGenericDomain::class, GENERIC_TABLE_NAME);

        $this->sessionManager->destroy();
    }

    /**
     * @param null $timestamp
     * @return false|string
     */
    public function getMySQLNow($timestamp = null)
    {
        return date('Y-m-d H:i:s', (is_null($timestamp) ? time() : $timestamp));
    }

}