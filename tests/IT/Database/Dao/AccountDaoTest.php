<?php

namespace TimKipp\Intersect\Tests\IT\Database\Dao;

use TimKipp\Intersect\Tests\Stubs\TestAccount;
use TimKipp\Intersect\Types\AccountStatusType;

class AccountDaoTest extends BaseDaoTest {
    
    public function test_createRecord()
    {
        $sampleAccount = new TestAccount();

        $newAccount = $this->accountDao->createRecord($sampleAccount);

        $this->assertNotNull($newAccount->getAccountId());
        $this->assertEquals($newAccount->getEmail(), $sampleAccount->getEmail());
        $this->assertEquals($newAccount->getUsername(), $sampleAccount->getUsername());
        $this->assertEquals($newAccount->getPassword(), $sampleAccount->getPassword());
        $this->assertEquals($sampleAccount->getStatus(), $sampleAccount->getStatus());
        $this->assertEquals($sampleAccount->getAutoLoginToken(), $sampleAccount->getAutoLoginToken());
    }

    public function test_deleteRecord()
    {
        $sampleAccount = new TestAccount();

        $newAccount = $this->accountDao->createRecord($sampleAccount);

        $sampleAccount->setAccountId($newAccount->getAccountId());

        $didDeleteAccount = $this->accountDao->deleteRecord($sampleAccount);
        $this->assertTrue($didDeleteAccount);

        $deletedAccount = $this->accountDao->getById($sampleAccount->getAccountId());

        $this->assertNull($deletedAccount);
    }

    public function test_updateRecord()
    {
        $sampleAccount = new TestAccount();

        $newAccount = $this->accountDao->createRecord($sampleAccount);

        $sampleAccount->setAccountId($newAccount->getAccountId());
        $sampleAccount->setEmail('email-' . uniqid());
        $sampleAccount->setUsername('username-' . uniqid());
        $sampleAccount->setPassword('password-updated');
        $sampleAccount->setStatus(AccountStatusType::DEACTIVATED);

        $updatedAccount = $this->accountDao->updateRecord($sampleAccount, $sampleAccount->getAccountId());

        $this->assertEquals($sampleAccount->getAccountId(), $updatedAccount->getAccountId());
        $this->assertEquals($sampleAccount->getEmail(), $updatedAccount->getEmail());
        $this->assertEquals($sampleAccount->getUsername(), $updatedAccount->getUsername());
        $this->assertEquals($sampleAccount->getPassword(), $updatedAccount->getPassword());
        $this->assertEquals(AccountStatusType::DEACTIVATED, $updatedAccount->getStatus());
    }

    public function test_checkIfAccountValidAndActive()
    {
        $sampleAccount = new TestAccount();
        $sampleAccount->setStatus(AccountStatusType::ACTIVE);

        $newAccount = $this->accountDao->createRecord($sampleAccount);

        $this->assertTrue($this->accountDao->checkIfEmailOrUsernameValidAndActive($newAccount));

        $newAccount->setEmail('doesnotexist@test.com');
        $newAccount->setUsername('doesnotexist');

        $this->assertFalse($this->accountDao->checkIfEmailOrUsernameValidAndActive($newAccount));
    }

    public function test_getByEmailUsernameAndPassword()
    {
        $sampleAccount = new TestAccount();
        $newAccount = $this->accountDao->createRecord($sampleAccount);

        $this->assertNull($this->accountDao->getByEmailUsernameAndPassword('invalid', 'invalid', 'invalid'));

        $rawAccount = $this->accountDao->getByEmailUsernameAndPassword($newAccount->getEmail(), $newAccount->getUsername(), $newAccount->getPassword());
        $this->assertNotNull($rawAccount);

        $this->assertEquals($newAccount->getAccountId(), $rawAccount->getAccountId());
        $this->assertEquals($newAccount->getEmail(), $rawAccount->getEmail());
        $this->assertEquals($newAccount->getUsername(), $rawAccount->getUsername());
        $this->assertEquals($newAccount->getPassword(), $rawAccount->getPassword());
    }

    public function test_getByAutoLoginToken()
    {
        $sampleAccount = new TestAccount();
        $newAccount = $this->accountDao->createRecord($sampleAccount);

        $this->assertNull($this->accountDao->getByAutoLoginToken('invalid'));

        $rawAccount = $this->accountDao->getByAutoLoginToken($newAccount->getAutoLoginToken());
        $this->assertNotNull($rawAccount);

        $this->assertEquals($newAccount->getAccountId(), $rawAccount->getAccountId());
    }

    public function test_getByEmail()
    {
        $sampleAccount = new TestAccount();
        $newAccount = $this->accountDao->createRecord($sampleAccount);

        $this->assertNull($this->accountDao->getByEmail('invalid'));

        $rawAccount = $this->accountDao->getByEmail($newAccount->getEmail());
        $this->assertNotNull($rawAccount);

        $this->assertEquals($newAccount->getAccountId(), $rawAccount->getAccountId());
        $this->assertEquals($newAccount->getEmail(), $rawAccount->getEmail());
        $this->assertEquals($newAccount->getUsername(), $rawAccount->getUsername());
        $this->assertEquals($newAccount->getPassword(), $rawAccount->getPassword());
    }

    public function test_getByVerificationCode()
    {
        $sampleAccount = new TestAccount();
        $newAccount = $this->accountDao->createRecord($sampleAccount);

        $this->assertNull($this->accountDao->getByVerificationCode('invalid'));

        $rawAccount = $this->accountDao->getByVerificationCode($newAccount->getVerificationCode());
        $this->assertNotNull($rawAccount);

        $this->assertEquals($newAccount->getAccountId(), $rawAccount->getAccountId());
        $this->assertEquals($newAccount->getEmail(), $rawAccount->getEmail());
        $this->assertEquals($newAccount->getUsername(), $rawAccount->getUsername());
        $this->assertEquals($newAccount->getPassword(), $rawAccount->getPassword());
    }

}