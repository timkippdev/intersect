<?php

namespace TimKipp\Intersect\Tests\IT\Database\Dao;

use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Domain\SocialAccount;
use TimKipp\Intersect\Domain\TemporalDomain;
use TimKipp\Intersect\Tests\Stubs\TestAccount;
use TimKipp\Intersect\Tests\Stubs\TestSocialAccount;

class SocialAccountDaoTest extends BaseDaoTest {

    /** @var Account $createdAccount */
    private $createdAccount;

    protected function setUp()
    {
        parent::setUp();

        $this->createdAccount = $this->accountDao->createRecord(new TestAccount());
    }

    public function test_createRecord()
    {
        $sampleSocialAccount = new TestSocialAccount($this->createdAccount->getAccountId());

        $createdSocialAccount = $this->socialAccountDao->createRecord($sampleSocialAccount);

        $this->assertNotNull($createdSocialAccount);
        $this->assertNotNull($createdSocialAccount->getId());
        $this->assertEquals($sampleSocialAccount->getEmail(), $sampleSocialAccount->getEmail());
        $this->assertEquals($sampleSocialAccount->getProviderId(), $sampleSocialAccount->getProviderId());
        $this->assertEquals($sampleSocialAccount->getProviderAccessToken(), $sampleSocialAccount->getProviderAccessToken());
        $this->assertEquals($sampleSocialAccount->getProviderAccountId(), $sampleSocialAccount->getProviderAccountId());
        $this->assertEquals($sampleSocialAccount->getStatus(), $sampleSocialAccount->getStatus());
        $this->assertEquals($sampleSocialAccount->getAccountId(), $sampleSocialAccount->getAccountId());
        $this->assertNotNull($sampleSocialAccount->getDateCreated());
    }

    public function test_getAllForProviderId()
    {
        $results = $this->socialAccountDao->getAllForProviderId(2);
        $this->assertCount(0, $results);

        $this->socialAccountDao->createRecord(new TestSocialAccount($this->createdAccount->getAccountId()));

        $results = $this->socialAccountDao->getAllForProviderId(1);
        $this->assertNotNull($results);
        $this->assertTrue(count($results) > 0);
    }

    public function test_getByEmailAndProviderId()
    {
        $result = $this->socialAccountDao->getByEmailAndProviderId('doesnotexist', 2);
        $this->assertNull($result);

        $createdSocialAccount = $this->socialAccountDao->createRecord(new TestSocialAccount($this->createdAccount->getAccountId()));

        $result = $this->socialAccountDao->getByEmailAndProviderId($createdSocialAccount->getEmail(), 1);

        $this->assertNotNull($result);
        $this->assertEquals($createdSocialAccount->getId(), $result->getId());
    }

    public function test_updateRecord()
    {
        $sampleSocialAccount = new TestSocialAccount($this->createdAccount->getAccountId());

        /** @var SocialAccount $createdSocialAccount */
        $createdSocialAccount = $this->socialAccountDao->createRecord($sampleSocialAccount);

        $this->assertNotNull($createdSocialAccount);

        $updatedEmail = 'test_' . uniqid() . '@test.com';
        $createdSocialAccount->setEmail($updatedEmail);

        /** @var SocialAccount $updatedSocialAccount */
        $updatedSocialAccount = $this->socialAccountDao->updateRecord($createdSocialAccount, $createdSocialAccount->getId());
        $this->assertNotNull($updatedSocialAccount);

        $this->assertNotNull($updatedSocialAccount->getDateUpdated());
        $this->assertEquals($updatedEmail, $updatedSocialAccount->getEmail());
    }

    public function test_verifyTemporalColumns()
    {
        $sampleSocialAccount = new TestSocialAccount($this->createdAccount->getAccountId());
        $this->assertTrue($sampleSocialAccount instanceof TemporalDomain);
        $this->assertNotNull($sampleSocialAccount->getDateCreatedColumn());
        $this->assertNotNull($sampleSocialAccount->getDateUpdatedColumn());
        $this->assertEquals('date_created', $sampleSocialAccount->getDateCreatedColumn());
        $this->assertEquals('date_updated', $sampleSocialAccount->getDateUpdatedColumn());
    }

}