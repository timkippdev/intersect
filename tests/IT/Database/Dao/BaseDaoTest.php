<?php

namespace TimKipp\Intersect\Tests\IT\Database\Dao;

use TimKipp\Intersect\Database\Dao\AccountDao;
use TimKipp\Intersect\Database\Dao\SocialAccountDao;
use TimKipp\Intersect\Database\Dao\SocialProviderDao;
use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Domain\SocialAccount;
use TimKipp\Intersect\Domain\SocialProvider;
use TimKipp\Intersect\Order\Dao\OrderDao;
use TimKipp\Intersect\Order\Dao\PaymentDao;
use TimKipp\Intersect\Order\Dao\PaymentTypeDao;
use TimKipp\Intersect\Order\Domain\Order;
use TimKipp\Intersect\Order\Domain\Payment;
use TimKipp\Intersect\Order\Domain\PaymentType;
use TimKipp\Intersect\Tests\IT\BaseITTest;

abstract class BaseDaoTest extends BaseITTest {

    /** @var AccountDao $accountDao */
    protected $accountDao;

    /** @var SocialAccountDao $socialAccountDao */
    protected $socialAccountDao;

    /** @var SocialProviderDao $socialProviderDao */
    protected $socialProviderDao;

    /** @var OrderDao $orderDao */
    protected $orderDao;

    /** @var PaymentDao $paymentDao */
    protected $paymentDao;

    /** @var PaymentTypeDao $paymentTypeDao */
    protected $paymentTypeDao;

    protected function setUp()
    {
        parent::setUp();

        $this->accountDao = new AccountDao(self::$DB, Account::class);
        $this->socialAccountDao = new SocialAccountDao(self::$DB, SocialAccount::class);
        $this->socialProviderDao = new SocialProviderDao(self::$DB, SocialProvider::class);
        $this->orderDao = new OrderDao(self::$DB, Order::class);
        $this->paymentDao = new PaymentDao(self::$DB, Payment::class);
        $this->paymentTypeDao = new PaymentTypeDao(self::$DB, PaymentType::class);
    }

}