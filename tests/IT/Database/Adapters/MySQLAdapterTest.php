<?php

namespace TimKipp\Intersect\Tests\IT\Database\Adapters;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Database\Adapters\MySQLAdapter;

class MySQLAdapterTest extends TestCase {

    private static $MYSQL_HOST = 'localhost';
    private static $MYSQL_USER = 'root';
    private static $MYSQL_PASS = 'root';
    private static $MYSQL_NAME = 'intersect_tests';
    private static $TEST_TABLE = 'test';

    /**
     * @var MySQLAdapter
     */
    private static $mysqlAdapter;

    public static function setUpBeforeClass()
    {
        self::$mysqlAdapter = new MySQLAdapter(self::$MYSQL_HOST, self::$MYSQL_USER, self::$MYSQL_PASS);

        self::$mysqlAdapter->createDatabase(self::$MYSQL_NAME);

        $createdDatabase = self::$mysqlAdapter->query("SHOW DATABASES LIKE '" . self::$MYSQL_NAME . "'");
        self::assertTrue($createdDatabase->getCount() > 0);
    }

    public static function tearDownAfterClass()
    {
        self::$mysqlAdapter->dropDatabase(self::$MYSQL_NAME);

        $droppedDatabase = self::$mysqlAdapter->query("SHOW DATABASES LIKE '" . self::$MYSQL_NAME . "'");
        self::assertTrue($droppedDatabase->getCount() == 0);
    }

    public function test_createDatabase()
    {
        // just asserting true in this test because createDatabase is tested in
        // setUpBeforeClass method but still wanted a test stub for the method for sanity
        $this->assertTrue(true);
    }

    public function test_dropDatabase()
    {
        $databaseName = self::$MYSQL_NAME . '_TBD';
        self::$mysqlAdapter = new MySQLAdapter(self::$MYSQL_HOST, self::$MYSQL_USER, self::$MYSQL_PASS);

        self::$mysqlAdapter->createDatabase($databaseName);

        $createdDatabase = self::$mysqlAdapter->query("SHOW DATABASES LIKE '" . $databaseName . "'");
        self::assertTrue($createdDatabase->getCount() > 0);

        self::$mysqlAdapter->dropDatabase($databaseName);

        $droppedDatabase = self::$mysqlAdapter->query("SHOW DATABASES LIKE '" . $databaseName . "'");
        self::assertTrue($droppedDatabase->getCount() == 0);
    }

    public function test_selectDatabase()
    {
        self::$mysqlAdapter->selectDatabase(self::$MYSQL_NAME);

        $this->assertEquals(self::$MYSQL_NAME, self::$mysqlAdapter->getDatabase());
    }

    public function test_dropTable()
    {
        self::$mysqlAdapter->selectDatabase(self::$MYSQL_NAME);

        self::$mysqlAdapter->query("CREATE TABLE " . self::$TEST_TABLE . "_tbd (id INT(11) AUTO_INCREMENT PRIMARY KEY)");

        $this->assertTrue(self::$mysqlAdapter->tableExists(self::$TEST_TABLE . '_tbd'));

        self::$mysqlAdapter->dropTable(self::$TEST_TABLE . '_tbd');

        $this->assertFalse(self::$mysqlAdapter->tableExists(self::$TEST_TABLE . '_tbd'));
    }

    public function test_query()
    {
        self::$mysqlAdapter->selectDatabase(self::$MYSQL_NAME);

        self::$mysqlAdapter->query("CREATE TABLE " . self::$TEST_TABLE . " (id INT(11) AUTO_INCREMENT PRIMARY KEY)");

        self::$mysqlAdapter->query("INSERT INTO " . self::$TEST_TABLE . " (id) VALUES (1)");

        $results = self::$mysqlAdapter->query("SELECT * FROM " . self::$TEST_TABLE);

        $this->assertEquals(1, $results->getCount());

        $this->cleanupTestTable();
    }

    public function test_getInsertId()
    {
        self::$mysqlAdapter->selectDatabase(self::$MYSQL_NAME);

        self::$mysqlAdapter->query("CREATE TABLE " . self::$TEST_TABLE . " (id INT(11) AUTO_INCREMENT PRIMARY KEY)");

        $result = self::$mysqlAdapter->query("INSERT INTO " . self::$TEST_TABLE . " (id) VALUES (1)");

        $insertId = $result->getInsertId();

        $this->assertNotNull($insertId);
        $this->assertTrue($insertId > 0);

        $this->cleanupTestTable();
    }

    public function test_tableExists()
    {
        self::$mysqlAdapter->selectDatabase(self::$MYSQL_NAME);

        $this->assertFalse(self::$mysqlAdapter->tableExists(self::$TEST_TABLE));

        self::$mysqlAdapter->query("CREATE TABLE " . self::$TEST_TABLE . " (id INT(11) AUTO_INCREMENT PRIMARY KEY)");

        $this->assertTrue(self::$mysqlAdapter->tableExists(self::$TEST_TABLE));

        $this->cleanupTestTable();
    }

    public function test_query_withParametersAsArrayKeys()
    {
        self::$mysqlAdapter->selectDatabase(self::$MYSQL_NAME);

        self::$mysqlAdapter->query("CREATE TABLE " . self::$TEST_TABLE . " (id INT(11) AUTO_INCREMENT PRIMARY KEY)");

        self::$mysqlAdapter->query("INSERT INTO " . self::$TEST_TABLE . " (id) VALUES (?)", array(123));

        $results = self::$mysqlAdapter->query("SELECT * FROM " . self::$TEST_TABLE);

        $this->assertEquals(1, $results->getCount());
        $this->assertEquals(123, $results->getFirstRecord()['id']);

        $this->cleanupTestTable();
    }

    public function test_query_withParametersAsArrayWithKeyValues()
    {
        self::$mysqlAdapter->selectDatabase(self::$MYSQL_NAME);

        self::$mysqlAdapter->query("CREATE TABLE " . self::$TEST_TABLE . " (id INT(11) AUTO_INCREMENT PRIMARY KEY)");

        self::$mysqlAdapter->query("INSERT INTO " . self::$TEST_TABLE . " (id) VALUES (:id)", array('id' => 12));

        $results = self::$mysqlAdapter->query("SELECT * FROM " . self::$TEST_TABLE);

        $this->assertEquals(1, $results->getCount());
        $this->assertEquals(12, $results->getFirstRecord()['id']);

        $this->cleanupTestTable();
    }

    private function cleanupTestTable()
    {
        self::$mysqlAdapter->selectDatabase(self::$MYSQL_NAME);
        self::$mysqlAdapter->query("DROP TABLE " . self::$TEST_TABLE);
    }

}