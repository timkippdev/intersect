<?php

namespace TimKipp\Intersect\IT\Tests;

use TimKipp\Intersect\Email\Domain\EmailQueue;
use TimKipp\Intersect\Tests\IT\BaseITTest;
use TimKipp\Intersect\Tests\Stubs\TestEmail;
use TimKipp\Intersect\Tests\Stubs\TestEmailQueue;

class EmailQueueServiceTest extends BaseITTest {

    public function setUp()
    {
        parent::setUp();

        $this->emailQueueService->deleteAll();
    }

    public function test_deleteAll()
    {
        $emailQueue = new TestEmailQueue();

        $this->emailQueueService->create($emailQueue);
        $this->emailQueueService->create($emailQueue);
        $this->emailQueueService->create($emailQueue);
        $this->emailQueueService->create($emailQueue);
        $this->emailQueueService->create($emailQueue);
        
        $this->assertCount(5, $this->emailQueueService->getAll());
        
        $this->emailQueueService->deleteAll();

        $this->assertCount(0, $this->emailQueueService->getAll());
    }

    public function test_deleteAllSent()
    {
        $emailQueue = new TestEmailQueue();

        $createdEmailQueue = $this->emailQueueService->create($emailQueue);
        $this->emailQueueService->create($emailQueue);

        $this->assertCount(2, $this->emailQueueService->getAll());

        $this->emailQueueService->deleteAllSent();

        $this->assertCount(2, $this->emailQueueService->getAll());

        /** @var EmailQueue $createdEmailQueue */
        $createdEmailQueue->setDateSent($this->getMySQLNow());
        $this->emailQueueService->update($createdEmailQueue, $createdEmailQueue->getEmailId());

        $this->emailQueueService->deleteAllSent();

        $this->assertCount(1, $this->emailQueueService->getAll());
    }

    public function test_getAllUnsent()
    {
        $emailQueue = new TestEmailQueue();

        $createdEmailQueue = $this->emailQueueService->create($emailQueue);
        $this->emailQueueService->create($emailQueue);

        $this->assertCount(2, $this->emailQueueService->getAllUnsent());

        /** @var EmailQueue $createdEmailQueue */
        $createdEmailQueue->setDateSent($this->getMySQLNow());
        $this->emailQueueService->update($createdEmailQueue, $createdEmailQueue->getEmailId());

        $this->assertCount(1, $this->emailQueueService->getAllUnsent());
    }

    public function test_getAllUnsent_withLimit()
    {
        $emailQueue = new TestEmailQueue();

        $createdEmailQueue1 = $this->emailQueueService->create($emailQueue);
        $createdEmailQueue2 = $this->emailQueueService->create($emailQueue);
        $this->emailQueueService->create($emailQueue);
        $this->emailQueueService->create($emailQueue);
        $this->emailQueueService->create($emailQueue);

        $this->assertCount(5, $this->emailQueueService->getAllUnsent());

        /**
         * @var EmailQueue $createdEmailQueue1
         * @var EmailQueue $createdEmailQueue2
         */
        $createdEmailQueue1->setDateSent($this->getMySQLNow());
        $this->emailQueueService->update($createdEmailQueue1, $createdEmailQueue1->getEmailId());
        $createdEmailQueue2->setDateSent($this->getMySQLNow());
        $this->emailQueueService->update($createdEmailQueue2, $createdEmailQueue2->getEmailId());

        $this->assertCount(3, $this->emailQueueService->getAllUnsent());
    }

}