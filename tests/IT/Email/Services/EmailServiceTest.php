<?php

namespace TimKipp\Intersect\Tests\IT\Email\Services;

use TimKipp\Intersect\Email\Domain\EmailQueue;
use TimKipp\Intersect\Email\Services\EmailService;
use TimKipp\Intersect\Event\Event;
use TimKipp\Intersect\Tests\IT\BaseITTest;
use TimKipp\Intersect\Tests\Stubs\TestEmail;
use TimKipp\Intersect\Tests\Stubs\TestEmailSenderProblem;
use TimKipp\Intersect\Tests\Stubs\TestListener;

class EmailServiceTest extends BaseITTest {

    public function setUp()
    {
        parent::setUp();

        $this->emailQueueService->deleteAll();
    }

    public function test_queue()
    {
        $email = new TestEmail();
        $email->setRecipient('test@test.com');

        $this->emailService->queue($email);

        $results = $this->emailQueueService->getAll();

        $this->assertEquals(1, count($results));
        $this->assertTrue($results[0] instanceof EmailQueue);

        /** @var EmailQueue $emailQueue */
        $emailQueue = $results[0];

        $this->assertEquals('test@test.com', $emailQueue->getRecipient());

        $headers = $emailQueue->getHeaders();
        $this->assertTrue(count($headers->getHeaders()) == 4);


        $normalizedHeaders = str_replace("\r\n", '***', $emailQueue->getHeadersDb());
        $this->assertEquals('MIME-Version: 1.0***Content-type: text/html; charset=iso-8859-1***From: fromName <fromEmail>***Reply-To: fromName <replyToEmail>', $normalizedHeaders);

        $this->assertEquals('TestEmail', $emailQueue->getType());
        $this->assertEquals('subject', $emailQueue->getSubject());
        $this->assertEquals('message', $emailQueue->getMessage());
        $this->assertNotNull($emailQueue->getDateCreated());
        $this->assertNull($emailQueue->getDateSent());

        $this->assertFalse($this->emailSender->getSendCalled());
        $this->assertEquals(0, $this->emailSender->getTimesSendCalled());
    }

    public function test_queue_withCCBCCRecipients()
    {
        $email = new TestEmail();
        $email->setRecipient('test@test.com');
        $email->addCCRecipient('cc@test.com');
        $email->addBCCRecipient('bcc@test.com');

        $this->emailService->queue($email);

        $results = $this->emailQueueService->getAll();

        $this->assertEquals(1, count($results));
        $this->assertTrue($results[0] instanceof EmailQueue);

        /** @var EmailQueue $emailQueue */
        $emailQueue = $results[0];

        $this->assertEquals('test@test.com', $emailQueue->getRecipient());

        $headers = $emailQueue->getHeaders();
        $this->assertTrue(count($headers->getHeaders()) == 6);

        $normalizedHeaders = str_replace("\r\n", '***', $emailQueue->getHeadersDb());
        $this->assertEquals('MIME-Version: 1.0***Content-type: text/html; charset=iso-8859-1***From: fromName <fromEmail>***Reply-To: fromName <replyToEmail>***Cc: cc@test.com***Bcc: bcc@test.com', $normalizedHeaders);

        $this->assertEquals('TestEmail', $emailQueue->getType());
        $this->assertEquals('subject', $emailQueue->getSubject());
        $this->assertEquals('message', $emailQueue->getMessage());
        $this->assertNotNull($emailQueue->getDateCreated());
        $this->assertNull($emailQueue->getDateSent());

        $this->assertFalse($this->emailSender->getSendCalled());
        $this->assertEquals(0, $this->emailSender->getTimesSendCalled());
    }

    public function test_processQueue()
    {
        $email = new TestEmail();
        $email->setRecipient('test@test.com');

        $this->emailService->queue($email);
        $this->emailService->queue($email);
        $this->emailService->queue($email);

        $results = $this->emailQueueService->getAllUnsent();

        $this->assertEquals(3, count($results));
        /** @var EmailQueue $result */
        foreach ($results as $result)
        {
            $this->assertNull($result->getDateSent());
        }

        $this->emailService->processQueue();

        $results = $this->emailQueueService->getAllUnsent();

        $this->assertEquals(0, count($results));

        $this->assertTrue($this->emailSender->getSendCalled());
        $this->assertEquals(3, $this->emailSender->getTimesSendCalled());
    }

    public function test_processQueue_withLimit()
    {
        $email = new TestEmail();
        $email->setRecipient('test@test.com');

        $this->emailService->queue($email);
        $this->emailService->queue($email);
        $this->emailService->queue($email);

        $results = $this->emailQueueService->getAllUnsent();

        $this->assertEquals(3, count($results));
        /** @var EmailQueue $result */
        foreach ($results as $result)
        {
            $this->assertNull($result->getDateSent());
        }

        $this->emailService->processQueue(2);

        $results = $this->emailQueueService->getAllUnsent();

        $this->assertEquals(1, count($results));

        $this->assertTrue($this->emailSender->getSendCalled());
        $this->assertEquals(2, $this->emailSender->getTimesSendCalled());
    }

    public function test_emailSentEventFired()
    {
        $email = new TestEmail();
        $email->setRecipient('test@test.com');
        $listener = new TestListener();

        $this->emailService->getEventDispatcher()->addListener(Event::EMAIL_SENT, $listener);

        $this->emailService->send($email);

        $this->assertTrue($this->emailSender->getSendCalled());
        $this->assertEquals(1, $this->emailSender->getTimesSendCalled());
        $this->assertTrue($listener->getTriggered());
    }

    public function test_emailSentFailedEventFired()
    {
        $email = new TestEmail();
        $email->setRecipient('test@test.com');
        $listener = new TestListener();

        $this->emailService = new EmailService(self::$DB, new TestEmailSenderProblem());

        $this->emailService->getEventDispatcher()->addListener(Event::EMAIL_SENT_FAILED, $listener);

        $this->emailService->send($email);

        $this->assertTrue($listener->getTriggered());
    }

}