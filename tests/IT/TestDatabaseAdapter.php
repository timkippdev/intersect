<?php

namespace TimKipp\Intersect\Tests\IT;

use TimKipp\Intersect\Database\Adapters\MySQLAdapter;
use TimKipp\Intersect\Utility\DatabaseUtility;

require_once 'test-config.php';

class TestDatabaseAdapter extends MySQLAdapter {

    public function __construct()
    {
        parent::__construct(DB_HOST, DB_USER, DB_PASS);
    }

    public function getDbName()
    {
        return DB_NAME;
    }

    public function createDatabase($name)
    {
        echo 'CREATING DATABASE: ' . $name . PHP_EOL . PHP_EOL;
        parent::createDatabase($name);
    }

    public function dropDatabase($name)
    {
        echo 'DROPPING DATABASE: ' . $name . PHP_EOL . PHP_EOL;
        parent::dropDatabase($name);
    }

    public function selectDatabase($name)
    {
        echo 'CHANGING DATABASE: ' . $name . PHP_EOL . PHP_EOL;
        parent::selectDatabase($name);
    }

    public function query($sql, $parameters = array())
    {
        $result = parent::query($sql, $parameters);

        if (isset($this->getConnection()->errorInfo()[2]))
        {
            echo 'QUERY FAILED: <br />' . $this->getConnection()->errorInfo()[2] . PHP_EOL;
            echo '      ' . DatabaseUtility::removeQueryLineBreaks($sql) . PHP_EOL;
            return false;
        }

        echo DatabaseUtility::removeQueryLineBreaks($sql) . PHP_EOL;

        return $result;
    }

}