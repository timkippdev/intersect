<?php

namespace TimKipp\Intersect\IT\Tests\Migration;

use TimKipp\Intersect\Migration\MigrationRunner;
use TimKipp\Intersect\Migration\MigrationType;
use TimKipp\Intersect\Tests\IT\BaseITTest;

class MigrationRunnerTest extends BaseITTest {

    private static $TABLE_NAME = 'migration_test';

    /** @var MigrationRunner $migrationRunner */
    private $migrationRunner;

    /** @var MigrationRunner $migrationRunner2 */
    private $migrationRunner2;

    protected function setUp()
    {
        parent::setUp();

        $this->migrationRunner = new MigrationRunner(self::$DB, dirname(__FILE__). '/test-samples/runner1');
        $this->migrationRunner->setTableName(self::$TABLE_NAME);
        $this->migrationRunner2 = new MigrationRunner(self::$DB, dirname(__FILE__). '/test-samples/runner2');
        $this->migrationRunner2->setTableName(self::$TABLE_NAME);

        self::$DB->query("DROP TABLE IF EXISTS " . $this->migrationRunner->getTableName());
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Migration paths must be set
     */
    public function test_migrationPath_notSet()
    {
        $this->migrationRunner->setMigrationPath(null);
        $this->migrationRunner->upgradeToLatest();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Desired migration version is not available: 1.0.1
     */
    public function test_updateTo_versionException()
    {
        $this->migrationRunner->upgradeTo('1.0.1');

        $this->assertEquals(MigrationType::UP, $this->migrationRunner->getMigrationType());
    }

    public function test_upgradeToLatest()
    {
        $this->migrationRunner->upgradeToLatest();

        $this->assertEquals(MigrationType::UP, $this->migrationRunner->getMigrationType());

        $this->assertEquals(2, $this->migrationRunner->getMigrationsRan());
    }

    // this test is to verify the issue where higher versions in the first runner while cause all lower versions in the
    // second runner to not be applied
    public function test_upgradeToLatest_multipleRunners()
    {
        $this->migrationRunner->upgradeToLatest();
        $this->migrationRunner2->upgradeToLatest();

        $this->assertEquals(2, $this->migrationRunner->getMigrationsRan());
        $this->assertEquals(4, $this->migrationRunner2->getMigrationsRan());
    }

    public function test_upgradeToLatest_runnerWithMultiplePaths()
    {
        $mr = new MigrationRunner(self::$DB, array(
            dirname(__FILE__). '/test-samples/runner1',
            dirname(__FILE__). '/test-samples/runner2'
        ));
        $mr->setTableName(self::$TABLE_NAME);

        $mr->upgradeToLatest();

        $actualMigrationsRan = ($mr->getMigrationsRan());

        $this->assertEquals(6, $actualMigrationsRan);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Migration paths must be set
     */
    public function test_upgradeToLatest_runnerWithMultiplePaths_noPaths()
    {
        $mr = new MigrationRunner(self::$DB);
        $mr->setTableName(self::$TABLE_NAME);

        $mr->upgradeToLatest();

        $this->assertEquals(6, $mr->getMigrationsRan());
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Migration paths must be set
     */
    public function test_upgradeToLatest_runnerWithMultiplePaths_emptyPaths()
    {
        $mr = new MigrationRunner(self::$DB, array());
        $mr->setTableName(self::$TABLE_NAME);

        $mr->upgradeToLatest();

        $this->assertEquals(6, $mr->getMigrationsRan());
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Desired migration version is not available: 1.0.1
     */
    public function test_downgradeTo_versionException()
    {
        $this->migrationRunner->downgradeTo('1.0.1');
    }

    public function test_downgradeTo()
    {
        $this->migrationRunner->upgradeToLatest();
        $this->migrationRunner->downgradeTo('1.0.0');

        $this->assertEquals(MigrationType::DOWN, $this->migrationRunner->getMigrationType());

        $this->assertEquals(3, $this->migrationRunner->getMigrationsRan());
    }

    public function test_downgradeCompletely()
    {
        $this->migrationRunner->upgradeToLatest();
        $this->migrationRunner->downgradeCompletely();

        $this->assertEquals(MigrationType::DOWN, $this->migrationRunner->getMigrationType());
        $this->assertEquals(4, $this->migrationRunner->getMigrationsRan());
    }

    public function test_downgradeCompletely_migratedOnColumnCleared()
    {
        $this->migrationRunner->upgradeToLatest();
        $this->assertEquals(2, $this->migrationRunner->getMigrationsRan());

        $records = self::$DB->query("SELECT * FROM " . self::$TABLE_NAME)->getRecords();
        foreach ($records as $record)
        {
            $this->assertNotNull($record['migrated_on']);
        }

        $this->migrationRunner->downgradeCompletely();
        $this->assertEquals(4, $this->migrationRunner->getMigrationsRan());

        $records = self::$DB->query("SELECT * FROM " . self::$TABLE_NAME)->getRecords();
        foreach ($records as $record)
        {
            $this->assertNull($record['migrated_on']);
        }
    }

}