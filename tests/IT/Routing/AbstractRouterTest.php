<?php

namespace TimKipp\Intersect\Tests\IT\Routing;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Tests\Stubs\TestRouter;

class AbstractRouterTest extends TestCase {

    /**
     * @var TestRouter $testRouter
     */
    private $testRouter;

    public function setUp()
    {
        $this->testRouter = new TestRouter();

        unset($_GET);
    }

    public function test_getResponse_urlParameterNotSet()
    {
        $response = $this->testRouter->getResponse();

        $this->assertNotNull($response);
        $this->assertEquals(200, $response->getStatus());
        $this->assertEquals('homepage', $response->getBody());
    }

    public function test_getResponse_urlParameterEmpty()
    {
        $_GET['url'] = '';
        $response = $this->testRouter->getResponse();

        $this->assertNotNull($response);
        $this->assertEquals(200, $response->getStatus());
        $this->assertEquals('homepage', $response->getBody());
    }

    public function test_getResponse_urlParameterSingleSlash()
    {
        $_GET['url'] = '/';

        $response = $this->testRouter->getResponse();

        $this->assertNotNull($response);
        $this->assertEquals(200, $response->getStatus());
        $this->assertEquals('homepage', $response->getBody());
    }

    public function test_getResponse_urlParameterChanged_notSet()
    {
        $_GET['url'] = '/get';

        $this->testRouter = new TestRouter('test');
        $response = $this->testRouter->getResponse();

        $this->assertNotNull($response);
        $this->assertEquals(200, $response->getStatus());
        $this->assertEquals('homepage', $response->getBody());
    }

    public function test_getResponse_urlParameterChanged()
    {
        $_GET['test'] = '/get';

        $this->testRouter = new TestRouter('test');
        $response = $this->testRouter->getResponse();

        $this->assertNotNull($response);
        $this->assertEquals(200, $response->getStatus());
        $this->assertEquals('get', $response->getBody());
    }

    public function test_getResponse_routeNotFound()
    {
        $_GET['url'] = '/not-found';
        $response = $this->testRouter->getResponse();

        $this->assertNotNull($response);
        $this->assertEquals(404, $response->getStatus());
        $this->assertEquals('Route not found', $response->getBody());
    }

    public function test_getResponse_methodReturnsString()
    {
        $_GET['url'] = '/get';
        $response = $this->testRouter->getResponse();
        $this->assertNotNull($response);
        $this->assertEquals(200, $response->getStatus());
        $this->assertEquals('get', $response->getBody());
    }

    public function test_getResponse_methodWithParameterReturnsString()
    {
        $_GET['url'] = '/get/1/test';
        $response = $this->testRouter->getResponse();
        $this->assertNotNull($response);
        $this->assertEquals(200, $response->getStatus());
        $this->assertEquals('get1test', $response->getBody());
    }

    public function test_getResponse_methodReturnsResponse()
    {
        $_GET['url'] = '/get2';
        $response = $this->testRouter->getResponse();
        $this->assertNotNull($response);
        $this->assertEquals(200, $response->getStatus());
        $this->assertEquals('get2', $response->getBody());
    }

    public function test_getResponse_methodWithParametersReturnsResponse()
    {
        $_GET['url'] = '/get2/1/test';
        $response = $this->testRouter->getResponse();
        $this->assertNotNull($response);
        $this->assertEquals(200, $response->getStatus());
        $this->assertEquals('get21test', $response->getBody());
    }

}