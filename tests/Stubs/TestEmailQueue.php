<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Email\Domain\EmailHeader;
use TimKipp\Intersect\Email\Domain\EmailHeaders;
use TimKipp\Intersect\Email\Domain\EmailQueue;

class TestEmailQueue extends EmailQueue {

    public function __construct()
    {
        $emailHeaders = new EmailHeaders();
        $emailHeaders->addHeader(new EmailHeader('test', 'value'));

        $this->setRecipient('test@test.com');
        $this->setHeaders($emailHeaders);
        $this->setMessage('message');
        $this->setSubject('subject');
        $this->setType('test');
    }

}