<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Routing\AbstractRouter;

class TestRouter extends AbstractRouter {

    public function getRouteFromUrl($url)
    {
        return parent::getRouteFromUrl($url);
    }

    public function setCurrentRequestMethod($requestMethod)
    {
        parent::setCurrentRequestMethod($requestMethod);
    }

    public function getCustomRoutes()
    {
        return array(
            'GET /' => '\TimKipp\Intersect\Tests\Stubs\TestController::testGetAsHomepage()',
            'GET /get' => '\TimKipp\Intersect\Tests\Stubs\TestController::testGetAsString()',
            'GET /get/:id/:username' => '\TimKipp\Intersect\Tests\Stubs\TestController::testGetWithParametersAsString(id,username)',
            'GET /get2' => '\TimKipp\Intersect\Tests\Stubs\TestController::testGetAsResponse()',
            'GET /get2/:id/:username' => '\TimKipp\Intersect\Tests\Stubs\TestController::testGetWithParametersAsResponse(id,username)',
            'GET /parameter/:id/test/:username' => '\TimKipp\Intersect\Tests\Stubs\TestController::testGet(id,username)',
            'POST /post' => '\TimKipp\Intersect\Tests\Stubs\TestController::testPost()',
            'PUT /put' => '\TimKipp\Intersect\Tests\Stubs\TestController::testPut()',
            'DELETE /delete' => '\TimKipp\Intersect\Tests\Stubs\TestController::testDelete()'
        );
    }

}