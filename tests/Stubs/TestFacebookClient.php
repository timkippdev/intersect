<?php

namespace TimKipp\Intersect\Tests\Stubs;

use Facebook\Facebook;
use Facebook\Helpers\FacebookRedirectLoginHelper;
use TimKipp\Intersect\Social\Facebook\FacebookClient;

class TestFacebookClient extends FacebookClient {

    public function getFacebook()
    {
        return parent::getFacebook();
    }

    public function setFacebook(Facebook $facebook)
    {
        parent::setFacebook($facebook);
    }

    public function setFacebookRedirectLoginHelper(FacebookRedirectLoginHelper $facebookRedirectLoginHelper)
    {
        parent::setFacebookRedirectLoginHelper($facebookRedirectLoginHelper);
    }

    public function getFacebookRedirectLoginHelper()
    {
        return parent::getFacebookRedirectLoginHelper();
    }


}