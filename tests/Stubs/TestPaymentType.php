<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Order\Domain\PaymentType;

class TestPaymentType extends PaymentType {

    public function __construct()
    {
        $this->setName('payment_type_' . uniqid());
    }

}