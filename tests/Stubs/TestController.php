<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Routing\Response;

class TestController {

    public function testGetAsHomePage()
    {
        return 'homepage';
    }

    public function testGetAsString()
    {
        return 'get';
    }

    public function testGetWithParametersAsString($id, $username)
    {
        return 'get' . $id . $username;
    }

    public function testGetAsResponse()
    {
        return new Response('get2', 200);
    }

    public function testGetWithParametersAsResponse($id, $username)
    {
        return new Response('get2' . $id . $username, 200);
    }

}