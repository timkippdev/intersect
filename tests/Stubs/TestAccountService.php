<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Services\AccountService;

class TestAccountService extends AccountService {

    public static $SESSION_NAME = 'act-rem-me';

    public function generateSessionValue($accountId, $timestamp)
    {
        return parent::generateSessionValue($accountId, $timestamp);
    }

    protected function generateSessionSignature($accountId, $timestamp)
    {
        return 'sig-' . $accountId . '-' . $timestamp;
    }


}