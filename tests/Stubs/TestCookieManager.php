<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Helper\CookieManager;

class TestCookieManager extends CookieManager {

    private $cookies = array();

    public function write($key, $value, $expires = 0, $path = '', $domain = '', $secure = false, $httpOnly = false)
    {
        $this->cookies[$key] = $value;
    }

    public function read($key)
    {
        if (array_key_exists($key, $this->cookies))
        {
            return $this->cookies[$key];
        }

        return null;
    }

    public function clear($key)
    {
        if (array_key_exists($key, $this->cookies))
        {
            unset($this->cookies[$key]);
        }
    }

    public function clearAll()
    {
        $this->cookies = array();
    }

}