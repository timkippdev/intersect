<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Database\Adapters\AbstractAdapter;

class TestAdapter extends AbstractAdapter {

    public function createDatabase($name) {}

    public function dropDatabase($name) {}

    public function selectDatabase($name) {}

    public function dropTable($table) {}

    public function execute($sql) {}

    public function query($sql, $parameters = array()) {}

    public function tableExists($table) {}

    public function connect()
    {
        return 'connected';
    }
}