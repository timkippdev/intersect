<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Database\Dao\AbstractAssociationDao;

class TestAssociationDao extends AbstractAssociationDao {

    protected function getAssociationColumnOne()
    {
        return 'column_one';
    }

    protected function getAssociationColumnTwo()
    {
        return 'column_two';
    }

    protected function getTable()
    {
        return 'association_test';
    }

}