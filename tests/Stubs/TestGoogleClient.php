<?php

namespace TimKipp\Intersect\Tests\Stubs;

use Abraham\TwitterOAuth\TwitterOAuth;
use TimKipp\Intersect\Social\Google\GoogleClient;
use TimKipp\Intersect\Social\Twitter\TwitterClient;

class TestGoogleClient extends GoogleClient {

    public function getOAuthClient()
    {
        return parent::getOAuthClient();
    }

    public function setOAuthClient(\Google_Client $client)
    {
        parent::setOAuthClient($client);
    }

    public function getOAuthService()
    {
        return parent::getOAuthService();
    }

    public function setOAuthService(\Google_Service $service)
    {
        parent::setOAuthService($service);
    }

}