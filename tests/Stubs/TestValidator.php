<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Validation\AbstractValidator;

class TestValidator extends AbstractValidator {

    public function validateCreate($obj) {}
    public function validateDelete($obj) {}
    public function validateUpdate($obj) {}

}