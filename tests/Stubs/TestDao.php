<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Database\Dao\AbstractDao;

/**
 * Class TestDao
 * @package TimKipp\Intersect\Tests\Stubs
 */
class TestDao extends AbstractDao {

    /**
     * @return string
     */
    protected function getTable()
    {
        return 'test';
    }

    /**
     * @return string
     */
    public function getDomainClass()
    {
        return TestDomain::class;
    }

    /**
     * @return TestValidator
     */
    public function getValidator()
    {
        return new TestValidator();
    }

}