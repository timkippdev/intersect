<?php

namespace TimKipp\Intersect\Tests\Stubs;

class TestGoogleServiceNullUserInfo {

    public function get() {
        return null;
    }

}