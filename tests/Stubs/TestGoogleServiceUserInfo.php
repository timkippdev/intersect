<?php

namespace TimKipp\Intersect\Tests\Stubs;

class TestGoogleServiceUserInfo {

    public function get() {
        return new TestGoogleServiceUser();
    }

}