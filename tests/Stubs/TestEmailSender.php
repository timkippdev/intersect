<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Email\Domain\EmailHeaders;
use TimKipp\Intersect\Email\Sender\EmailSender;

class TestEmailSender implements EmailSender {

    private $sendCalled;
    private $timesSendCalled;

    public function __construct()
    {
        $this->sendCalled = false;
        $this->timesSendCalled = 0;
    }

    public function send($recipient, $subject, $message, EmailHeaders $headers)
    {
        $this->sendCalled = true;
        $this->timesSendCalled++;

        return true;
    }

    public function getSendCalled()
    {
        return $this->sendCalled;
    }

    public function getTimesSendCalled()
    {
        return $this->timesSendCalled;
    }

}