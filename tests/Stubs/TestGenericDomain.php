<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Domain\AbstractDomain;

class TestGenericDomain extends AbstractDomain {

    public $id;
    public $message;

    public function __construct()
    {
        $this->message = 'test message: ' . time();
    }

    public function getPrimaryKeyColumn()
    {
        return 'id';
    }

    public static function getColumnMappings()
    {
        return array(
            'id' => 'id',
            'message' => 'message'
        );
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

}