<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Event\Event;
use TimKipp\Intersect\Event\Listener;

class TestListener extends Listener {

    private $triggered;
    private $receivedMessage;

    public function __construct()
    {
        $this->triggered = false;
    }
    public function handle(Event $event, $customMessage = null)
    {
        $this->triggered = true;
        $this->receivedMessage = $customMessage;

        return 'handled event - ' . $event->getName();
    }

    public function getTriggered()
    {
        return $this->triggered;
    }

    public function getReceivedMessage()
    {
        return $this->receivedMessage;
    }

}