<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Event\Event;

class TestEvent extends Event {

    public function getName()
    {
        return 'test';
    }

}