<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Email\Domain\EmailHeaders;
use TimKipp\Intersect\Email\Sender\EmailSender;

class TestEmailSenderProblem extends TestEmailSender {

    /**
     * @param $recipient
     * @param $subject
     * @param $message
     * @param EmailHeaders $headers
     * @return bool|void
     * @throws \Exception
     */
    public function send($recipient, $subject, $message, EmailHeaders $headers)
    {
        throw new \Exception('Failed to send');
    }

}