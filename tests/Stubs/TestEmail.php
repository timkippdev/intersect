<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Email\Domain\BaseEmail;

class TestEmail extends BaseEmail {

    public function getSubject()
    {
        return 'subject';
    }

    public function getMessage()
    {
        return 'message';
    }

    public function getFromEmail()
    {
        return 'fromEmail';
    }

    public function getFromName()
    {
        return 'fromName';
    }

    public function getReplyToEmail()
    {
        return 'replyToEmail';
    }

}