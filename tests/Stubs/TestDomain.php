<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Domain\AbstractDomain;

class TestDomain extends AbstractDomain {
    public $id;
    public $email;
    public $username;
    public $nullable;

    public static function getColumnMappings()
    {
        return array(
            'id' => 'id',
            'email' => 'email',
            'username' => 'username',
            'nullable' => 'nullable'
        );
    }

    public static function getNullableColumns()
    {
        return array(
            'nullable'
        );
    }

    public function getPrimaryKeyColumn()
    {
        return 'id';
    }
}