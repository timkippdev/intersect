<?php

namespace TimKipp\Intersect\Tests\Stubs;

class TestGoogleServiceInvalidUserInfo {

    public function get() {
        return new TestGoogleServiceInvalidUser();
    }

}