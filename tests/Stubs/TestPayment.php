<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Order\Domain\Payment;
use TimKipp\Intersect\Order\Domain\PaymentType;

class TestPayment extends Payment {

    public function __construct()
    {
        $confirmationNumber= sha1(time() . uniqid());

        $this->setAmount(12.34);
        $this->setConfirmationNumber($confirmationNumber);
        $this->setExternalConfirmationNumber('ext_' . $confirmationNumber);
        $this->setPaymentTypeId(PaymentType::CASH);
    }

}