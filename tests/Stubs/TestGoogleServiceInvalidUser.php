<?php

namespace TimKipp\Intersect\Tests\Stubs;

class TestGoogleServiceInvalidUser {

    public function getId()
    {
        return '';
    }

    public function getEmail()
    {
        return '';
    }

}