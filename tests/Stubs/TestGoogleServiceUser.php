<?php

namespace TimKipp\Intersect\Tests\Stubs;

class TestGoogleServiceUser {

    public function getId()
    {
        return '123';
    }

    public function getEmail()
    {
        return 'test@test.com';
    }

}